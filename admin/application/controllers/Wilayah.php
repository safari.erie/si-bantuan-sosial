<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah extends CI_Controller {

	function __construct() {
        parent:: __construct();
		$this->load->model('Wilayah_model','wm');
		$user_id = $this->session->userdata(S_ID_USER);
		if($user_id == null){
			redirect(base_url()."admin/login");
		}
	}


	function getKecamatan()
	{

		$data = $this->wm->getKec();
		$result['Data'] = $data;
		echo json_encode($result);
	}

	function getDesa(){
		$kode_kec = $this->input->post('kode_kec');
		
		$data = $this->wm->getDesa($kode_kec);
		$result['Data'] = $data;
		echo json_encode($result);

	}




}
