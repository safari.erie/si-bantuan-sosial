<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	public function TestDownload(){

			$year 	= $this->input->post('year');
			include APPPATH.'third_party/PHPExcel/PHPExcel.php';
			$file_name = "Test Download ".date('Y-m-d H:i:s').".xlsx";

			// Panggil class PHPExcel nya
			$excel = new PHPExcel();	

			// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
			$style_col = array(
				'font' => array('bold' => true), // Set font nya jadi bold
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
				),
				'borders' => array(
					'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
					'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
					'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
					'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
				)
			);

			// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
			$style_row = array(
				'alignment' => array(
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
				),
				'borders' => array(
					'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
					'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
					'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
					'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
				)
			);
		
			$excel->setActiveSheetIndex(0)->setCellValue('B1', "Report For Employee History of The Month"); // Set kolom A1 dengan tulisan "DATA "
			$excel->getActiveSheet()->mergeCells('B1:F1'); // Set Merge Cell pada kolom A1 sampai E1
			$excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(TRUE); // Set bold kolom A1
			$excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
			$excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1

			$excel->setActiveSheetIndex(0)->setCellValue('B3', 'Test'); // Set kolom A3 dengan tulisan "NO"	
			$excel->setActiveSheetIndex(0)->setCellValue('B4', 'Test'); // Set kolom A3 dengan tulisan "NO"	
			$excel->setActiveSheetIndex(0)->setCellValue('B5', 'Test'); // Set kolom A3 dengan tulisan "NO"	
			$excel->getActiveSheet()->getStyle('B3')->getFont()->setBold(TRUE); // Set bold kolom A1
			$excel->getActiveSheet()->getStyle('B3')->getFont()->setSize(12); // Set font size 15 untuk kolom A1
			$excel->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1
			
			$excel->setActiveSheetIndex(0)->setCellValue('B7', "NO");
			$excel->getActiveSheet()->mergeCells('B7:B8');
			$excel->setActiveSheetIndex(0)->setCellValue('C7', "NAME");
			$excel->getActiveSheet()->mergeCells('C7:C8');
			$excel->setActiveSheetIndex(0)->setCellValue('D7', "DIVISION");
			$excel->getActiveSheet()->mergeCells('D7:D8');
			$excel->setActiveSheetIndex(0)->setCellValue('E7', "POSITION");
			$excel->getActiveSheet()->mergeCells('E7:E8');
			$excel->setActiveSheetIndex(0)->setCellValue('F7', "MONTH");
			$excel->getActiveSheet()->mergeCells('F7:F8');
			$excel->setActiveSheetIndex(0)->setCellValue('G7', "STATUS");
			$excel->getActiveSheet()->mergeCells('G7:G8');
			$excel->setActiveSheetIndex(0)->setCellValue('H7', "COMPLIMENT");
			$excel->getActiveSheet()->mergeCells('H7:H8');
			$excel->setActiveSheetIndex(0)->setCellValue('I7', "STAR");
			$excel->getActiveSheet()->mergeCells('I7:K7');
			$excel->setActiveSheetIndex(0)->setCellValue('I8', "BLACK");
			$excel->setActiveSheetIndex(0)->setCellValue('J8', "GOLD");
			$excel->setActiveSheetIndex(0)->setCellValue('K8', "SILVER");

			// apply style header 
			$excel->getActiveSheet()->getStyle('B7')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('B8')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('C7')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('C8')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('D7')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('D8')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('E7')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('E8')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('F7')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('F8')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('G7')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('G8')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('H7')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('H8')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('I7')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('J7')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('I8')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('J8')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('K7')->applyFromArray($style_col);
			$excel->getActiveSheet()->getStyle('K8')->applyFromArray($style_col);

			
		
			$data = array();

			$no = 1; // Untuk penomoran tabel, di awal set dengan 1
			$numrow = 9; // Set baris pertama untuk isi tabel adalah baris ke 
			$black_status_arr = array();
			$gold_status_arr = array();
			$silver_status_arr = array();
			
		 	$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, "TOTAL");
		

		

			$excel->getActiveSheet()->getColumnDimension('B')->setWidth(5); // Set width kolom By
			$excel->getActiveSheet()->getColumnDimension('C')->setWidth(35); // Set width kolom C
			$excel->getActiveSheet()->getColumnDimension('D')->setWidth(35); // Set width kolom D
			$excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
			$excel->getActiveSheet()->getColumnDimension('F')->setWidth(20); // Set width kolom E
			$excel->getActiveSheet()->getColumnDimension('G')->setWidth(20); // Set width kolom F
			$excel->getActiveSheet()->getColumnDimension('H')->setWidth(40); // Set width kolom G
			$excel->getActiveSheet()->getColumnDimension('I')->setWidth(8); // Set width kolom H
			$excel->getActiveSheet()->getColumnDimension('J')->setWidth(8); // Set width kolom I
			$excel->getActiveSheet()->getColumnDimension('K')->setWidth(8); // Set width kolom J

			// Set orientasi kertas jadi LANDSCAPE
			$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

			// Set judul file excel nya
			$excel->getActiveSheet(0)->setTitle("Laporan Data");
			$excel->setActiveSheetIndex(0);
			ob_clean();
			// Proses file excel
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="'.$file_name.'"'); // Set nama file excel nya
			header('Cache-Control: max-age=0');
			$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
			$write->save('php://output');
	}

}
