<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VerifikasiApprove extends CI_Controller {

	function __construct() {
        parent:: __construct();
		$this->load->model('verifikasi_model','vm');
		$user_id = $this->session->userdata(S_ID_USER);
		if($user_id == null){
			redirect(base_url()."admin/login");
		}
	}

	public function index()
	{
		
		$data['jsapp'] = array('admin/verifikasi_approve');
		

		$this->load->view('header',$data);
		$this->load->view('verifikasi_approve');
		$this->load->view('footer');
	}

	function get(){

		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];
	
		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		$kode_kec = $this->input->post('kode_kec');
		$kode_desa = $this->input->post('kode_desa');
		$nik = $this->input->post('nik');
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		

		$result = array();
        $recordsTotal = $this->vm->count_data_verifikasi_approve($kode_kec, $kode_desa, $nik);
		
        $row = array();
		$results = $this->vm->get_approve_verifikasi($length,$start, $def['order'], 'asc',$kode_kec, $kode_desa, $nik);
	
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				$ret_status_verifikasi;
				if($d->status_verifikasi == 0){
					$ret_status_verifikasi = "Baru dan Menunggu Persetujuan";
				}else if($d->status_verifikasi == 1){
					$ret_status_verifikasi = "Verifikasi disetujui";
				}else if($d->status_verifikasi == -1){
					$ret_status_verifikasi = "Verifikasi disetujui";
				}
                $row[] = array
				(
					"no"					=> $ii,
					"nama" 					=> $d->nama ,
					"nik" 					=> $d->nik ,
					"alamat" 				=> $d->alamat,					
					"nama_bantuan" 			=> $d->nama_bantuan,
					"kecamatan" 			=> $d->nama_kec,
					"desa" 					=> $d->nama_desa,
				    "status_verifikasi" 		=> $ret_status_verifikasi,
					"foto_ktp" 				=> '<img class="img-thumbnail" src="'. base_url().'admin/uploads/kk_ktp/'.$d->foto_ktp.'">',
					"foto_rumah" 			=> '<img class="img-thumbnail" src="'. base_url().'admin/uploads/kk_ktp/'.$d->foto_rumah.'">',
					"created"				=> $d->created,
					"aksi" 	    			=> '<button type="button" class="btn btn-info btn-flat btn-sm" onclick="getDetailVerifikasiById('.$d->id_verifikasi.');"><i class="fa fa-check"></i> Detail </button>'
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
	}


	function GetVerifikasiById(){
		$id_verifikasi = $this->input->post('id_verifikasi');
		$DataVerifikasiId = $this->vm->verifikasiById($id_verifikasi);
		//echo json_encode($DataVerifikasiId)
		$file_path 	  = './uploads/kk_ktp/';
		$fileNamerumah = $DataVerifikasiId->foto_rumah;
		$fileNameKtp = $DataVerifikasiId->foto_ktp;
		$retRumah= "";
		if(file_exists($file_path.$fileNamerumah)){
			$retRumah = $DataVerifikasiId->foto_rumah;
		}else{
			$retRumah = null;
		}

		$retKtp= "";
		if(file_exists($file_path.$fileNameKtp)){
			$retKtp = $DataVerifikasiId->foto_ktp;
		}else{
			$retKtp = null;
		}

		if(count($DataVerifikasiId) != 0){
			$dt = array(
				'id_verifikasi'	=> $DataVerifikasiId->id_verifikasi,
				'hasil'	=> $DataVerifikasiId->hasil,
				'foto_ktp'	=> $retKtp,
				'foto_rumah'	=> $retRumah,
				'id_kpm'	=> $DataVerifikasiId->id_kpm,
				'created'	=> $DataVerifikasiId->created,
				'id_bansos'	=> $DataVerifikasiId->id_bansos,
				'id_bantuan'	=> $DataVerifikasiId->id_bantuan,
				'status_verifikasi'	=> $DataVerifikasiId->status_verifikasi,
				'nama_bantuan'	=> $DataVerifikasiId->nama_bantuan,
				'keterangan'	=> $DataVerifikasiId->keterangan,
				'status'	=> $DataVerifikasiId->status,
				'nama'	=> $DataVerifikasiId->nama,
				'nik'	=> $DataVerifikasiId->nik,
				'alamat'	=> $DataVerifikasiId->alamat,
				'layak'	=> $DataVerifikasiId->layak,
				'dtks'	=> $DataVerifikasiId->dtks,
				'latitude'	=> $DataVerifikasiId->latitude,
				'longitude'	=> $DataVerifikasiId->longitude,
				'kode_desa'	=> $DataVerifikasiId->kode_desa,
				'nama_desa'	=> $DataVerifikasiId->nama_desa,
				'kode_kec'	=> $DataVerifikasiId->kode_kec,
				'nama_kec'	=> $DataVerifikasiId->nama_kec,

			);

			$output = array(
				'state'	=> true,
				'msg'	=> null,
				'data'	=> $dt
			);

			echo json_encode($output);
		}else {
			
			$output = array(
				'state'	=> false,
				'msg'	=> 'data tidak ditemukan',
				'data'	=> null
			);

			echo json_encode($output);
		}

	}

	function SaveProsesVerifikasi(){

		$id_verifikasi = $this->input->post('id_verifikasi_inp');
		$persetujuan = $this->input->post('persetujuan');
		$id_kpm_verifikasi = $this->input->post('id_kpm_verifikasi');

		$saveVerifikasi = $this->vm->SaveProsesVerifikasi($id_verifikasi,$persetujuan,$id_kpm_verifikasi);

		if($saveVerifikasi){
			$output = array(
				'state'	=> true,
				'msg'	=> 'Verifikasi berhasil'
			);

			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Proses Verifikasi Gagal '
			);

			echo json_encode($output);
		}
	}

}
