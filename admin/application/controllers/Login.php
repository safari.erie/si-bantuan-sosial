<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('login_model');
    }

	public function index()
	{
		$this->load->view('login');
	}

	public function check_login(){

		$usernameOremail = $this->input->post('UsernameOrEmail');
		$password = md5($this->input->post('password',true));
		
		$get_user_login = $this->login_model->auth_user($usernameOremail);
		
		if(count($get_user_login) > 0){
			$get_user_login = $get_user_login[0];			
			if($password == $get_user_login->password){
				$this->session->set_userdata(S_USER_NAME, $get_user_login->username);
                $this->session->set_userdata(S_USER_EMAIL, $get_user_login->email);
                $this->session->set_userdata(S_LOGGED_IN, TRUE);
				$this->session->set_userdata(S_ID_USER,$get_user_login->id_user);
				redirect('admin/main');
			}else{
				$this->session->set_flashdata('notif_pass_wrong', '<strong>Gagal. </strong> Password anda tidak sesuai.');
                redirect('admin/login/failed');
			}

		}else{
			$this->session->set_flashdata('notif_email_wrong', '<strong>Gagal. </strong> Username atau Email anda tidak terdaftar.');
            redirect('admin/login/');
		}

	}

	function failed() {        
        $this->index();
	}
	
	function out(){
		$this->session->sess_destroy();
        redirect('admin/login/' . $this->uri->segment(4));
	}


}
