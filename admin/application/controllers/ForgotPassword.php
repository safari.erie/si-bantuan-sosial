<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ForgotPassword extends CI_Controller {

	function __construct() {
        parent:: __construct();
		$this->load->model('ForgotPassword_model','fm');
		$this->load->helper('string');
    }

	function index(){
		
		$this->load->view('forgot_password');
	}

	function saveForgotPassword(){
		$email = $this->input->post('email');

		$data_user = $this->fm->CheckUserEmail($email);
		if(count($data_user) != 0){
			if($data_user->status != 1){
				$output = array(
					'state'	=> false,
					'msg'	=> 'Email Sudah di non aktifkan, silahkan hubungi Admin',
					'data'	=> null,
				);
	
				echo json_encode($output);
			}else{
				$code 					= md5(random_string('alnum', 8));
				$data['fullName']	= $data_user->full_name;
				$data['email']	= $email;
				$data['link'] 			= base_url() . 'admin/ForgotPassword/reset_password?k=' . $code; 
				$emp['reset_password_code'] = $code;
				$this->fm->reset_password($emp, $email, false); 
				
				$sent_email = $this->sent_email_reset_password($data,'link_forgot');
				if($sent_email)
				{
					$result = [
						'state' => true,
						'msg' => 'Permintaan Reset Kata Sandi telah dikirim ke email',
						'data' => ''
					];

					echo json_encode($result);
				}
				else
				{
					$result = [
						'state' => false,
						'message' => 'Terjadi kesalahan saat mengirimkan email',
						'data' => array()
					];
					echo json_encode($result);
				}
			}
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Email tidak ditemukan, silahkan hubungi Admin',
				'data'	=> null,
			);

			echo json_encode($output);
		}
	}

	function sent_email_reset_password($data) 
	{
        $this->load->library('email');

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_timeout'] = '30';
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';

        $this->email->initialize($config);
		$this->email->from(SMTP_USER, EMAIL_ALIAS);
        $this->email->to($data['email']);


        $message = $this->load->view('mail_reset_password', $data, true);
        $this->email->subject('Permintaan Ubah Kata Sandi ' . APP_NAME);
        $this->email->message($message);				

        return $this->email->send();
	}
	

	function reset_password(){
		$data['key_code'] = $this->input->get('k');		
		$this->load->view('frm_reset_password',$data);
	}

	function SaveResetPassword(){
		$key_code = $this->input->post('key_code');
		$password = md5($this->input->post('password'));

		$checkKeyCode = $this->fm->CheckKeyCodeResetPassword($key_code);

		if(count($checkKeyCode) != 0 || $checkKeyCode != null){
			$emp = array(
				'password'	=> $password,
				'reset_password_code'	=> null,
				'updated_by' => $checkKeyCode->id_user,
				'updated_date' => date('Y-m-d H:i:s')
			);
			$reset_password = $this->fm->reset_password($emp, '', true, $key_code); 			
			if($reset_password){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Reset Password Berhasil',
					'data'	=> null,
				);
				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> false,
					'msg'	=> 'Reset Password Gagal',
					'data'	=> null,
				);
				echo json_encode($output);
			}

		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Key Code Expired, Silahkan Mengulangi request reset Password',
				'data'	=> null
			);
			echo json_encode($output);
		}
	}

}
