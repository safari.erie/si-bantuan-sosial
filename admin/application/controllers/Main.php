<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct() {
        parent:: __construct();
		$this->load->model('dashboard_model','dshm');
		$user_id = $this->session->userdata(S_ID_USER);
		if($user_id == null){
			redirect(base_url()."admin/login");
		}
	}

	public function index()
	{
		
		$data['jsapp'] = array('admin/main_dashboard');
		// all count data is
		$data['CProvAll'] = $this->dshm->countAllBansos(2);
		$data['CPresidenAll'] = $this->dshm->countAllBansos(1);
		$data['CBupatiAll'] = $this->dshm->countAllBansos(3);
		$data['CDdAll'] = $this->dshm->countAllBansos(4);
		$data['CSembakoRegulerAll'] = $this->dshm->countAllBansos(5);
		$data['CSembakoPerluasanAll'] = $this->dshm->countAllBansos(6);
		$data['CBltAll'] = $this->dshm->countAllBansos(7);
		$data['CPkhAll'] = $this->dshm->countAllBansos(8);

		$data['CDtksProv'] = $this->dshm->countDtksBansos(2);
		$data['CNonDtksProv'] = $this->dshm->countNonDtksBansos(2);

		$data['CDtksPresiden'] = $this->dshm->countDtksBansos(1);
		$data['CNonDtksPresiden'] = $this->dshm->countNonDtksBansos(1);

		$data['CDtksBupati'] = $this->dshm->countDtksBansos(3);
		$data['CNonDtksBupati'] = $this->dshm->countNonDtksBansos(3);

		$data['CDtksDd'] = $this->dshm->countDtksBansos(4);
		$data['CNonDtksDd'] = $this->dshm->countNonDtksBansos(4);

		$data['CDtksSembakoReguler'] = $this->dshm->countDtksBansos(5);
		$data['CNonDtksSembakoReguler'] = $this->dshm->countNonDtksBansos(5);

		$data['CDtksSembakoPerluasan'] = $this->dshm->countDtksBansos(6);
		$data['CNonDtksSembakoPerluasan'] = $this->dshm->countNonDtksBansos(6);

		$data['CDtksBlt'] = $this->dshm->countDtksBansos(7);
		$data['CNonDtksBlt'] = $this->dshm->countNonDtksBansos(7);
		
		$data['CDtksPkh'] = $this->dshm->countDtksBansos(8);
		$data['CNonDtksPkh'] = $this->dshm->countNonDtksBansos(8);

		$this->load->view('header',$data);
		$this->load->view('main');
		$this->load->view('footer');
	}

	public function GraphDtKsNonDtksKec(){

		$dataDashDtksKec = $this->dshm->GraphDtKsNonDtksKec();
		if(count($dataDashDtksKec) != 0){
			$output = array(
				'state'	=> true,
				'msg'	=> null,
				'data'	=> $dataDashDtksKec
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Data tidak ada',
				'data'	=> null
			);
			echo json_encode($output);
		}
	}


	public function FilterDashboard(){

		$kode_kec = $this->input->post('kode_kec');
		$kode_desa = $this->input->post('kode_desa');
		$status_verifikasi = $this->input->post('status_verifikasi');
		/* $kode_kec = 0;
		$kode_desa = 3201010001;
		$status_verifikasi = 0; */

		

		if($kode_desa == 0 || $kode_desa == -1){
			
			$cprovAll = $this->dshm->countAllAjaxKecBansos($kode_kec,2,$status_verifikasi);
			$CdtksProv = $this->dshm->countDtksAjaxKecBansos($kode_kec, 2,$status_verifikasi);
			$CNonDtksProv = $this->dshm->countNonDtksAjaxKecBansos($kode_kec, 2, $status_verifikasi);

			$CPresidenAll = $this->dshm->countAllAjaxKecBansos($kode_kec,1,$status_verifikasi);
			$CdtksPresiden = $this->dshm->countDtksAjaxKecBansos($kode_kec, 1,$status_verifikasi);
			$CNonDtksPresiden = $this->dshm->countNonDtksAjaxKecBansos($kode_kec, 1, $status_verifikasi);


			$CBupatiAll = $this->dshm->countAllAjaxKecBansos($kode_kec,3,$status_verifikasi);
			$CDtksBupati = $this->dshm->countDtksAjaxKecBansos($kode_kec, 3,$status_verifikasi);
			$CNonDtksBupati = $this->dshm->countNonDtksAjaxKecBansos($kode_kec, 3, $status_verifikasi);

			$CDdAll = $this->dshm->countAllAjaxKecBansos($kode_kec,4,$status_verifikasi);
			$CDtksDd = $this->dshm->countDtksAjaxKecBansos($kode_kec, 4,$status_verifikasi);
			$CNonDtksDd = $this->dshm->countNonDtksAjaxKecBansos($kode_kec, 4, $status_verifikasi);

			$CSembakoRegulerAll = $this->dshm->countAllAjaxKecBansos($kode_kec,5,$status_verifikasi);
			$CDtksSembakoReguler = $this->dshm->countDtksAjaxKecBansos($kode_kec, 5,$status_verifikasi);
			$CNonDtksSembakoReguler = $this->dshm->countNonDtksAjaxKecBansos($kode_kec, 5, $status_verifikasi);

			$CSembakoPerluasanAll = $this->dshm->countAllAjaxKecBansos($kode_kec,6,$status_verifikasi);
			$CDtksSembakoPerluasan = $this->dshm->countDtksAjaxKecBansos($kode_kec, 6,$status_verifikasi);
			$CNonDtksSembakoPerluasan = $this->dshm->countNonDtksAjaxKecBansos($kode_kec, 6, $status_verifikasi);

			$CSembakoPerluasanAll = $this->dshm->countAllAjaxKecBansos($kode_kec,6,$status_verifikasi);
			$CDtksSembakoPerluasan = $this->dshm->countDtksAjaxKecBansos($kode_kec, 6,$status_verifikasi);
			$CNonDtksSembakoPerluasan = $this->dshm->countNonDtksAjaxKecBansos($kode_kec, 6, $status_verifikasi);

			$CBltAll = $this->dshm->countAllAjaxKecBansos($kode_kec, 7 ,$status_verifikasi);
			$CDtksBlt = $this->dshm->countDtksAjaxKecBansos($kode_kec, 7,$status_verifikasi);
			$CNonDtksBlt = $this->dshm->countNonDtksAjaxKecBansos($kode_kec, 7, $status_verifikasi);

			$CPkhAll = $this->dshm->countAllAjaxKecBansos($kode_kec, 8 ,$status_verifikasi);
			$CDtksPkh = $this->dshm->countDtksAjaxKecBansos($kode_kec, 8,$status_verifikasi);
			$CNonDtksPkh = $this->dshm->countNonDtksAjaxKecBansos($kode_kec, 8, $status_verifikasi);



			$dt_tmp_arr = array(
				'CProvAll'	=> number_format($cprovAll,0,",","."),
				'CDtksProv'	=> number_format($CdtksProv,0,",","."),
				'CNonDtksProv'	=> number_format($CNonDtksProv,0,",","."),
				'CPresidenAll'	=> number_format($CPresidenAll,0,",","."),
				'CDtksPresiden'	=> number_format($CdtksPresiden,0,",","."),
				'CNonDtksPresiden'	=> number_format($CNonDtksPresiden,0,",","."),
				'CBupatiAll'	=> number_format($CBupatiAll,0,",","."),
				'CDtksBupati'	=> number_format($CDtksBupati,0,",","."),
				'CNonDtksBupati'	=> number_format($CNonDtksBupati,0,",","."),
				'CDdAll'	=> number_format($CDdAll,0,",","."),
				'CDtksDd'	=> number_format($CDtksDd,0,",","."),
				'CNonDtksDd'	=> number_format($CNonDtksDd,0,",","."),
				'CSembakoRegulerAll'	=> number_format($CSembakoRegulerAll,0,",","."),
				'CDtksSembakoReguler'	=> number_format($CDtksSembakoReguler,0,",","."),
				'CNonDtksSembakoReguler'	=> number_format($CNonDtksSembakoReguler,0,",","."),
				'CSembakoRegulerAll'	=> number_format($CSembakoRegulerAll,0,",","."),
				'CDtksSembakoReguler'	=> number_format($CDtksSembakoReguler,0,",","."),
				'CNonDtksSembakoReguler'	=> number_format($CNonDtksSembakoReguler,0,",","."),
				'CSembakoPerluasanAll'	=> number_format($CSembakoPerluasanAll,0,",","."),
				'CDtksSembakoPerluasan'	=> number_format($CDtksSembakoPerluasan,0,",","."),
				'CNonDtksSembakoPerluasan'	=> number_format($CNonDtksSembakoPerluasan,0,",","."),
				'CBltAll'	=> number_format($CBltAll,0,",","."),
				'CDtksBlt'	=> number_format($CDtksBlt,0,",","."),
				'CNonDtksBlt'	=> number_format($CNonDtksBlt,0,",","."),
				'CPkhAll'	=> number_format($CPkhAll,0,",","."),
				'CDtksPkh'	=> number_format($CDtksPkh,0,",","."),
				'CNonDtksPkh'	=> number_format($CNonDtksPkh,0,",","."),
			);
			$output = array(
				'state'	=> true,
				'msg'	=> 'Berhasil menampilkan data',
				'data'	=> $dt_tmp_arr
			);

			echo json_encode($output);
		}
		else
		{
			$cprovAll = $this->dshm->countAllAjaxDesaBansos($kode_desa,2,$status_verifikasi);
			$CdtksProv = $this->dshm->countDtksAjaxKecBansos($kode_desa, 2,$status_verifikasi);
			$CNonDtksProv = $this->dshm->countNonDtksAjaxDesaBansos($kode_desa, 2, $status_verifikasi);
			
			$CPresidenAll = $this->dshm->countAllAjaxDesaBansos($kode_desa,1,$status_verifikasi);
			$CdtksPresiden = $this->dshm->countDtksAjaxDesaBansos($kode_desa, 1,$status_verifikasi);
			$CNonDtksPresiden = $this->dshm->countNonDtksAjaxDesaBansos($kode_desa, 1, $status_verifikasi);


			$CBupatiAll = $this->dshm->countAllAjaxDesaBansos($kode_desa,3,$status_verifikasi);
			$CDtksBupati = $this->dshm->countDtksAjaxDesaBansos($kode_desa, 3,$status_verifikasi);
			$CNonDtksBupati = $this->dshm->countNonDtksAjaxDesaBansos($kode_desa, 3, $status_verifikasi);

			$CDdAll = $this->dshm->countAllAjaxDesaBansos($kode_desa,4,$status_verifikasi);
			$CDtksDd = $this->dshm->countDtksAjaxDesaBansos($kode_desa, 4,$status_verifikasi);
			$CNonDtksDd = $this->dshm->countNonDtksAjaxDesaBansos($kode_desa, 4, $status_verifikasi);

			$CSembakoRegulerAll = $this->dshm->countAllAjaxDesaBansos($kode_desa,5,$status_verifikasi);
			$CDtksSembakoReguler = $this->dshm->countDtksAjaxDesaBansos($kode_desa, 5,$status_verifikasi);
			$CNonDtksSembakoReguler = $this->dshm->countNonDtksAjaxDesaBansos($kode_desa, 5, $status_verifikasi);

			$CSembakoPerluasanAll = $this->dshm->countAllAjaxDesaBansos($kode_desa,6,$status_verifikasi);
			$CDtksSembakoPerluasan = $this->dshm->countDtksAjaxDesaBansos($kode_desa, 6,$status_verifikasi);
			$CNonDtksSembakoPerluasan = $this->dshm->countNonDtksAjaxDesaBansos($kode_desa, 6, $status_verifikasi);

			$CSembakoPerluasanAll = $this->dshm->countAllAjaxDesaBansos($kode_desa,6,$status_verifikasi);
			$CDtksSembakoPerluasan = $this->dshm->countDtksAjaxDesaBansos($kode_desa, 6,$status_verifikasi);
			$CNonDtksSembakoPerluasan = $this->dshm->countNonDtksAjaxDesaBansos($kode_desa, 6, $status_verifikasi);

			$CBltAll = $this->dshm->countAllAjaxDesaBansos($kode_desa, 7 ,$status_verifikasi);
			$CDtksBlt = $this->dshm->countDtksAjaxDesaBansos($kode_desa, 7,$status_verifikasi);
			$CNonDtksBlt = $this->dshm->countNonDtksAjaxDesaBansos($kode_desa, 7, $status_verifikasi);

			$CPkhAll = $this->dshm->countAllAjaxDesaBansos($kode_desa, 8 ,$status_verifikasi);
			$CDtksPkh = $this->dshm->countDtksAjaxDesaBansos($kode_desa, 8,$status_verifikasi);
			$CNonDtksPkh = $this->dshm->countNonDtksAjaxDesaBansos($kode_desa, 8, $status_verifikasi);



			$dt_tmp_arr = array(
				'CProvAll'	=> number_format($cprovAll,0,",","."),
				'CDtksProv'	=> number_format($CdtksProv,0,",","."),
				'CNonDtksProv'	=> number_format($CNonDtksProv,0,",","."),
				'CPresidenAll'	=> number_format($CPresidenAll,0,",","."),
				'CDtksPresiden'	=> number_format($CdtksPresiden,0,",","."),
				'CNonDtksPresiden'	=> number_format($CNonDtksPresiden,0,",","."),
				'CBupatiAll'	=> number_format($CBupatiAll,0,",","."),
				'CDtksBupati'	=> number_format($CDtksBupati,0,",","."),
				'CNonDtksBupati'	=> number_format($CNonDtksBupati,0,",","."),
				'CDdAll'	=> number_format($CDdAll,0,",","."),
				'CDtksDd'	=> number_format($CDtksDd,0,",","."),
				'CNonDtksDd'	=> number_format($CNonDtksDd,0,",","."),
				'CSembakoRegulerAll'	=> number_format($CSembakoRegulerAll,0,",","."),
				'CDtksSembakoReguler'	=> number_format($CDtksSembakoReguler,0,",","."),
				'CNonDtksSembakoReguler'	=> number_format($CNonDtksSembakoReguler,0,",","."),
				'CSembakoRegulerAll'	=> number_format($CSembakoRegulerAll,0,",","."),
				'CDtksSembakoReguler'	=> number_format($CDtksSembakoReguler,0,",","."),
				'CNonDtksSembakoReguler'	=> number_format($CNonDtksSembakoReguler,0,",","."),
				'CSembakoPerluasanAll'	=> number_format($CSembakoPerluasanAll,0,",","."),
				'CDtksSembakoPerluasan'	=> number_format($CDtksSembakoPerluasan,0,",","."),
				'CNonDtksSembakoPerluasan'	=> number_format($CNonDtksSembakoPerluasan,0,",","."),
				'CBltAll'	=> number_format($CBltAll,0,",","."),
				'CDtksBlt'	=> number_format($CDtksBlt,0,",","."),
				'CNonDtksBlt'	=> number_format($CNonDtksBlt,0,",","."),
				'CPkhAll'	=> number_format($CPkhAll,0,",","."),
				'CDtksPkh'	=> number_format($CDtksPkh,0,",","."),
				'CNonDtksPkh'	=> number_format($CNonDtksPkh,0,",","."),
			);
			$output = array(
				'state'	=> true,
				'msg'	=> 'Berhasil menampilkan data',
				'data'	=> $dt_tmp_arr
			);

			echo json_encode($output);


		}
	}


}
