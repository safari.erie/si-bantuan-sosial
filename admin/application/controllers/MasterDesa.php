<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterDesa extends CI_Controller {

	function __construct() {
        parent:: __construct();
		$this->load->model('Wilayah_model','wm');
		$user_id = $this->session->userdata(S_ID_USER);
		if($user_id == null){
			redirect(base_url()."admin/login");
		}
	}

	public function index()
	{
		
		$data['jsapp'] = array('admin/master_desa');
		

		$this->load->view('header',$data);
		$this->load->view('master_desa');
		$this->load->view('footer');
	}

	function get(){
		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		$namadesa = $this->input->post('nama_desa');
		

		$result = array();
        $recordsTotal = $this->wm->count_data_desa($namadesa);
		
        $row = array();
		$results = $this->wm->getDatadesas($length,$start, $def['order'], 'asc',$namadesa);
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				
                $row[] = array
				(
					"no"					=> $ii,
					'kodeKec'					=> $d->kode_kec,
					'namaKec'					=> $d->nama_kecamatan,
					'kodeDesa'					=> $d->kode_desa,
					'namaDesa'					=> $d->nama_desa,
					"aksi" 	    			=> '<button type="button" class="btn btn-info btn-flat btn-sm" onclick="EditDesa('.$d->kode_desa.');"><i class="fa fa-edit"></i> Edit Desa</button> '
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
	}

	function SaveDesa(){

		$id_desa = $this->input->post('id_desa');
		$kode_kec = $this->input->post('kode_kec');
		$kode_desa = $this->input->post('kode_desa');
		$nama_desa = $this->input->post('nama_desa');

		if($id_desa == -1){

			$data_insert = array(
				'kode_desa'	=> $kode_desa,
				'kode_kec'	=> $kode_kec,
				'name'	=> $kode_kec,
			);

			$tableName = 'master.tb_desa';
			$insert = $this->wm->saveDesa($id_desa,$data_insert,$tableName);
			if($insert){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Berhasil diinput'
				);

				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> false,
					'msg'	=> 'Data Gagal diinput'
				);

				echo json_encode($output);
			}
		}
	}
}
