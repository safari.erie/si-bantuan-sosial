<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MgtNews extends CI_Controller {
	function __construct() {
        parent:: __construct();
		$this->load->model('MgtNews_model','mnw_m');
		$this->load->model('Common_model','cmnm');
		$user_id = $this->session->userdata(S_ID_USER);
		if($user_id == null){
			redirect(base_url()."admin/login");
		}
	}


	function index(){
		$data['jsapp'] = array('admin/mgt_news');
		$this->load->view('header',$data);
		$this->load->view('mgt_news');
		$this->load->view('footer');
	}


	function get(){
		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		$judul_berita = $this->input->post('judul_berita');
		

		$result = array();
        $recordsTotal = $this->mnw_m->count_berita($judul_berita);
		
        $row = array();
		$results = $this->mnw_m->get_berita($length,$start, $def['order'], 'asc',$judul_berita);
		/* print_r($this->db->last_query());
		die; */
		$dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				$get_gambar_berita = $d->gambar_berita;
				$ret_gambar_berita="";
				if($get_gambar_berita == '' || $get_gambar_berita == null){
					$ret_gambar_berita .= '';
				}else{
					$file_path 	  = './uploads/berita/';
					$fileName = $get_gambar_berita;
					if(file_exists($file_path.$fileName)){
						$ret_gambar_berita .= '<img class="rounded" style="height: 10vh;" src="'. base_url().'admin/uploads/berita/'.$get_gambar_berita.'">';
					}else{
						$ret_gambar_berita .= '';
					}
				}
                $row[] = array
				(
					"no"				=> $ii,
					'judul_berita'		=> $d->judul_berita,
					"isi"				=> $d->isi_berita,
					"gambar"			=> $ret_gambar_berita,
					"created"			=>  $d->created,
					"aksi" 	    		=> '<button type="button" class="btn btn-info btn-flat btn-sm" onclick="EditBerita('.$d->id_berita.');"><i class="fa fa-edit"></i> Edit Berita</button> <button type="button" class="btn btn-danger btn-flat btn-sm" onclick="HapusBerita('.$d->id_berita.');"><i class="fa fa-trash"></i> Hapus Berita</button>'
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
	}


	function SaveBerita(){
		$this->load->library('Compress');  // load the codeginiter library
		$id_berita = $this->input->post('id_berita');
		$judul_berita = $this->input->post('judul_berita');
		$isi_berita = $this->input->post('isi_berita');
		$gambar_berita = $this->input->post('gambar_berita');

		$getLastId = $this->cmnm->common_last_id('id_berita','bansos.tx_berita');
		$lastIdBerita;
		if($getLastId->lastid == null){
			$lastIdBerita = 1;
		}else{
			$lastIdBerita = $getLastId->lastid + 1;
		}

		if($id_berita == -1){ //for insert

			$name_of_file;
			$file_path 	  = './uploads/berita/original';
			$config['upload_path'] =   $file_path;
			$config['allowed_types'] = 'jpg|jpeg|png|gif';  
			$config['file_name']        = "Berita_".time();
			$this->load->library('upload', $config);
			$this->upload->initialize($config);  
            if(!$this->upload->do_upload('foto_berita'))  
            {  
				
				$output = array(
					'state' => false,
					'msg'	=> $this->upload->display_errors(),
					'data'	=> null
				);
					echo json_encode($output); 
            }else{
				$data = $this->upload->data();  
				$nama_filenya 	= $data['file_name'];
				$this->load->library('Compress');  // load the codeginiter library
				//File to block
				$file0 = base_url().'admin/uploads/berita/original/'.$nama_filenya; // file that you wanna compress
				$new_name_image0 = $nama_filenya; // name of new file compressed
				$quality0 = 60; // Value that I chose
				$destination0 = base_url().'admin/uploads/berita/'; // This destination must be exist on your project
				$compress0 = new Compress();
				$compress0->file_url = $file0;
				$compress0->new_name_image = $new_name_image0;
				$compress0->quality = $quality0;
				$compress0->destination = $destination0;
				$result0 = $compress0->compress_image();
				
				/* $config['image_library'] = 'gd2';  
				$config['source_image'] = './uploads/berita/original/'.$data["file_name"];  
				$config['maintain_ratio'] = TRUE;  
				$config['quality'] = '100%';  
				$config['new_image'] = './uploads/berita/'.$data["file_name"];  
				$this->load->library('image_lib', $config);  
				$this->image_lib->initialize($config);
				$this->image_lib->resize();  
				$name_of_file = $data["file_name"];
				if (!$this->image_lib->resize()) {
				   echo $this->image_lib->display_errors();
			   }
				
			   $this->image_lib->clear(); */
			   
			   unlink('./uploads/berita/original/'.$data["file_name"]);
			   $data_berita = array(
					'id_berita'			=> $lastIdBerita,
					'judul_berita'		=> $judul_berita,
					'isi_berita'		=> $isi_berita,
					'gambar_berita'		=> $nama_filenya,
					'created_by' 		=> $this->session->userdata(S_ID_USER),
					'created_dt' 		=> date('Y-m-d H:i:s')
				);
				$insertBerita = $this->mnw_m->SaveNews($data_berita);
				if($insertBerita){
					$output = array(
						'state' => true,
						'msg'	=> 'Berita Berhasil Ditambahkan',
						'data'	=> null
					);
					 echo json_encode($output);
				}else{
					$output = array(
						'state' => false,
						'msg'	=> 'Berita Gagal ditambahkan',
						'data'	=> null
					);
					 echo json_encode($output);
				}

			}	
		}else{

			$dataBeritaById = $this->mnw_m->getBeritaById($id_berita);
			
			if (isset($_FILES['foto_berita']['name']) && !empty($_FILES['foto_berita']['name'])) {
				$name_of_file;
				$file_path 	  = './uploads/berita/original';
				$config['upload_path'] =   $file_path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';  
				$config['file_name']        = "Berita_".time();
				$this->load->library('upload', $config);
				$this->upload->initialize($config);  
				if(!$this->upload->do_upload('foto_berita'))  
				{  
					
					$output = array(
						'state' => false,
						'msg'	=> $this->upload->display_errors(),
						'data'	=> null
					);
					echo json_encode($output); 
				}else{
					$data = $this->upload->data();  
					$nama_filenya 	= $data['file_name'];
					$this->load->library('Compress');  // load the codeginiter library
					//File to block
					$file0 = base_url().'admin/uploads/berita/original/'.$nama_filenya; // file that you wanna compress
					$new_name_image0 = $nama_filenya; // name of new file compressed
					$quality0 = 60; // Value that I chose
					$destination0 = base_url().'admin/uploads/berita/'; // This destination must be exist on your project
					$compress0 = new Compress();
					$compress0->file_url = $file0;
					$compress0->new_name_image = $new_name_image0;
					$compress0->quality = $quality0;
					$compress0->destination = $destination0;
					$result0 = $compress0->compress_image();
					unlink('./uploads/berita/original/'.$data["file_name"]);	
					if($dataBeritaById->gambar_berita != null || $dataBeritaById->gambar_berita != ''){
						// cek file path is
						$path_target_remove = './uploads/berita/'.$dataBeritaById->gambar_berita;

						if(file_exists($path_target_remove)){
							unlink($path_target_remove);
						}
					}

				}

				$data_berita = array(
					'judul_berita'		=> $judul_berita,
					'isi_berita'		=> $isi_berita,
					'gambar_berita'		=> $nama_filenya,
					'update_by' 		=> $this->session->userdata(S_ID_USER),
					'update_dt' 		=> date('Y-m-d H:i:s')
				);
				$updateNews = $this->mnw_m->SaveNewsUpdate($data_berita,$id_berita);
				if($updateNews){
					$output = array(
						'state' => true,
						'msg'	=> 'Berita Berhasil Dirubah',
						'data'	=> null
					);
					 echo json_encode($output);
				}else{
					$output = array(
						'state' => false,
						'msg'	=> 'Berita Gagal Dirubah',
						'data'	=> null
					);
					 echo json_encode($output);
				}


			}else{
				
				$data_berita = array(
					'judul_berita'		=> $judul_berita,
					'isi_berita'		=> $isi_berita,
					'gambar_berita'		=> $dataBeritaById->gambar_berita,
					'update_by' 		=> $this->session->userdata(S_ID_USER),
					'update_dt' 		=> date('Y-m-d H:i:s')
				);
				$updateNews = $this->mnw_m->SaveNewsUpdate($data_berita,$id_berita);
				if($updateNews){
					$output = array(
						'state' => true,
						'msg'	=> 'Berita Berhasil Dirubah',
						'data'	=> null
					);
					 echo json_encode($output);
				}else{
					$output = array(
						'state' => false,
						'msg'	=> 'Berita Gagal Dirubah',
						'data'	=> null
					);
					 echo json_encode($output);
				}
			}

			

		}

	}


	function compress($source, $destination, $quality)
	{
		$info = getimagesize($source);
		if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source);
		elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source);
		elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source);
		imagejpeg($image, $destination, $quality);
		return $destination;
	}

	function getBeritaById(){
		$id_berita = $this->input->post('id_berita');

		$dataBeritaById = $this->mnw_m->getBeritaById($id_berita);

		if(count($dataBeritaById) != 0){
			$retBerita = '';
			$data_berita_gambar = $dataBeritaById->gambar_berita;
			if($data_berita_gambar == '' || $data_berita_gambar == null){
				$retBerita = null;
			}else{
				$file_path_berita = './uploads/berita/'.$data_berita_gambar;
				if(file_exists($file_path_berita)){
					$retBerita = $data_berita_gambar;
				}else{
					$retBerita = null;
				}
				
			}
			$dt = array(
				'id_berita'	=> $dataBeritaById->id_berita,
				'judul_berita'	=> $dataBeritaById->judul_berita,
				'isi_berita'	=> $dataBeritaById->isi_berita,
				'gambar'	=> $retBerita,
			);
			$output = array(
				'state'	=> true,
				'msg'	=> 'Data Berita ditemukan',
				'data'	=> $dt
			);

			echo json_encode($output);

		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Data Berita tidak ditemukan',
				'data'	=> null
			);
			echo json_encode($output);
		}
	}

	function NewsDelete(){
		
		$id_berita = $this->input->post('id_berita');
		$dataBeritaById = $this->mnw_m->getBeritaById($id_berita);		

		$deletNews = $this->mnw_m->NewsDelete($id_berita);
		if($deletNews){
			$data_berita_gambar = $dataBeritaById->gambar_berita;
			if($dataBeritaById->gambar_berita != null || $dataBeritaById->gambar_berita != ''){
				// cek file path is
				$path_target_remove = './uploads/berita/'.$dataBeritaById->gambar_berita;

				if(file_exists($path_target_remove)){
					unlink($path_target_remove);
				}
			}	
			$output = array(
				'state'	=> true,
				'msg'	=> 'Data Berita Berhasil Di hapus'

			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> true,
				'msg'	=> 'Data Berita Gagal Di hapus'

			);

			echo json_encode($output);
		}



	}


}
