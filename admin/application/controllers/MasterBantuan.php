<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterBantuan extends CI_Controller {

	function __construct() {
        parent:: __construct();
		$this->load->model('Master_bantuan_model','mbm');
		$user_id = $this->session->userdata(S_ID_USER);
		if($user_id == null){
			redirect(base_url()."admin/login");
		}
	}

	public function index()
	{
		
		$data['jsapp'] = array('admin/master_bantuan');
		

		$this->load->view('header',$data);
		$this->load->view('master_bantuan');
		$this->load->view('footer');
	}

	function get(){
		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		$namaBantuan = $this->input->post('nama_bantuan');
		

		$result = array();
        $recordsTotal = $this->mbm->count_data_bantuan($namaBantuan);
		
        $row = array();
		$results = $this->mbm->getData($length,$start, $def['order'], 'asc',$namaBantuan);
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				
                $row[] = array
				(
					"no"					=> $ii,
					'nama'					=> $d->nama_bantuan,
					"keterangan"			=> $d->keterangan,
					"status"				=> ($d->status == true) ? "Aktif" : "Tidak Aktif",
					"aksi" 	    			=> '<button type="button" class="btn btn-info btn-flat btn-sm" onclick="EdiBantuan('.$d->id_bantuan.');"><i class="fa fa-edit"></i> Edit Bantuan</button> <button type="button" class="btn btn-danger btn-flat btn-sm" onclick="HapusBantuan('.$d->id_bantuan.');"><i class="fa fa-trash"></i> Hapus Bantuan</button>'
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
	}

	function SaveBantuan(){


		$id_bantuan 	= $this->input->post('id_bantuan');
		$nama_bantuan 	= $this->input->post('nama_bantuan');
		$keterangan 	= $this->input->post('keterangan');
		$status 		= $this->input->post('status');
		
		if($id_bantuan == -1){
			
			$last_id = $this->mbm->getLast()->lastid;
		
			$data_insert = array(
				'id_bantuan'	=> $last_id,
				'nama_bantuan'	=> $nama_bantuan,
				'keterangan'	=> $keterangan,
				'status'		=> $status,
				'created_by' => $this->session->userdata(S_ID_USER),
				'created_dt' => date('Y-m-d H:i:s')
			);

			$table_name = 'ref.tb_bantuan';

			$insertBantuan = $this->mbm->saveBantuan($data_insert,$table_name,$id_bantuan);

			if($insertBantuan){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Berhasil diinput'
				);

				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> false,
					'msg'	=> 'Data Gagal diinput'
				);

				echo json_encode($output);
			}
		}else{
			$data_update = array(
				'nama_bantuan'	=> $nama_bantuan,
				'keterangan'	=> $keterangan,
				'status'		=> $status,
				'update_by' => $this->session->userdata(S_ID_USER),
				'updated_dt' => date('Y-m-d H:i:s')
			);
			$table_name = 'ref.tb_bantuan';
			$updateBantuan = $this->mbm->saveBantuan($data_update,$table_name,$id_bantuan);
			if($updateBantuan){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Berhasil dirubah'
				);

				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> false,
					'msg'	=> 'Data Gagal dirubah'
				);

				echo json_encode($output);
			}
		}
	}

	function GetDataBantuanById(){
		$id_bantuan = $this->input->post('id_bantuan');

		$dataBantuanId = $this->mbm->GetDataBantuanById($id_bantuan);
		if(count($dataBantuanId) != 0){
			$dataTmpArr = array(
				'id_bantuan'	=> $dataBantuanId->id_bantuan,
				'nama_bantuan'	=> $dataBantuanId->nama_bantuan,
				'keterangan'	=> $dataBantuanId->keterangan,
				'status'	=> $dataBantuanId->status,
			);

			$output = array(
				'state'	=> true,
				'msg'	=> null,
				'data'	=> $dataTmpArr
			);

			echo json_encode($output);
		}
	}

	function GetBantuanDataDelete(){

		$id_bantuan = $this->input->post('id_bantuan');
		$checkDataBansos = $this->mbm->checkBansosBantuan($id_bantuan);


		if(count($checkDataBansos) != 0){
			$output = array(
				'state'	=> false,
				'msg'	=> ' Tidak bisa di hapus, sudah ada di Penerima Bansos'
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> true,
				'msg'	=> ' '
			);
			echo json_encode($output);
		}
	}

	function BantuanDelete()
	{
		$id_bantuan = $this->input->post('id_bantuan');
		$arr_where = array(
			'id_bantuan'	=> $id_bantuan
		);
		$tableName = 'ref.tb_bantuan';
		$deleteBantuan = $this->mbm->deleteBantuan($arr_where,$tableName);
		if($deleteBantuan){ 
			$output = array(
				'state'	=> true,
				'msg'	=> ' Data Bantuan Berhasil dihapus'
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> ' Data Gagal Dihapus'
			);
			echo json_encode($output);
		}
	}
}
