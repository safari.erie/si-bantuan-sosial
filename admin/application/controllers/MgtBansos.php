<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'/third_party/spout/src/Spout/Autoloader/autoload.php';

//lets Use the Spout Namespaces
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class MgtBansos extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('bansos_model','bm');
		$this->load->model('verifikasi_model','vm');
		$user_id = $this->session->userdata(S_ID_USER);
		if($user_id == null){
			redirect(base_url()."admin/login");
		}
	}

	function index(){
		
		$data['jsapp'] = array('admin/mgt_bansos');
		$data['kodeWilayahLogin'] = $this->vm->getKodeWilayahByIdUser()->kode_wilayah;
		$this->load->view('header',$data);
		$this->load->view('mgtbansos');
		$this->load->view('footer');
	}

	function get(){

		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		$kode_kec = $this->input->post('kode_kec');
		$kode_desa = $this->input->post('kode_desa');
		$nik = $this->input->post('nik');
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		

		$result = array();
        $recordsTotal = $this->bm->count_data($kode_kec, $kode_desa, $nik);
		
        $row = array();
		$results = $this->bm->get_data($length,$start, $def['order'], 'asc',$kode_kec, $kode_desa, $nik);
		
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				$ret_status_verifikasi;
				if($d->status_verifikasi == 0){
					$ret_status_verifikasi = "Baru dan Menunggu Persetujuan";
				}else if($d->status_verifikasi == 1){
					$ret_status_verifikasi = "Verifikasi disetujui";
				}else if($d->status_verifikasi == -1){
					$ret_status_verifikasi = "Verifikasi ditolak";
				}
				
                $row[] = array
				(
					"no"				=> $ii,
					"nama" 				=> $d->nama,
					"nik" 				=> $d->nik,
					"alamat" 			=> $d->alamat,
					"kecamatan" 		=> $d->nama_kecamatan,
					"desa" 				=> $d->nama_desa,
					"nama_bantuan" 		=> $d->nama_bantuan,
					"status" 			=> $ret_status_verifikasi,
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
	}

	function get_bantuan_duplikat(){

		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		$kode_kec = $this->input->post('kode_kec');
		$kode_desa = $this->input->post('kode_desa');
		$nik = $this->input->post('nik');
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		

		$result = array();
        $recordsTotal = $this->bm->count_data_duplicate($kode_kec, $kode_desa, $nik);
		
        $row = array();
		$results = $this->bm->get_data_duplicate($length,$start, $def['order'], 'asc',$kode_kec, $kode_desa, $nik);
		
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				/* $ret_status_verifikasi;
				if($d->status_verifikasi == 0){
					$ret_status_verifikasi = "Baru dan Menunggu Persetujuan";
				}else if($d->status_verifikasi == 1){
					$ret_status_verifikasi = "Verifikasi disetujui";
				}else if($d->status_verifikasi == -1){
					$ret_status_verifikasi = "Verifikasi ditolak";
				} */
				
                $row[] = array
				(
					"no"				=> $ii,
					"nama" 				=> $d->nama,
					"nik" 				=> $d->nik,
					"alamat" 			=> $d->alamat,
					"kecamatan" 		=> $d->nama_kecamatan,
					"desa" 				=> $d->nama_desa,
					"nama_bantuan" 		=> $d->nama_bantuan,
					"status" 			=> $d->status_verifikasi,
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
	}


	function getDataKpm(){

		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
			'draw' => $this->input->post('draw'),
			'length' => $this->input->post('length'),
			'order' => $column[$idx_cols]['name'],
			'start' => $this->input->post('start'),
			'dir' => $order[0]['dir']
		);
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		$nama = $this->input->post('nama');

		$result = array();
		$recordsTotal = $this->bm->count_data_kpm($nama);
		
		$row = array();
		$results = $this->bm->getKpmByKodeWilayah($length,$start, $def['order'], 'asc',$nama);
		$dd = "";

		if (count($results) > 0) {
			$ii = $start;
			
			foreach ($results as $d) {
				$ii++;
				
				$row[] = array
				(
					"no"				=> $ii,
					"id_kpm"				=> $d->id_kpm,
					"nama" 			=> $d->nama,
					"nik" 			=> $d->nik,
					"alamat" 			=> $d->alamat,
				);
			}
		}
	
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);

	}

	function getBantuan(){

		$dataBantuan = $this->bm->getBantuan();

		if(count($dataBantuan) > 0){
			$output = array(
				'state'	=> true,
				'msg'	=> null,
				'data'	=> $dataBantuan
			);
			echo json_encode($output);
		}
	}

	function SaveBansos(){

		$id_bansos = $this->input->post('id_bansos');
		$id_kpm = $this->input->post('id_kpm');
		$id_bantuan = $this->input->post('id_bantuan');
		$getLastId = $this->bm->getLastIdBansos()->lastid;
		
		if($id_bansos == -1){
			
			$dataInsert = array(
				'id_bansos'	=> $getLastId,
				'id_bantuan'	=> $id_bantuan,
				'id_kpm'	=> $id_kpm,
				'status_verifikasi'	=> 0,
				'created_by' => $this->session->userdata(S_ID_USER),
				'created_dt' => date('Y-m-d H:i:s')
			);

			$insertsBantuan = $this->bm->SaveBantuan($dataInsert,$id_bansos, 'bansos.tx_bansos');

			if($insertsBantuan){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Bansos Berhasil Diinput'
				);
				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> false,
					'msg'	=> 'Data Bansos gagal Diinput'
				);
				echo json_encode($output);
			}
		}
	}

	public function DownloadData(){
		
		ini_set('memory_limit', '512M');
		$kode_kec = $this->input->get('kode_kec');
		$kode_desa = $this->input->get('kode_desa');
		$nik = $this->input->get('nik');		
		$getData = $this->bm->download_data($kode_kec,$kode_desa,'');
		

		if(count($getData) != 0){
			$writer = WriterFactory::create(Type::XLSX);
	
			//stream to browser
			$writer->openToBrowser("data_bansos_".date('Y-m-d H:i:s').".xlsx");


			$header1 = [
				'Data Bansos'

			];

			$headerStyle1 = (new StyleBuilder())
               ->setFontBold()
               ->build();
			
			   $writer->addRowWithStyle($header1,$headerStyle1); // add a row at a time
			$header2 = [
				'No',
				'Nama',
				'Nik',
				'Kecamatan',
				'Desa',
				'Alamat',	
				'Nama Bantuan',
			];

			$headerStyle2 = (new StyleBuilder())
               ->setFontBold()
               ->build();
			$writer->addRowWithStyle($header2,$headerStyle2); // add a row at a time
			
			$data = array();
			$no = 1;
			foreach($getData as $dt){

				$dt = array(
					$no++,$dt->nama, $dt->nik, $dt->nama_kecamatan, $dt->nama_desa, $dt->alamat, $dt->nama_bantuan
				);
				array_push($data,$dt);
			}

			$writer->addRows($data); // add multiple rows at a time
	
			$writer->close();
		}
		
	}


	public function tcDownload()
    {
        $writer = WriterFactory::create(Type::XLSX);
        //$writer = WriterFactory::create(Type::CSV); // for CSV files
        //$writer = WriterFactory::create(Type::ODS); // for ODS files

        //stream to browser
        $writer->openToBrowser("testing.xlsx");

        $header = [
            'No SP',
            'Tgl SP',
            'Payment'
        ];
        $writer->addRow($header); // add a row at a time

        $rows = [
            ['SP-903923', '2017-11-12', '35'],
            ['SP-6546', '2017-10-29', '7567'],
            ['SP-546546', '2017-08-29', '3453'],
            ['SP-675677', '2017-02-29', '4654'],
            ['SP-324344', '2017-12-29', '9789']
        ];

        $writer->addRows($rows); // add multiple rows at a time

        $writer->close();

	}
	

	public function TemplateImport(){
		
		ini_set('memory_limit', '512M');
		$writer = WriterFactory::create(Type::XLSX);

		//stream to browser
		$writer->openToBrowser("template_import_bansos_".date('Y-m-d H:i:s').".xlsx");

		
		
		$header2 = [
			'No',
			'Nama Lengkap',
			'Nik / No KTP',
			'Alamat',
			'Layak (1 = Layak, 0 = Tidak Layak)',
		];

		$headerStyle2 = (new StyleBuilder())
			->setFontBold()
			->build();
		$writer->addRowWithStyle($header2,$headerStyle2); // add a row at a time
		$writer->close();
		
	}

	



}

