<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('user_model','um');
		$user_id = $this->session->userdata(S_ID_USER);
		if($user_id == null){
			redirect(base_url()."admin/login");
		}
	}
	public function index()
	{
	
		$data['jsapp'] = array('admin/user_pengguna');
		$this->load->view('header',$data);
		$this->load->view('user');
		$this->load->view('footer');
	}

	function getRoles(){
		$Roles = $this->um->getRole();
		$result['Data'] = $Roles;
		echo json_encode($result);
	}


	function get() {
        $order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		$nama = $this->input->post('nama_pengguna');
		
		$result = array();
        $recordsTotal = $this->um->count_data_user($nama);
		
        $row = array();
		$results = $this->um->get_data($length,$start, $def['order'], 'asc',$nama);
		
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				$htmlButton ='';
				$htmlTitleStatus = '';
				if($d->status == 1){
					$htmlButton .= '<button type="button" class="btn btn-danger btn-flat btn-sm" onclick="Deaktivasi('.$d->id_user.');"><i class="fa fa-times"></i> Deaktivasi</button>  ';
					$htmlTitleStatus .= 'Aktif';
				}else{
					$htmlButton .= '<button type="button" class="btn btn-warning btn-flat btn-sm" onclick="Aktivasi('.$d->id_user.');"><i class="fa fa-check"></i> Aktivasi</button> ';
					$htmlTitleStatus .= 'Tidak Aktif';
				}
				$retButton = "";
				$retButton .= $htmlButton.' &nbsp;&nbsp; <button type="button" class="btn btn-info btn-flat btn-sm" id="btn_edit_pengguna" onclick="EditPengguna('.$d->id_user.');"><i class="fa fa-edit"></i> Edit </button>'; 
                $row[] = array
				(
					"no"		=> $ii,
					"username" 	=> $d->username,
					"email" 	=> $d->email,
					"fullName" 	=> $d->name,
					"role_name" => $d->role_name,
					"status" 	=> $htmlTitleStatus,
					"aksi" 		=> $retButton,
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
    }

	function SaveUser(){

		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$email = $this->input->post('email');
		$firstName = $this->input->post('firstName');
		$middleName = $this->input->post('middleName');
		$lastName = $this->input->post('lastName');
		$address = $this->input->post('address');
		$phoneNumber = $this->input->post('phoneNumber');
		$mobileNumber = $this->input->post('mobileNumber');
		$roles = $this->input->post('roles');
		
		$kodeWilayah = $this->input->post('kodeWilayah');

		$checkUser = $this->um->checkUserNameOrEmail($username,$email);
		if(count($checkUser) !=0 ){
			$arrResult = array(
				'state' => -1,
				'msg'   => 'Username or Email Sudah terdaftar'

			);
			$output = array
			(
				"data"           => $arrResult
			);
			echo json_encode($arrResult);
		}else{
			

		

			$data_insert = array(
				'username' => $username,
				'password' => $password,
				'email'	   => $email,
				'firstName' => $firstName,
				'middleName' => $middleName,
				'lastName'	=> $lastName,
				'address'	=> $address,
				'phoneNumber'	=> $phoneNumber,
				'mobileNumber'	=> $mobileNumber,
				'roles'	=> $roles,
				'kodeWilayah' => $kodeWilayah,
			);

			$inserUsers = $this->um->save_user($data_insert);
			$arrResult = array(
				'state' => 1,
				'msg'   => 'user berhasil diinput'

			);
			echo json_encode($arrResult);
		}

	}


	function SaveUserEdit(){
		$id_user = $this->input->post('idUser');
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$firstName = $this->input->post('firstName');
		$middleName = $this->input->post('middleName');
		$lastName = $this->input->post('lastName');
		$address = $this->input->post('address');
		$phoneNumber = $this->input->post('phoneNumber');
		$mobileNumber = $this->input->post('mobileNumber');
		$roles = $this->input->post('roles');
		
		$kodeWilayah = $this->input->post('kodeWilayah');

		$checkUser = $this->um->checkUserNameOrEmail($username,$email);
		
		if(count($checkUser) !=0 ){
			
			if($checkUser->id_user != $id_user){
				$arrResult = array(
				'state' => -1,
				'msg'   => 'Username or Email Sudah terdaftar'

				);
				$output = array
				(
					"data"           => $arrResult
				);
				echo json_encode($arrResult);
			}else{
				$data_update_tb_user = array(
					'username' => $username,
					'email'	   => $email,
					'updated_by' => $this->session->userdata(S_ID_USER),
					'updated_date' => date('Y-m-d H:i:s')
				);

				$data_update_tb_profile = array(
					
					'first_name' => $firstName,
					'middle_name' => $middleName,
					'last_name'	=> $lastName,
					'address'	=> $address,
					'phone_number'	=> $phoneNumber,
					'mobile_number'	=> $mobileNumber,
					'kode_wilayah' => $kodeWilayah,
				);

				$data_update_tb_role = array(
					
					'id_role'	=> $roles,
				);

				$updated_users = $this->um->SaveUpdateUser($id_user,$data_update_tb_user,$data_update_tb_profile,$data_update_tb_role);
				if($updated_users){
					$output = array(
						'state'	=> true,
						'msg'	=> 'Data Berhasil diupdate',
						'data'	=> null
					);
					echo json_encode($output);
				}else{
					$output = array(
						'state'	=> true,
						'msg'	=> 'Data Gagal diupdate',
						'data'	=> null
					);
					echo json_encode($output);
				}

			}
		}else{
			
			$data_update_tb_user = array(
				'username' => $username,
				'email'	   => $email,
				'updated_by' => $this->session->userdata(S_ID_USER),
				'updated_date' => date('Y-m-d H:i:s')
			);

			$data_update_tb_profile = array(
				
				'first_name' => $firstName,
				'middle_name' => $middleName,
				'last_name'	=> $lastName,
				'address'	=> $address,
				'phone_number'	=> $phoneNumber,
				'mobile_number'	=> $mobileNumber,
				'kode_wilayah' => $kodeWilayah,
			);

			$data_update_tb_role = array(
				
				'id_role'	=> $roles,
			);
			$updated_users = $this->um->SaveUpdateUser($id_user,$data_update_tb_user,$data_update_tb_profile,$data_update_tb_role);
			if($updated_users){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Berhasil diupdate',
					'data'	=> null
				);
				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Gagal diupdate',
					'data'	=> null
				);
				echo json_encode($output);
			}

			

		}
	}


	function Deaktivasi(){
		$id_user = $this->input->post('id_user');

		$data_update = array(
			'status'	=> 0,
			'updated_by'	=> $this->session->userdata(S_ID_USER),
			'updated_date' => date('Y-m-d H:i:s')
		);

		$updateDeaktivasi = $this->um->AktivasiDeaktivasi($id_user, $data_update);
		if($updateDeaktivasi){
			$output = array(
				'state'	=> true,
				'msg'	=> 'Data Berhasil di Dekatif',
				'data'	=> null,
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Data Gagal Dekatif',
				'data'	=> null,
			);
			echo json_encode($output);
		}

	}


	function Aktivasi(){
		$id_user = $this->input->post('id_user');

		$data_update = array(
			'status'	=> 1,
			'updated_by'	=> $this->session->userdata(S_ID_USER),
			'updated_date' => date('Y-m-d H:i:s')
		);

		$updateDeaktivasi = $this->um->AktivasiDeaktivasi($id_user, $data_update);
		if($updateDeaktivasi){
			$output = array(
				'state'	=> true,
				'msg'	=> 'Data Berhasil di Aktivasi',
				'data'	=> null,
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Data Gagal Aktivasi',
				'data'	=> null,
			);
			echo json_encode($output);
		}
	}


	function getUserById(){
		$id_user = $this->input->post('id_user');

		$data_user_by_id = $this->um->getUserById($id_user);
		if(count($data_user_by_id) != 0){

			$tmpDtArr = array(
				'id_user'	=> $data_user_by_id->id_user,
				'id_role'	=> $data_user_by_id->id_role,
				'username'	=> $data_user_by_id->username,
				'email'		=> $data_user_by_id->email,
				'first_name'		=> $data_user_by_id->first_name,
				'middle_name'		=> $data_user_by_id->middle_name,
				'last_name'		=> $data_user_by_id->last_name,
				'address'		=> $data_user_by_id->address,
				'phone_number'		=> $data_user_by_id->phone_number,
				'mobile_number'		=> $data_user_by_id->mobile_number,
				'kode_wilayah'		=> $data_user_by_id->kode_wilayah
			);
			$output = array(
				'state'	=> true,
				'msg'	=> 'data user  ditemukan',
				'data'	=> $tmpDtArr
			);
			echo json_encode($output);

		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'data user tidak ditemukan',
				'data'	=> null
			);
			echo json_encode($output);
		}
	}
}
