<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('verifikasi_model','vm');
        $this->load->model('Wilayah_model','wm');
		$this->load->model('Common_model','cmnm');
		$user_id = $this->session->userdata(S_ID_USER);
		if($user_id == null){
			redirect(base_url()."admin/login");
		}
	}

	function index(){
	
		$data['jsapp'] = array('admin/verifikasi');
		$this->load->view('header',$data);
		$this->load->view('verifikasi_bansos');
		$this->load->view('footer');

	}

	function add(){
		$data['jsapp'] = array('admin/addverifikasi');
		$this->load->view('header',$data);
		$this->load->view('add_verifikasi');
		$this->load->view('footer');

	}

	function getKpmByWilayah(){
		
		
		$data = $this->vm->getKpmByKodeWilayah();
		$result['Data'] = $data;
		echo json_encode($result);
	}

	function getDataKpm(){
		
		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		$nama = $this->input->post('nama');
		$kode_wilayah = $this->input->post('kode_wilayah');		
		$kode_kec = $this->input->post('kode_kec');
		
		$kode_desa = $this->input->post('kode_desa');
		$bantuan = $this->input->post('bantuan');
		
		$kodeWilayahLogin = $this->vm->getKodeWilayahByIdUser()->kode_wilayah;
		if($kode_wilayah != '' && $kode_wilayah !=0){
			if((strlen($kode_wilayah) == 7) && $kode_wilayah != 0){
				if($kodeWilayahLogin != $kode_kec){
					$row = array();
					$output = array
					(
						"draw"           => $def['draw'],
						"recordsTotal"   => 0,
						"recordsFiltered"=> 0,
						"data"           => $row
					);

					echo json_encode($output);
				}else{
					$result = array();					
					$recordsTotal = $this->vm->count_data_kpm($nama,$kode_kec,$kode_desa,$bantuan);		
					
					$row = array();
					$results = $this->vm->getKpmByKodeWilayah($length,$start, $def['order'], 'asc', $nama,$kode_kec,$kode_desa,$bantuan);
					$dd = "";

					if (count($results) > 0) {
						$ii = $start;
						
						foreach ($results as $d) {
							$ii++;
							
							$row[] = array
							(
								"no"				=> $ii,
								"id_kpm"				=> $d->id_kpm,
								"nama" 			=> $d->nama,
								"nik" 			=> $d->nik,
								"alamat" 			=> $d->alamat,
							);
						}
					}
				
					$output = array
					(
						"draw"           => $def['draw'],
						"recordsTotal"   => $recordsTotal,
						"recordsFiltered"=> $recordsTotal,
						"data"           => $row
					);

					echo json_encode($output);
				}
			}
			else if((strlen($kode_wilayah) > 7) && $kode_wilayah != 0){
				if($kodeWilayahLogin != $kode_desa){
					$row = array();
					$output = array
					(
						"draw"           => $def['draw'],
						"recordsTotal"   => 0,
						"recordsFiltered"=> 0,
						"data"           => $row
					);

					echo json_encode($output);
				}else{
					$result = array();					
					$recordsTotal = $this->vm->count_data_kpm($nama,$kode_kec,$kode_desa,$bantuan);		
					$row = array();
					$results = $this->vm->getKpmByKodeWilayah($length,$start, $def['order'], 'asc', $nama,$kode_kec,$kode_desa,$bantuan);
					$dd = "";

					if (count($results) > 0) {
						$ii = $start;
						
						foreach ($results as $d) {
							$ii++;
							
							$row[] = array
							(
								"no"				=> $ii,
								"id_kpm"				=> $d->id_kpm,
								"nama" 			=> $d->nama,
								"nik" 			=> $d->nik,
								"alamat" 			=> $d->alamat,
							);
						}
					}
				
					$output = array
					(
						"draw"           => $def['draw'],
						"recordsTotal"   => $recordsTotal,
						"recordsFiltered"=> $recordsTotal,
						"data"           => $row
					);

					echo json_encode($output);
				}
			}
		}else{

			$result = array();
			$recordsTotal = $this->vm->count_data_kpm($nama,$kode_kec,$kode_desa,$bantuan);	
						
			$row = array();
			$results = $this->vm->getKpmByKodeWilayah($length,$start, $def['order'], 'asc', $nama,$kode_kec,$kode_desa,$bantuan);
			
			$dd = "";

			if (count($results) > 0) {
				$ii = $start;
				
				foreach ($results as $d) {
					$ii++;
					
					$row[] = array
					(
						"no"				=> $ii,
						"id_kpm"			=> $d->id_kpm,
						"nama" 			    => $d->nama,
						"nik" 			    => $d->nik,
						"alamat" 			=> $d->alamat,
					);
				}
			}
		
			$output = array
			(
				"draw"           => $def['draw'],
				"recordsTotal"   => $recordsTotal,
				"recordsFiltered"=> $recordsTotal,
				"data"           => $row
			);

			echo json_encode($output);
		}

	}

	function saveVerifikasi(){

		$id_kpm = $this->input->post('id_kpm');
		$photo_ktp = $this->input->post('photo_ktp');
		$photo_kk = $this->input->post('photo_kk');
		$keterangan = $this->input->post('keterangan');
		$latitude = $this->input->post('latitude');
		$longitude = $this->input->post('longitude');
		
		//$lastidverifikasi = $this->vm->getLastVerifikasi()->lastidverifikasi;
		$getLastId = $this->cmnm->common_last_id('id_verifikasi','bansos.tb_verifikasi');
		$lastidverifikasi;
		if($getLastId->lastid == null){
			$lastidverifikasi = 1;
		}else{
			$lastidverifikasi = $getLastId->lastid + 1;
		}
	
		$photoKtp = "";
		if (isset($_FILES['photo_ktp']['name']) && !empty($_FILES['photo_ktp']['name'])) {
			$photoKtp = $this->do_upload_ktp($id_kpm);
		}else{
			$photoKtp = null;
		}

		$photoRumah = "";
		if (isset($_FILES['photo_kk']['name']) && !empty($_FILES['photo_kk']['name'])) {
			$photoRumah = $this->do_upload_kk($id_kpm);
		}else{
			$photoRumah = null;
		}
		
		$dataInsertVerifikasi = array(
			'id_verifikasi' => $lastidverifikasi,
			'id_kpm'	=> $id_kpm,
			'hasil'		=> $keterangan,
			'foto_ktp'	=> $photoKtp,
			'foto_rumah'	=> $photoRumah,
			'latitude'	=> $latitude,
			'longitude'	=> $longitude,
			'created_by' => $this->session->userdata(S_ID_USER),
			'created_dt' => date('Y-m-d H:i:s')
		); 
		$insert_data = $this->vm->saveVerifikasi($dataInsertVerifikasi,$latitude,$longitude);
		$output = array(
			'state'	=> $insert_data,
			'msg'	=> 'Data Berhasil diinput'
		);
		echo json_encode($output);
	}

	function SaveProsesEditVerifikasi(){
		$id_kpm = $this->input->post('id_kpm');
		$photo_ktp = $this->input->post('photo_ktp');
		$photo_kk = $this->input->post('photo_kk');
		$keterangan = $this->input->post('keterangan');
		$latitude = $this->input->post('latitude');
		$longitude = $this->input->post('longitude');
		$id_verifikasi = $this->input->post('id_verifikasi');

		$DataVerifikasiId = $this->vm->verifikasiById($id_verifikasi);
	
		$photoKtp = "";
		if (isset($_FILES['photo_ktp']['name']) && !empty($_FILES['photo_ktp']['name'])) {
			if($DataVerifikasiId->foto_ktp != null){
				$file_path 	  = './uploads/kk_ktp/';
				$fileNameKtp = $DataVerifikasiId->foto_ktp;
				
				if(file_exists($file_path.$fileNameKtp)){
					unlink($file_path.$fileNameKtp);
				}

			}
			$photoKtp = $this->do_upload_ktp($id_kpm);
			
		}else{
			$photoKtp = $DataVerifikasiId->foto_ktp;
		}

		$photoRumah = "";
		if (isset($_FILES['photo_kk']['name']) && !empty($_FILES['photo_kk']['name'])) {
			if($DataVerifikasiId->foto_ktp != null){
				$file_path 	  = './uploads/kk_ktp/';
				$fileNamerumah = $DataVerifikasiId->foto_rumah;

				if(file_exists($file_path.$fileNamerumah)){
					unlink($file_path.$fileNamerumah);
				}
				

			}
			$photoRumah = $this->do_upload_kk($id_kpm);
		}else{
			$photoRumah = $DataVerifikasiId->foto_rumah;
		}
		
		$dataUpdate = array(
			'id_kpm'	=> $id_kpm,
			'hasil'		=> $keterangan,
			'foto_ktp'	=> $photoKtp,
			'foto_rumah'	=> $photoRumah,
			'latitude'	=> $latitude,
			'longitude'	=> $longitude,
		); 
		$updateProses = $this->vm->SaveUpdateVerifikasi($dataUpdate,$latitude,$longitude,$id_verifikasi);
	
		echo json_encode($updateProses);
	}


	function do_upload_ktp($photo) 
	{	
		
		
		$file_path 	  = './uploads/kk_ktp';
		
		$temp_file_lama = $photo;
		if ($temp_file_lama != '') 
		{
			$filep = $file_path . $temp_file_lama;
			if (file_exists($filep)) {
				unlink($filep);
			}
		}
			
		$config['file_name']        = "KTP_".$photo.'_'.time();
		$config['upload_path'] 		= $file_path;
		$config['allowed_types'] 	= 'jpg|jpeg|gif|png';
		$config['max_size'] 		= '20000';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('photo_ktp')){
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		}else{
			$file_data 		= $this->upload->data();
			$nama_filenya 	= $file_data['file_name'];
		
			return $nama_filenya;
		}

		
		
	}
	
	function do_upload_kk($photo) 
	{	
		$file_path 	  = './uploads/kk_ktp';
		$config['file_name']        = "FOTO_RUMAH_".$photo.'_'.time();	
		$config['upload_path'] 		= $file_path;
		$config['allowed_types'] 	= 'jpg|jpeg|gif|png';
		$config['max_size'] 		= '20000';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( !$this->upload->do_upload('photo_kk')){
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
			die;
		}else{
			$file_data 		= $this->upload->data();
			$nama_filenya 	= $file_data['file_name'];
		
			return $nama_filenya;
		}

		
		
	}
	

	function get(){
		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		

		$result = array();
        $recordsTotal = $this->vm->countVerifikasi();
		
        $row = array();
		$results = $this->vm->getDataVerifikasi($length,$start, $def['order'], 'asc');
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				$html_button = "";
				$html_title_status = "";
				if($d->status_verifikasi == 1){
					$html_button .= '<a href="javascript:void(0)" class="btn btn-icon icon-left btn-secondary"><i class="far fa-user"></i> Edit</a>';
					$html_title_status = 'Sudah Verifikasi';
				}else if($d->status_verifikasi == 0){
					$html_button .= '<a href="javascript:void(0)" class="btn btn-icon icon-left btn-info" onclick="getDetailVerifikasiById('.$d->id_verifikasi.');"> <i class="fa fa-check"></i>  Edit </a>';
					$html_title_status = 'Belum Verifikasi';
				}else{
					$html_button .= '<a href="javascript:void(0)" class="btn btn-icon icon-left btn-secondary"><i class="far fa-user"></i> Edit</a>';
					$html_title_status = 'Ditolak';
				}
                $row[] = array
				(
					"no"					=> $ii,
					'nama'					=> $d->nama,
					"nik"					=> $d->nik,
					"alamat"					=> $d->alamat,
					"nama_bantuan"					=> $d->nama_bantuan,
					"status_verifikasi" 	=> $html_title_status,
					"keterangan" 			=> $d->keterangan,
					"foto_ktp" 				=> '<img class="rounded" style="height: 10vh;" src="'. base_url().'admin/uploads/kk_ktp/'.$d->foto_ktp.'">',
					"foto_rumah" 			=> '<img class="rounded" style="height: 10vh;" src="'. base_url().'admin/uploads/kk_ktp/'.$d->foto_rumah.'">',
					'aksi'					=> $html_button
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
	}

	function getBantuan(){	
		
		$data = $this->wm->getBantuan();
		$result['Data'] = $data;
		echo json_encode($result);
	}

	function checkUserLoginWilayah(){
		$kodeWilayah = $this->vm->getKodeWilayahByIdUser();
		if($kodeWilayah->kode_wilayah != null){
			$output = array(
				'state'	=> true,
				'msg'	=> null,
				'data'	=> $kodeWilayah->kode_wilayah,
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> true,
				'msg'	=> 'Login Admin atau operator',
				'data'	=> null
			);
			echo json_encode($output);
		}
	}


	function WilayahKec(){
		$kodeWilayah = $this->vm->getKodeWilayahByIdUser();
		$dataKec = $this->wm->getKec();
		
		if(count($dataKec) != 0){
			$output = array(
				'state'	=> true,
				'msg'	=> null,
				'kode_wilayah' => $kodeWilayah,
				'data_kec' => $dataKec,
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> true,
				'msg'	=> null,
				'kode_wilayah' => $kodeWilayah,
				'data_kec' => null,
			);
			echo json_encode($output);

		}
		

	}

}
