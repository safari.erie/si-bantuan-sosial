<?php


class User_model extends CI_Model
{
	
	
	public function get_data($start = ''
	, $length = ''
	, $order = ''
	, $dir = 'asc'
	, $nama = ''){
		$nama_search = strtolower($nama);
		$sql = "
			SELECT a.id_user,a.username,a.password,a.email,a.status,
				b.first_name,b.middle_name,b.last_name, concat(b.first_name,' ',b.middle_name,' ',b.last_name) as name,b.address,
				b.phone_number,b.mobile_number,b.kode_wilayah,
				d.role_name
			from tb_user a
			inner join tb_user_profile b on a.id_user = b.id_user
			inner join tb_user_role c on a.id_user = c.id_user
			inner join tb_role d on c.id_role = d.id_role
			where 1 = 1
		";

		if($nama != ''){
			$sql .= " AND LOWER(concat(b.first_name,' ',b.middle_name,' ',b.last_name)) LIKE '%{$nama_search}%' ";
		}

		$sql .= " limit " . $start . "OFFSET " . $length;
        return $this->db->query($sql)->result();

	}

	function count_data_user($start = ''
	, $length = ''
	, $order = ''
	, $dir = 'asc'
	, $nama = '')
	{
		$nama_search = strtolower($nama);
		$sql = "			
			SELECT count(*) as cnt
			from tb_user a
			inner join tb_user_profile b on a.id_user = b.id_user
			inner join tb_user_role c on a.id_user = c.id_user
			inner join tb_role d on c.id_role = d.id_role
		";

		if($nama != ''){
			$sql .= " AND LOWER(concat(b.first_name,' ',b.middle_name,' ',b.last_name)) LIKE '%{$nama_search}%' ";
		}

		return $this->db->query($sql)->row()->cnt;
	}

	public function getRole(){

		$sql = "
			SELECT 
			id_role,role_name 
			FROM public.tb_role
		";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function checkUserNameOrEmail($username,$email){
		$username_lower = strtolower($username);
		$email_lower = strtolower($email);
		$sql = "
			select id_user,username,email from tb_user
			where LOWER(username) = '$username_lower' or LOWER(email) = '$email_lower'
		";

		$query = $this->db->query($sql);
		return $query->row();

	}

	public function save_user($data_user){

		$lastIdUser = $this->getLastIdUser()->lastiduser;

		$dt_tbl_user = array(
			'id_user' => $lastIdUser,
			'username'	=> $data_user['username'],
			'password'	=> $data_user['password'],
			'email'		=> $data_user['email'],
			'status'	=> 1,
			'created_date' => date('Y-m-d H:i:s')
		);
		$inserUser = $this->db->insert('tb_user',$dt_tbl_user);

		$dt_tb_user_info = array(
			'id_user'	=> $lastIdUser,
			'first_name'	=> $data_user['firstName'],
			'middle_name'	=> $data_user['middleName'],
			'last_name'	=> $data_user['lastName'],
			'address'	=> $data_user['address'],
			'phone_number'	=> $data_user['phoneNumber'],
			'mobile_number'	=> $data_user['mobileNumber'],
			'kode_wilayah'	=> $data_user['kodeWilayah'],
		);

		$insertUserProfile = $this->db->insert('tb_user_profile', $dt_tb_user_info);

		$dt_user_role = array(
			'id_user' => $lastIdUser,
			'id_role' => $data_user['roles']
		);

		$insertUserRole = $this->db->insert('tb_user_role', $dt_user_role);
		$user_detail = array(
            'last_user' => $lastIdUser
        );

        return $user_detail; 
	}

	public function getLastIdUser(){

		$sql = "
			select max(id_user)+1 as lastIdUser from tb_user
		";
		$query = $this->db->query($sql)->row();
		return $query;
	}

	public function SaveUpdateUser($idUser, $data_user, $data_user_profile, $data_roles){
		$this->db->trans_start();
		$this->db->where('id_user',$idUser);
		$this->db->update('public.tb_user',$data_user);

		$this->db->where('id_user',$idUser);
		$this->db->update('public.tb_user_profile',$data_user_profile);

		$this->db->where('id_user',$idUser);
		$this->db->update('public.tb_user_role',$data_roles);

		$this->db->trans_complete();
		$result;
		if (!$this->db->affected_rows()) {
			$result = false;
		} else {
			$result = true;
		}
		return $result;


	}

	public function AktivasiDeaktivasi($idUser, $data){
		$this->db->where('id_user',$idUser);
		$this->db->update('public.tb_user', $data);
		$result;
		if (!$this->db->affected_rows()) {
			$result = false;
		} else {
			$result = true;
		}
		return $result;

	}

	public function getUserById($idUser){
		$sql = "
			select a.id_user,a.username,a.email,
					b.first_name,b.middle_name,b.last_name,b.address,b.phone_number,b.mobile_number,b.kode_wilayah,
					d.id_role,d.role_name
			from tb_user a
			inner join tb_user_profile b on a.id_user = b.id_user
			inner join tb_user_role c on a.id_user = c.id_user
			inner join tb_role d on c.id_role = d.id_role
			where a.id_user = '{$idUser}'";

		$query = $this->db->query($sql)->row();
		return $query;
	}

}
