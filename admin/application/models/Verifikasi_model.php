<?php


class Verifikasi_model extends CI_Model
{

	function getKpmByKodeWilayah(
		 $start = ''
		, $length = ''
		, $order = ''
		, $dir = 'asc'
		, $nama = ''
		, $kode_kec = ''
		, $kode_desa = '' 
		, $jenis_bantuan = ''
	){

		
		$whereCondition;
		if(($kode_kec == '' || $kode_kec == 0) && $kode_desa == 0 && $jenis_bantuan == 0){

			$kodeWilayah = $this->getKodeWilayahByIdUser()->kode_wilayah;
			
			if($kodeWilayah == null || $kodeWilayah == 0 || $kodeWilayah == ''){
				$whereCondition = "";
			}
			else if(strlen($kodeWilayah) == 7){
				$whereCondition = " AND d.kode_kec = $kodeWilayah";
			}else{
				$whereCondition = " AND a.kode_desa = ".$kodeWilayah;
			}

			$sql = " 
				select a.id_kpm,a.nama,a.nik,a.alamat,
						b.id_bansos,b.status_verifikasi,b.keterangan,
						c.kode_desa, c.name as nama_desa,
						d.kode_kec, d.name as nama_kec
				from bansos.tm_kpm a
				inner join bansos.tx_bansos b on a.id_kpm = b.id_kpm
				inner join master.tb_desa c on a.kode_desa = c.kode_desa
				inner join master.tb_kec d on c.kode_kec = d.kode_kec
				where b.status_verifikasi = 0
				and  NOT EXISTS (select * from bansos.tb_verifikasi where id_kpm = a.id_kpm)
				$whereCondition
			";

			$sql .= " limit " . $start . " OFFSET " . $length;
			return $this->db->query($sql)->result();

		}else{
			$sql = "
				select a.id_kpm,a.nama,a.nik,a.alamat,
						b.id_bansos,b.status_verifikasi,b.keterangan,
						c.kode_desa, c.name as nama_desa,
						d.kode_kec, d.name as nama_kec
				from bansos.tm_kpm a
				inner join bansos.tx_bansos b on a.id_kpm = b.id_kpm
				inner join master.tb_desa c on a.kode_desa = c.kode_desa
				inner join master.tb_kec d on c.kode_kec = d.kode_kec
				where b.status_verifikasi = 0
				and  NOT EXISTS (select * from bansos.tb_verifikasi where id_kpm = a.id_kpm) ";

			if($kode_kec != 0 && $kode_desa == 0 && $jenis_bantuan == 0){
				$sql .= " AND d.kode_kec = $kode_kec";
			}

			if($kode_kec != 0 && $kode_desa != 0 && $jenis_bantuan != 0){
				$sql .=  " AND d.kode_kec = $kode_kec AND c.kode_desa =$kode_desa AND b.id_bantuan = $jenis_bantuan";
			}

			if($kode_kec != 0 && $kode_desa != 0 && $jenis_bantuan == 0){
				$sql .=  " AND d.kode_kec = $kode_kec AND c.kode_desa =$kode_desa";
			}

			if($kode_kec != 0 && $kode_desa == 0 && $jenis_bantuan != 0){
				$sql .=  " AND d.kode_kec = $kode_kec AND b.id_bantuan = $jenis_bantuan";
			}

			$sql .= " limit " . $start . " OFFSET " . $length;
			return $this->db->query($sql)->result();
		}

		

		

	}

	function count_data_kpm($nama, $kode_kec, $kode_desa, $jenis_bantuan){

		if(($kode_kec == '' || $kode_kec == 0) && $kode_desa == 0 && $jenis_bantuan == 0){
			$whereCondition;
			$kodeWilayah = $this->getKodeWilayahByIdUser()->kode_wilayah;
			
			if($kodeWilayah == null || $kodeWilayah == 0 || $kodeWilayah == ''){
				$whereCondition = " ";
			}
			else if(strlen($kodeWilayah) == 7){
				$whereCondition = " AND d.kode_kec = $kodeWilayah";
			}else{
				$whereCondition = " AND a.kode_desa = ".$kodeWilayah;
			}
			$sql = "								
				SELECT count(*) as cnt
				FROM bansos.tm_kpm a
				INNER JOIN bansos.tx_bansos b on a.id_kpm = b.id_kpm
				INNER JOIN master.tb_desa c on a.kode_desa = c.kode_desa
				INNER JOIN master.tb_kec d on c.kode_kec = d.kode_kec
				where b.status_verifikasi = 0	
				and  NOT EXISTS (select * from bansos.tb_verifikasi where id_kpm = a.id_kpm)
				$whereCondition
			";
			return $this->db->query($sql)->row()->cnt;
		}else{
			$sql = "
				SELECT count(*) as cnt
				FROM bansos.tm_kpm a
				INNER JOIN bansos.tx_bansos b on a.id_kpm = b.id_kpm
				INNER JOIN master.tb_desa c on a.kode_desa = c.kode_desa
				INNER JOIN master.tb_kec d on c.kode_kec = d.kode_kec
				where b.status_verifikasi = 0	
				and  NOT EXISTS (select * from bansos.tb_verifikasi where id_kpm = a.id_kpm)
			";
			if($kode_kec != 0 && $kode_desa == 0 && $jenis_bantuan == 0){
				$sql .=  " AND d.kode_kec = $kode_kec";
			}
			
			if($kode_kec != 0 && $kode_desa == 0 && $jenis_bantuan != 0){
				$sql .=  " AND d.kode_kec = $kode_kec AND b.id_bantuan = $jenis_bantuan";
			}
			if($kode_kec != 0 && $kode_desa != 0 && $jenis_bantuan == 0){
				$sql .=  " AND d.kode_kec = $kode_kec AND c.kode_desa =$kode_desa";
			}
			if($kode_kec != 0 && $kode_desa != 0 && $jenis_bantuan != 0){
				$sql .=  " AND d.kode_kec = $kode_kec AND c.kode_desa =$kode_desa AND b.id_bantuan = $jenis_bantuan";
			}

			if($kode_kec == 0 && $kode_desa = 0 && $jenis_bantuan != 0){
				$sql .=  " AND b.id_bantuan = $jenis_bantuan";
			}
			if($nama != ''){
				$sql .= " AND c.nama = '{$nama}'";
			}
			return $this->db->query($sql)->row()->cnt;
		}


		
		


	}

	function getKodeWilayahByIdUser(){
		$id_user = $this->session->userdata(S_ID_USER);
		$sql = "
			SELECT kode_wilayah from tb_user_profile 
			where id_user = $id_user;
		"; 

		$query = $this->db->query($sql)->row();
		return $query;
	}

	function getLastVerifikasi(){
		$sql = "
			select count(*) + 1 as  lastIdVerifikasi from bansos.tb_verifikasi
		";
		$query = $this->db->query($sql)->row();
		return $query;
	}

	function saveVerifikasi($dataVerifikasi,$lat,$lon){

		$checkVerifikasi = $this->checkVerifikasi($dataVerifikasi['id_kpm']);

		if(count($checkVerifikasi) == 0){
			$this->db->insert('bansos.tb_verifikasi', $dataVerifikasi);
		}else{
			$updateVerifikasi = array(
				'hasil'	=> $dataVerifikasi['hasil'],
				'foto_ktp'	=> $dataVerifikasi['foto_ktp'],
				'foto_rumah'	=> $dataVerifikasi['foto_rumah'],
				'updated_by' => $this->session->userdata(S_ID_USER),
				'updated_dt' => date('Y-m-d H:i:s')
			);

			$this->db->where('id_verifikasi',$checkVerifikasi->id_verifikasi);
			$this->db->update('bansos.tb_verifikasi', $updateVerifikasi);

		}
		

		$dataUpdate = array(
			'status_verifikasi' => 0,
			'keterangan'	=> $dataVerifikasi['hasil'],
		);
		$this->db->where('id_kpm',$dataVerifikasi['id_kpm']);
		$this->db->update('bansos.tx_bansos', $dataUpdate);

		return $this->db->affected_rows() > 0;

	}


	function SaveUpdateVerifikasi($dataVerifikasi,$lat,$lon,$idVerifikasi){
		
		$updateVerifikasi = array(
			'hasil'	=> $dataVerifikasi['hasil'],
			'latitude'	=> $lat,
			'longitude'	=> $lon,
			'foto_ktp'	=> $dataVerifikasi['foto_ktp'],
			'foto_rumah'	=> $dataVerifikasi['foto_rumah'],
			'updated_by' => $this->session->userdata(S_ID_USER),
			'updated_dt' => date('Y-m-d H:i:s')
		);

		$this->db->trans_start();

		$this->db->where('id_verifikasi',$idVerifikasi);
		$this->db->update('bansos.tb_verifikasi', $updateVerifikasi);

		$dataUpdate = array(
			'status_verifikasi' => 0,
			'keterangan'	=> $dataVerifikasi['hasil'],
		);
		$this->db->where('id_kpm',$dataVerifikasi['id_kpm']);
		$this->db->update('bansos.tx_bansos', $dataUpdate);
		
		$this->db->trans_complete();
		$result;
		if (!$this->db->affected_rows()) {
			$result = false;
		} else {
			$result = true;
		}

		$output = array(
			'state' => $result,
			'msg'	=> '',
		);

		return $output;

	}

	function checkVerifikasi($idVerifikasi){

		$sql = "select id_verifikasi,id_kpm, hasil
				from bansos.tb_verifikasi 
				where id_kpm = ".$idVerifikasi;
		$query = $this->db->query($sql)->row();
		return $query;
	}

	function getDataVerifikasi(
		$start = ''
		, $length = ''
		, $order = ''
		, $dir = 'asc'
	){

		$sql = "
			select a.id_verifikasi,a.id_kpm,a.hasil,a.foto_ktp,a.foto_rumah,
					b.id_bansos,b.status_verifikasi,b.keterangan,
					c.nama,c.nik,c.alamat,
					d.nama_bantuan
			from bansos.tb_verifikasi a
			inner join bansos.tx_bansos b on a.id_kpm = b.id_kpm
			inner join bansos.tm_kpm c on b.id_kpm = c.id_kpm
			inner join  ref.tb_bantuan d on b.id_bantuan = d.id_bantuan
		";

		$sql .= " limit " . $start . "OFFSET " . $length;
        return $this->db->query($sql)->result();

		
	}

	function countVerifikasi(){
		$sql = "
			select count(*) as cnt
			from bansos.tb_verifikasi a
			inner join bansos.tx_bansos b on a.id_kpm = b.id_kpm
			inner join bansos.tm_kpm c on b.id_kpm = c.id_kpm
			inner join  ref.tb_bantuan d on b.id_bantuan = d.id_bantuan
		";
		
		return $this->db->query($sql)->row()->cnt;
	}

	function get_approve_verifikasi(
		$start = ''
		, $length = ''
	    , $order = ''
		, $dir = 'asc'
		, $kode_kec = ''
		, $kode_desa = ''
		, $nik = ''
	){

		$sql = "
			SELECT a.id_verifikasi,a.hasil,a.foto_ktp,a.foto_rumah,
				CONCAT(b.first_name,' ',b.middle_name,' ',b.last_name) as created,
				c.id_bansos,c.id_bantuan,c.status_verifikasi,
				d.nama_bantuan,d.keterangan,d.status,
				e.nama,e.nik,e.alamat,e.layak,e.status,e.dtks,a.latitude,a.longitude,
				f.kode_desa,f.name as nama_desa,
				g.kode_kec,g.name as nama_kec
			FROM bansos.tb_verifikasi a
			INNER JOIN tb_user_profile b on a.created_by = b.id_user
			INNER JOIN bansos.tx_bansos c on a.id_kpm = c.id_kpm
			INNER JOIN ref.tb_bantuan d on c.id_bantuan = d.id_bantuan
			INNER JOIN bansos.tm_kpm e on a.id_kpm = e.id_kpm
			INNER JOIN master.tb_desa f on e.kode_desa = f.kode_desa
			INNER JOIN master.tb_kec g on f.kode_kec = g.kode_kec
			WHERE c.status_verifikasi = 0
		";

		if($kode_kec != 0 && $kode_desa == 0 && $nik == ''){
			$sql .= " AND g.kode_kec = '{$kode_kec}'";
		}

		if($kode_kec != 0 && $kode_desa != 0  && $nik == ''){
			$sql .= " AND g.kode_kec = '{$kode_kec}' AND f.kode_desa = '{$kode_desa}'";
		}

		if($kode_kec != 0 && $kode_desa != 0 && $nik != ''){
			$sql .= " AND g.kode_kec = {$kode_kec} AND f.kode_desa = {$kode_desa} AND e.nik LIKE '{$nik}' ";
		}

		if($kode_kec == 0 && $kode_desa == 0  && $nik != ''){
			$sql .= " AND e.nik LIKE '{$nik}'";
		}
		
		$sql .= " limit " . $start . " OFFSET " . $length;
        return $this->db->query($sql)->result();


	}

	function count_data_verifikasi_approve(	 
		$kode_kec = ''
	, $kode_desa = ''
	, $nik = ''){

		$sql = "
			SELECT count(*) as cnt
			FROM bansos.tb_verifikasi a
			INNER JOIN tb_user_profile b on a.created_by = b.id_user
			INNER JOIN bansos.tx_bansos c on a.id_kpm = c.id_kpm
			INNER JOIN ref.tb_bantuan d on c.id_bantuan = d.id_bantuan
			INNER JOIN bansos.tm_kpm e on a.id_kpm = e.id_kpm
			INNER JOIN master.tb_desa f on e.kode_desa = f.kode_desa
			INNER JOIN master.tb_kec g on f.kode_kec = g.kode_kec
			WHERE c.status_verifikasi = 0
		";

		if($kode_kec != 0 && $kode_desa == 0 && $nik == ''){
			$sql .= " AND g.kode_kec = '{$kode_kec}'";
		}

		if($kode_kec != 0 && $kode_desa != 0  && $nik == ''){
			$sql .= " AND g.kode_kec = '{$kode_kec}' AND f.kode_desa = '{$kode_desa}'";
		}

		if($kode_kec != 0 && $kode_desa != 0 && $nik != ''){
			$sql .= " AND g.kode_kec = {$kode_kec} AND f.kode_desa = {$kode_desa} AND e.nik LIKE '{$nik}' ";
		}

		if($kode_kec == 0 && $kode_desa == 0  && $nik != ''){
			$sql .= " AND e.nik LIKE '{$nik}'";
		}

		return $this->db->query($sql)->row()->cnt;
		

	}


	function verifikasiById($idVerifikasi){

		$sql = "						
			SELECT a.id_verifikasi,a.hasil,a.foto_ktp,a.foto_rumah,a.id_kpm,
				CONCAT(b.first_name,' ',b.middle_name,' ',b.last_name) as created,
				c.id_bansos,c.id_bantuan,c.status_verifikasi,
				d.nama_bantuan,d.keterangan,d.status,
				e.nama,e.nik,e.alamat,e.layak,e.status,e.dtks,a.latitude,a.longitude,
				f.kode_desa,f.name as nama_desa,
				g.kode_kec,g.name as nama_kec
			FROM bansos.tb_verifikasi a
			INNER JOIN tb_user_profile b on a.created_by = b.id_user
			INNER JOIN bansos.tx_bansos c on a.id_kpm = c.id_kpm
			INNER JOIN ref.tb_bantuan d on c.id_bantuan = d.id_bantuan
			INNER JOIN bansos.tm_kpm e on a.id_kpm = e.id_kpm
			INNER JOIN master.tb_desa f on e.kode_desa = f.kode_desa
			INNER JOIN master.tb_kec g on f.kode_kec = g.kode_kec
			WHERE a. id_verifikasi = $idVerifikasi";
		
		$query = $this->db->query($sql)->row();
		return $query;


	}

	function SaveProsesVerifikasi($id_verifikasi,$persetujuan,$id_kpm_verifikasi){

		if($persetujuan == 1){ /* -- disetujui */

			$sqlVerifikasiId = "
			select id_verifikasi,hasil from bansos.tb_verifikasi  where id_verifikasi = $id_verifikasi";

			$queryVerifikasiKet = $this->db->query($sqlVerifikasiId)->row();


			$data_where_arr_bansos_diterima = array(
				'status_verifikasi'	=> $persetujuan,
				'keterangan'		=> $queryVerifikasiKet->hasil,
				'updated_by' => $this->session->userdata(S_ID_USER),
				'updated_dt' => date('Y-m-d H:i:s')
			); 

			$this->db->where('id_kpm',$id_kpm_verifikasi);
			$this->db->update('bansos.tx_bansos',$data_where_arr_bansos_diterima);

			
			$result;
			if (!$this->db->affected_rows()) {
				$result = false;
			} else {
				$result = true;
			}

			return $result;

		}else{ /* ditolak */
			
			$data_where_arr_bansos = array(
				'status_verifikasi'	=> $persetujuan,
				'keterangan'		=> null,
				'updated_by' => $this->session->userdata(S_ID_USER),
				'updated_dt' => date('Y-m-d H:i:s')
			); 

			$this->db->where('id_kpm',$id_kpm_verifikasi);
			$this->db->update('bansos.tx_bansos',$data_where_arr_bansos);

			/* $data_where_kpm = array(
				'latitude'	=> 0,
				'longitude'	=> 0,
			); */

			/* $this->db->where('id_kpm',$id_kpm_verifikasi);
			$this->db->update('bansos.tm_kpm',$data_where_kpm); */

			$this->db->where('id_verifikasi',$id_verifikasi);
			$this->db->delete('bansos.tb_verifikasi');


			$result;
			if (!$this->db->affected_rows()) {
				$result = false;
			} else {
				$result = true;
			}

			return $result;

		}
	}


}
