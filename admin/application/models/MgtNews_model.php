<?php


class Mgtnews_model extends CI_Model
{
	function __construct() {
        parent:: __construct();
		$this->load->model('Common_model','cmnm');
	}


	function get_berita(
		$start = ''
		, $length = ''
		, $order = ''
		, $dir = 'asc'
		, $judul_berita = ''
	){
		$user_id = $this->session->userdata(S_ID_USER);
		$judul_berita_lower = strtolower($judul_berita);
		$sql = "
			SELECT
				 a.id_berita,a.judul_berita,a.isi_berita,a.gambar_berita, concat(b.first_name,' ',b.middle_name,' ',b.last_name) as created
			FROM bansos.tx_berita a
			INNER join tb_user_profile b on a.created_by = b.id_user
			where a.created_by = $user_id
		";

		if($judul_berita != ''){
			$sql .= " AND LOWER(judul_berita) LIKE '%{$judul_berita_lower}%'";
		}

		$sql .= " limit " . $start . " OFFSET " . $length;
        return $this->db->query($sql)->result();
	}

	function count_berita($judul_berita){
		$user_id = $this->session->userdata(S_ID_USER);
		$judul_berita_lower = strtolower($judul_berita);
		$sql = "
			SELECT
				 count(*) as cnt
			FROM bansos.tx_berita a
			INNER join tb_user_profile b on a.created_by = b.id_user
			where a.created_by = $user_id
		";

		if($judul_berita != ''){
			$sql .= " AND LOWER(judul_berita) LIKE '%{$judul_berita_lower}%'";
		}

		return $this->db->query($sql)->row()->cnt;

	}

	function SaveNews($data){
		$this->db->insert('bansos.tx_berita', $data);
		$result;
		if (!$this->db->affected_rows()) {
			$result = false;
		} else {
			$result = true;
		}
		return $result;

	}

	function SaveNewsUpdate($data,$idBerita){
		$this->db->where('id_berita',$idBerita);
		$this->db->update('bansos.tx_berita',$data);
		$result;
		if (!$this->db->affected_rows()) {
			$result = false;
		} else {
			$result = true;
		}
		return $result;
	}

	function getBeritaById($idBerita){
		$sql = "
					SELECT
					a.id_berita,a.judul_berita,a.isi_berita,a.gambar_berita, concat(b.first_name,' ',b.middle_name,' ',b.last_name) as created
			FROM bansos.tx_berita a
			INNER join tb_user_profile b on a.created_by = b.id_user
			where a.id_berita = $idBerita
		";

		$query = $this->db->query($sql);
		return $query->row();
	}

	function NewsDelete($id){
		$this->db->where('id_berita',$id);
		$this->db->delete('bansos.tx_berita');
		$result;
		if (!$this->db->affected_rows()) {
			$result = false;
		} else {
			$result = true;
		}

		return $result;
	}

}
