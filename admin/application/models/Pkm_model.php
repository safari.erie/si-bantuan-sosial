<?php


class Pkm_model extends CI_Model
{
	function __construct() {
        parent:: __construct();
		$this->load->model('Common_model','cmnm');
	}
	
	function get_data(
		$start = ''
		, $length = ''
	    , $order = ''
	    , $dir = 'asc'
	    , $kode_kec = ''
		, $kode_desa  = ''
		, $nik = '' 

	){
		$sql = "
			SELECT a.id_kpm,a.kode_desa,a.nama,a.nik,a.alamat,a.status,a.dtks,
					b.kode_desa,b.name as nama_desa,
					c.kode_kec,c.name as nama_kec
			from bansos.tm_kpm a
			inner join master.tb_desa b on a.kode_desa = b.kode_desa
			inner join master.tb_kec c on b.kode_kec = c.kode_kec
			WHERE 1 = 1
		";

		if($kode_kec != 0 && $kode_desa == 0 && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}'";
		}

		if($kode_kec != 0 && $kode_desa != 0  && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}' AND b.kode_desa = '{$kode_desa}'";
		}

		if($kode_kec != 0 && $kode_desa != 0 && $nik != ''){
			$sql .= " AND c.kode_kec = {$kode_kec} AND b.kode_desa = {$kode_desa} AND a.nik = '{$nik}' ";
		}
		if($kode_kec == 0 && $kode_desa == 0  && $nik != ''){
			$sql .= " AND a.nik = '{$nik}'";
		}

		
		
		$sql .= " limit " . $start . " OFFSET " . $length;
        return $this->db->query($sql)->result();
	
	}

	function count_data_pkm($kode_kec = ''
	, $kode_desa  = ''
	, $nik = ''){

		$sql = "
			SELECT 
				count(*) as cnt
				from bansos.tm_kpm a
				inner join master.tb_desa b on a.kode_desa = b.kode_desa
				inner join master.tb_kec c on b.kode_kec = c.kode_kec
			WHERE 1 = 1
		";

		if($kode_kec != 0 && $kode_desa == 0 && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}'";
		}

		if($kode_kec != 0 && $kode_desa != 0  && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}' AND b.kode_desa = '{$kode_desa}'";
		}

		if($kode_kec != 0 && $kode_desa != 0 && $nik != ''){
			$sql .= " AND c.kode_kec = {$kode_kec} AND b.kode_desa = {$kode_desa} AND a.nik = '{$nik}' ";
		}
		if($kode_kec == 0 && $kode_desa == 0  && $nik != ''){
			$sql .= " AND a.nik = '{$nik}'";
		}

		return $this->db->query($sql)->row()->cnt;

	}


	function SaveKpm($id_kpm,$dataKpm){

		if($id_kpm == -1){
			$this->db->insert('bansos.tm_kpm', $dataKpm);
			return $this->db->affected_rows() > 0;
		}else{

			$this->db->where('id_kpm',$id_kpm);
			$this->db->update('bansos.tm_kpm', $dataKpm);
			return $this->db->affected_rows() > 0;
		}
		
	}


	function getLastIdKpm(){
		$sql = "
			select count(*) + 1 as  lastId from bansos.tm_kpm
		";
		$query = $this->db->query($sql)->row();
		return $query;
	}

	function getKpmById($idKpm){
		$sql = "
			SELECT a.id_kpm,a.kode_desa,a.nama,a.nik,a.alamat,a.layak,a.status,a.dtks,
					b.kode_kec
			from bansos.tm_kpm a
			inner join master.tb_desa b on a.kode_desa = b.kode_desa
			where id_kpm = $idKpm
		";

		$query = $this->db->query($sql);
		return $query->row();
	}

	function checkBansos($id_kpm){

		$sql = "
			SELECT id_bansos,id_bantuan,id_kpm,status_verifikasi,keterangan
			FROM 
			bansos.tx_bansos where id_kpm = $id_kpm
		";

		$query = $this->db->query($sql);
		return $query->row();
	}

	function KpmDelete($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
		$result;
		if (!$this->db->affected_rows()) {
			$result = false;
		} else {
			$result = true;
		}

		return $result;
	}

	function save_upload_kpm_bansos($flag_bansos,$kode_desa,$dtks,$dataArr){

		if($flag_bansos == 0){
			$getLastId = $this->cmnm->common_last_id('id_kpm','bansos.tm_kpm');
			$lastKpm;
			if($getLastId->lastid == null){
				$lastKpm = 1;
			}else{
				$lastKpm = $getLastId->lastid + 1;
			}
			
			
			$kpm_ids = array();
			foreach ($dataArr as $item) {
				$nik_upload = $item['nik'];
				array_push($kpm_ids,$nik_upload);
			}
			ini_set('memory_limit', '1024M');
		
			$dt_arr_exist = $this->cek_kpm_by_in_nik($kpm_ids);
		

		
			$dt_tmp = array();
			foreach($dt_arr_exist as $rows){
				array_push($dt_tmp,$rows->nik);
			}

			$data_no_exist = array();
			foreach($dataArr as $key=>$item ){
				if(!in_array($item['nik'],$dt_tmp)){
					array_push($data_no_exist,array(
						'id_kpm'	=> $lastKpm,
						//'kode_desa'	=> $item['kode_desa'],
						'kode_desa'	=> $kode_desa,
						'nik'		=> $item['nik'],
						'nama'		=> $item['nama'],
						'alamat'	=> $item['alamat'],
						'layak'		=> $item['layak'],
						'dtks'		=> $dtks,
						//'dtks'		=> $item['dtks'],
						'status'		=> true,
						'created_by' => $this->session->userdata(S_ID_USER),
						'created_dt' => date('Y-m-d H:i:s')
					));
					$lastKpm++;
				}
				
			}

			if($data_no_exist != 0){

				$this->db->trans_start();

				$_datas = array_chunk($data_no_exist, 300);

				foreach ($_datas as $key => $data) {
					$this->db->insert_batch('bansos.tm_kpm',$data);	
				}

				$this->db->trans_complete();
							
				$result;
				if (!$this->db->affected_rows()) {
					$result = false;
				} else {
					$result = true;
				}

				$output = array(
					'state' => $result,
					'msg'   => null,
					'data_insert'	=> $data_no_exist,
					'data_no_insert'	=> $dt_arr_exist,
				);

				return $output;
			}else{
				$output = array(
					'state' => false,
					'msg'	=> 'Data tidak ada yang di insert',
					'data_insert'	=> $data_no_exist,
					'data_no_insert'	=> $dt_arr_exist,
				);
				return $output;
			}
			

		}else{

			$getLastId = $this->cmnm->common_last_id('id_kpm','bansos.tm_kpm');
			$lastKpm;
			if($getLastId->lastid == null){
				$lastKpm = 1;
			}else{
				$lastKpm = $getLastId->lastid + 1;
			}


			$getLastIdBansos = $this->cmnm->common_last_id('id_bansos','bansos.tx_bansos');
			$lasIdBansos;
			if($getLastIdBansos->lastid == null){
				$lasIdBansos = 1;
			}else{
				$lasIdBansos = $getLastIdBansos->lastid + 1;
			}
			
			
			$kpm_ids = array();
			foreach ($dataArr as $item) {
				$nik_upload = $item['nik'];
				array_push($kpm_ids,$nik_upload);
			}
			ini_set('memory_limit', '1024M');
		
			$dt_arr_exist = $this->cek_kpm_by_in_nik($kpm_ids);
		

		
			$dt_tmp = array();
			foreach($dt_arr_exist as $rows){
				array_push($dt_tmp,$rows->nik);
			}

			$data_no_exist = array();
			$data_tmp_arr_bansos = array();
			foreach($dataArr as $key=>$item ){
				if(!in_array($item['nik'],$dt_tmp)){
					array_push($data_no_exist,array(
						'id_kpm'	=> $lastKpm,
						//'kode_desa'	=> $item['kode_desa'],
						'kode_desa'	=> $kode_desa,
						'nik'		=> $item['nik'],
						'nama'		=> $item['nama'],
						'alamat'	=> $item['alamat'],
						'layak'		=> $item['layak'],
						//'dtks'		=> $item['dtks'],
						'dtks'		=> $dtks,
						'status'		=> true,
						'created_by' => $this->session->userdata(S_ID_USER),
						'created_dt' => date('Y-m-d H:i:s')
					));

					array_push($data_tmp_arr_bansos,array(
						'id_bansos'	=> $lasIdBansos,
						'id_kpm'	=> $lastKpm,
						'id_bantuan'=> $flag_bansos,
						'status_verifikasi'	=> 0,
						'keterangan'	=> 'From Upload',
						'created_by' => $this->session->userdata(S_ID_USER),
						'created_dt' => date('Y-m-d H:i:s')
					));
					
					$lastKpm++;
					$lasIdBansos++;
				}
				
			}

			if($data_no_exist != 0){

				$this->db->trans_start();

				$_datas = array_chunk($data_no_exist, 300);

				foreach ($_datas as $key => $data) {
					$this->db->insert_batch('bansos.tm_kpm',$data);	
				}


				$_datas2 = array_chunk($data_tmp_arr_bansos, 300);

				foreach ($_datas2 as $key => $data2) {
					$this->db->insert_batch('bansos.tx_bansos',$data2);	
				}



				$this->db->trans_complete();


				/* $this->db->insert_batch('bansos.tm_kpm',$data_no_exist);				
				$this->db->insert_batch('bansos.tx_bansos',$data_tmp_arr_bansos);	 */			
				$result;
				if (!$this->db->affected_rows()) {
					$result = false;
				} else {
					$result = true;
				}

				$output = array(
					'state' => $result,
					'msg'	=> '',
					'data_insert'	=> $data_no_exist,
					'data_no_insert'	=> $dt_arr_exist,
				);

				return $output;
			}else{
				$output = array(
					'state' => false,
					'msg'	=> 'Data tidak ada yang di insert',
					'data_insert'	=> $data_no_exist,
					'data_no_insert'	=> $dt_arr_exist,
				);
				return $output;
			}

		}
	}

	function cek_kpm_by_nik($nik){
		$sql = "
				SELECT 
				id_kpm,nama,nik,kode_desa 
				FROM bansos.tm_kpm where nik = '$nik'";
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function cek_kpm_by_in_nik($arrNik){
		$this->db->where_in('nik',$arrNik);
		$this->db->select('nik');
		$query = $this->db->get('bansos.tm_kpm');
		return $query->result();
	}
	

}
