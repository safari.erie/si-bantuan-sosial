<?php


class Complaint_model extends CI_Model
{

	function getDataComplaint(
		$start = ''
		, $length = ''
		, $order = ''
		, $dir = 'asc'
	){
		$whereCondition;
		$kodeWilayah = $this->getKodeWilayahByIdUser()->kode_wilayah;
		if($kodeWilayah == 0 || $kodeWilayah == null || $kodeWilayah == ''){
			$whereCondition = "WHERE 1 = 1";
		}else if(strlen($kodeWilayah) == 7){ // untuk kecamatan
			$whereCondition = "WHERE c.kode_kec = ".$kodeWilayah;
		}

		$sql = "			
			select 	a.id_pengaduan,a.id_desa,a.nik_pengadu,a.nama_pengadu,a.alamat as alamat_pengadu,a.id_jenis_pengaduan,a.id_kpm,a.dokumentasi,a.no_telp,
					b.nama,b.nik,b.alamat,
					c.name as nama_desa,
					d.name as nama_kec,
					e.name as nama_desa_pengadu,
					f.name as nama_kec_pengadu,
					h.nama_bantuan,
					i.name as nama_jenis_pengaduan
			from bansos.tb_pengaduan a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			inner join master.tb_kec d on c.kode_kec = d.kode_kec
			inner join master.tb_desa e on a.id_desa = e.kode_desa
			inner join master.tb_kec f on e.kode_kec = f.kode_kec
			inner join bansos.tx_bansos g on b.id_kpm = g.id_kpm
			inner join ref.tb_jenis_pengaduan i on a.id_jenis_pengaduan = i.id_jenis_pengaduan
			inner join ref.tb_bantuan h on g.id_bantuan = h.id_bantuan
			$whereCondition
		";

		//$sql .= " limit " . $start . " OFFSET " . $length;
        return $this->db->query($sql)->result();

		
	}

	function CountDataComplaint(){
		$whereCondition;
		$kodeWilayah = $this->getKodeWilayahByIdUser()->kode_wilayah;
		if($kodeWilayah == 0 || $kodeWilayah == null || $kodeWilayah == ''){
			$whereCondition = "WHERE 1 = 1";
		}else if(strlen($kodeWilayah) == 7){ // untuk kecamatan
			$whereCondition = "WHERE d.kode_kec = ".$kodeWilayah;
		}else{
			$whereCondition = "WHERE c.kode_desa = ".$kodeWilayah;
		}
		$sql = "			
			select count(*) as cnt
			from bansos.tb_pengaduan a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			inner join master.tb_kec d on c.kode_kec = d.kode_kec
			inner join master.tb_desa e on a.id_desa = e.kode_desa
			inner join master.tb_kec f on e.kode_kec = f.kode_kec
			inner join ref.tb_jenis_pengaduan i on a.id_jenis_pengaduan = i.id_jenis_pengaduan
			$whereCondition
		";

		return $this->db->query($sql)->row()->cnt;


		
	}

	function getComplaintById($id_jenis_pengaduan){
		$sql = "
		
			SELECT 			
				a.id_pengaduan,a.id_desa,a.nik_pengadu,a.nama_pengadu,a.alamat as alamat_pengadu,a.id_jenis_pengaduan,a.id_kpm,a.dokumentasi,a.no_telp,
				b.nama,b.nik,b.alamat,
				c.name as nama_desa,
				d.name as nama_kec,
				e.name as nama_desa_pengadu,
				f.name as nama_kec_pengadu,
				h.nama_bantuan,
				i.name as nama_jenis_bantuan
			FROM bansos.tb_pengaduan a
			INNER JOIN bansos.tm_kpm b on a.id_kpm = b.id_kpm
			INNER JOIN master.tb_desa c on b.kode_desa = c.kode_desa
			INNER JOIN master.tb_kec d on c.kode_kec = d.kode_kec
			INNER JOIN master.tb_desa e on a.id_desa = e.kode_desa
			INNER JOIN master.tb_kec f on e.kode_kec = f.kode_kec
			INNER JOIN bansos.tx_bansos g on b.id_kpm = g.id_kpm
			INNER JOIN ref.tb_jenis_pengaduan i on a.id_jenis_pengaduan = i.id_jenis_pengaduan
			INNER JOIN ref.tb_bantuan h on g.id_bantuan = h.id_bantuan
			where a.id_pengaduan = $id_jenis_pengaduan
		";

		$query = $this->db->query($sql)->row();
		return $query;
	}

	
	function getKodeWilayahByIdUser(){
		$id_user = $this->session->userdata(S_ID_USER);

		$sql = "
			SELECT kode_wilayah from tb_user_profile 
			where id_user = $id_user;
		"; 

		$query = $this->db->query($sql)->row();
		return $query;
	}

}


