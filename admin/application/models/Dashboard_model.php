<?php

class Dashboard_model extends CI_Model
{

	function countAllBansos($idBantuan){
		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			where a.id_bantuan = $idBantuan;
		";

		return $this->db->query($sql)->row()->cnt;

	}

	function countAllAjaxKecBansos($kode_kec, $idBantuan,$statusVerifikasi){
		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			where a.id_bantuan = $idBantuan and a.status_verifikasi = $statusVerifikasi
		";

		if($kode_kec != 0){
			$sql .= " and  substring(cast(b.kode_desa as text), 0,8) = '{$kode_kec}' ";
		}

		return $this->db->query($sql)->row()->cnt;

	}

	function countDtksAjaxKecBansos($kode_kec, $idBantuan, $statusVerifikasi){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = $idBantuan and b.dtks = true and a.status_verifikasi = $statusVerifikasi";
		if($kode_kec != 0){
			$sql .= " and  substring(cast(b.kode_desa as text), 0,8) = '{$kode_kec}' ";
		}
		return $this->db->query($sql)->row()->cnt;
	}


	function countAllAjaxDesaBansos($kode_desa, $idBantuan,$statusVerifikasi){
		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			where a.id_bantuan = $idBantuan and a.status_verifikasi = $statusVerifikasi
		";

		if($kode_desa != 0){
			$sql .= " and  b.kode_desa = {$kode_desa}";
		}

		return $this->db->query($sql)->row()->cnt;

	}

	function countNonDtksAjaxDesaBansos($kode_desa, $idBantuan, $statusVerifikasi){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = $idBantuan and b.dtks = false and a.status_verifikasi = $statusVerifikasi";
		if($kode_desa != 0){
			$sql .= " and b.kode_desa = {$kode_desa} ";
		}
		return $this->db->query($sql)->row()->cnt;
	}

	function countDtksAjaxDesaBansos($kode_desa, $idBantuan, $statusVerifikasi){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = $idBantuan and b.dtks = true and a.status_verifikasi = $statusVerifikasi";
		if($kode_desa != 0){
			$sql .= " and b.kode_desa = {$kode_desa} ";
		}
		return $this->db->query($sql)->row()->cnt;
	}

	function countNonDtksAjaxKecBansos($kode_kec, $idBantuan, $statusVerifikasi){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = $idBantuan and b.dtks = false and a.status_verifikasi = $statusVerifikasi";
		if($kode_kec != 0){
			$sql .= " and  substring(cast(b.kode_desa as text), 0,8) = '{$kode_kec}' ";
		}
		return $this->db->query($sql)->row()->cnt;
	}

	function countDtksBansos($idBantuan){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = $idBantuan and b.dtks = true";
		return $this->db->query($sql)->row()->cnt;
	}

	

	
	function countNonDtksBansos($idBantuan){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = $idBantuan and b.dtks = false";
		return $this->db->query($sql)->row()->cnt;
	}

	function GraphDtKsNonDtksKec(){

		$sql = "
			SELECT d.name as nama_kecamatan,d.kode_kec,
				COUNT(CASE WHEN a.dtks = true then 1 END ) as dtks,
				COUNT(CASE WHEN a.dtks = false then 1 END) as no_dtks
			from bansos.tm_kpm a
			inner join bansos.tx_bansos b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on a.kode_desa = c.kode_desa
			inner join master.tb_kec d on c.kode_kec = d.kode_kec
			where substring(cast(a.kode_desa as text), 0,8) = substring(cast(d.kode_kec as text), 0,8)
			group by d.name,d.name,d.kode_kec
		";

		$query = $this->db->query($sql)->result();

		return $query;
	}

}
