<?php

class ForgotPassword_model extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function CheckUserEmail($email){
		$sql = " 
				SELECT a.id_user,a.username,a.email,a.status,
						CONCAT(b.first_name,' ',b.middle_name,'',b.last_name) as full_name
				FROM tb_user a
				INNER JOIN tb_user_profile b on a.id_user = b.id_user
				where a.email = '{$email}'
		";

		$query = $this->db->query($sql);
		return $query->row();
				
	}

	function reset_password($data, $email, $isReset, $key ="" )
	{
		
		if($isReset == true && $key != ""){
			$this->db->where('reset_password_code', $key);
		}else{
			$this->db->where('email', $email);
		}
		$this->db->update('tb_user', $data);
		$result;
		if (!$this->db->affected_rows()) {
			$result = false;
		} else {
			$result = true;
		}
		return $result;
	}

	function CheckKeyCodeResetPassword($resetPasswordKeyCode){
		$sql = " 
				SELECT a.id_user,a.username,a.email,a.status,a.reset_password_code,
						CONCAT(b.first_name,' ',b.middle_name,'',b.last_name) as full_name
				FROM tb_user a
				INNER JOIN tb_user_profile b on a.id_user = b.id_user
				where a.reset_password_code = '{$resetPasswordKeyCode}'
		";

		$query = $this->db->query($sql);
		return $query->row();
	}

}
