<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author: misbah@arkamaya.co.id
 * @created: 2020-13-11
 */



function search_for_key($id, $array, $btnId) {
    $k = array();
    foreach ($array as $key => $val) {
        if ($val['type'] === $id && strpos($val['id'], $btnId) !== false ) {
            array_push($k, $val);
        }
    }
    return $k;
 }



function create_menu( $list = array() ){
	//echo json_encode($list);
	$CI = & get_instance();
	$html = '';
    //$list = $this->menu;
    if ($list){
        //echo json_encode($list);
		$i = 1;
		$html .= '<ul class="sidebar-menu">';
        foreach ($list as $row) {		
			  if($row['id_appl_task_parent'] == 0)
			  {
					if($row['controller_name'] != ""){
						$html .= "<li>";
                        $this->create_menu .= "<a href=" . base_url() . $row['controller_name'] . "> " . $row['appl_task_name'] . " </a>";
					}else{
						$html .= "<li class='nav-item dropdown'>";
						$html .= "<a href='#' class='nav-link has-dropdown'> " . $row['appl_task_name'] . " </a>";
						echo 'XXXXXX '.menu($row['id_appl_task'], $list);
						$html .= "</li>";
					}
					$i++;

			  }
        } 
        $html .= '</ul></li>';
    }else{
		$html .= '<ul></ul>';
	}
    
    echo $html;
}

function menu($parent, $list = array()){
	
	$htmls = "<ul class='dropdown-menu'>";;
        foreach ($list as $item) {
		
            if ($parent == $item['id_appl_task_parent']) {
			
                $has_child = has_child($item['id_appl_task'], $list);
                if ($item['controller_name'] != "") {
					//echo 'masuk XXX';
                    $html .= "<li>";
                    $html .= "<a href=" . base_url() . $item['controller_name'] . ">" . strtoupper($item['appl_task_name']) . "</a>";
					$html .= "</li>";
					
                } else {
                    if ($has_child) {
                        $html .= "<li class='dropdown-submenu'>";
                        $html .= "<a href='#'>" . $item['Name'] . "</a>";
                        menu($item['id_appl_task'], $get_url_service);
                        $html .= "</li>";

                    }

                }

            }
        }
        return $html .= "</ul>";
}

function has_child($parent, $list = array())
{
	
	foreach ($list as $item) {
		
		if ($parent == $item['id_appl_task_parent']) {
			return true;
		}
	}
	return false;
}



