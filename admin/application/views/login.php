<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>SIBOS Pancakarsa</title>
  <!-- General CSS Files -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> 
<!-- 	<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/bootstrap/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link type="text/css" href="<?php echo base_url();?>assets/img/design_01.png" rel="icon">

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/components.css">
	<style>
		.field-icon {
			float: right;
			margin-right: 20px;
			margin-top: -25px;
			position: relative;
			z-index: 2;
			cursor: pointer;
	}
	</style>
</head>

<body>
<div id="app">
    <section class="section">
      <div class="d-flex flex-wrap align-items-stretch">
        <div class="col-lg-4 col-md-6 col-12 order-lg-1 min-vh-100 order-2 bg-white">
          <div class="p-4 m-3">
		  <div class="text-center">
		 	 <img src="<?php echo base_url();?>assets/img/design_01.png" alt="logo" width="80" class="mb-5 mt-2">	
		  </div>            
            <h4 class="text-dark font-weight-normal">Selamat Datang di <span class="font-weight-bold">Admin Sibos</span></h4>           
            <form method="POST" action="<?php echo site_url('admin/login/check_login')?>" class="needs-validation" novalidate="">
              <div class="form-group">
                <label for="email">Username or email</label>
                <input id="email" type="text" class="form-control" name="UsernameOrEmail" tabindex="1" required autofocus>
								<?php if ($this->session->flashdata('notif_email_wrong') != 'success'): ?>                          
									<div class="form-group">
											<div class="d-flex justify-content-between mg-b-5">
											<span class="tx-13 text-danger"><?php echo $this->session->flashdata('notif_email_wrong'); ?></span>
											</div>
									</div>
								<?php endif; ?>
              </div>

              <div class="form-group">
                <div class="d-block">
									<label for="password" class="control-label">Password</label>
									<div class="float-right">
												<a  href="<?php echo base_url(); ?>admin/ForgotPassword" class="text-small">
														Lupa Password?
												</a>
										</div>
                </div>
								<!-- <input id="password" type="password" class="form-control" name="password" tabindex="2" required> -->
								<input type="password" id="password-field" class="form-control" name="password" tabindex="2"  placeholder="Ketikkan Password Anda..." required="">
								<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
								<?php if ($this->session->flashdata('notif_pass_wrong') != 'success'): ?>                          
									<div class="form-group">
											<div class="d-flex justify-content-between mg-b-5">
											<span class="tx-13 text-danger"><?php echo $this->session->flashdata('notif_pass_wrong'); ?></span>
											</div>
									</div>
								<?php endif; ?>
              </div>
              <div class="form-group text-right">
                <button type="submit" class="btn btn-primary btn-lg btn-icon icon-right" tabindex="4">
                  Login
                </button>
              </div>
              <br>
             
            </form>

           
          </div>
        </div>
        <div class="col-lg-8 col-12 order-lg-2 order-1 min-vh-100 background-walk-y position-relative overlay-gradient-bottom" 
			data-background="<?php echo base_url()?>assets/admin/img/unsplash/city.jpg">
          <div class="absolute-bottom-left index-2">
            <div class="text-light p-5 pb-2">
			
              <div class="mb-5 pb-3">
                <h5 class="font-weight-normal text-muted-transparent">Bogor, Indonesia</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- General JS Scripts -->
  <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
	<script src="<?php echo base_url();?>assets/admin/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/popper.js"></script>
	<!-- <script src="<?php echo base_url();?>assets/admin/bootstrap/js/bootstrap.min.js"></script> -->
	<script src="<?php echo base_url();?>assets/admin/nicescroll/jquery.nicescroll.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/moment.min.js"></script>

 <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> -->
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script> -->
  <script src="<?php echo base_url();?>assets/admin/js/stisla.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo base_url();?>assets/admin/js/scripts.js"></script>
	<script src="<?php echo base_url();?>assets/admin/js/custom.js"></script>
	<script type="text/javascript">
		$(".toggle-password").click(function () {
			$(this).toggleClass("fa-eye fa-eye-slash");
			var input = $($(this).attr("toggle"));
			if (input.attr("type") == "password") {
					input.attr("type", "text");
			} else {
					input.attr("type", "password");
			}
	});
	</script>
</body>
</html>
