	<style>
		.modal-full {
			max-width: 98%;
		}

		.map-content {
			-ms-flex-preferred-size: 0;
			flex-basis: 0;
			-webkit-box-flex: 1;
			-ms-flex-positive: 1;
			flex-grow: 1;
			max-width: 100%;
			background-color: #777776;
			position: relative;
		}

		#map {
			margin: 0px;
			width: 100%;
			height: 400px;
			padding: 0px;
		}
	</style>
  <div class="main-content">
	
	<section class="section">
		<div class="section-header">
            <h1>Daftar Verifikasi Bansos</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Daftar Approve Verifikasi Bansos</a></div>
                <div class="breadcrumb-item"><a href="#">Admin</a></div>
                <div class="breadcrumb-item active"><a href="#">Daftar Verifikasi Bansos</a></div>
            </div>
        </div>
		<div class="card" id="ListData" style="">
			<div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3">Daftar Approve Verifikasi Bansos</h4>
			<!-- 	<div class="col-md-6 text-right"> 
					<button type="button" class="btn btn-primary btn-flat" id="addBtnVerifikasi" onclick="AddApproveVerifikasi();"><i class="fa fa-plus"></i> Verifikasi</button> &nbsp; &nbsp; &nbsp; 
				</div> -->
			</div>
			<div class="card-body">
				<div class="row pb-4" >
					<div class="col-md-3">
						<label for="name">Kecamatan</label>
						<select name="kecamatan" class="form-control" id="kecamatans">_mo
						</select>
					</div>
					<div class="col-md-3">
						<label for="name">Desa</label>
						<select name="desa" class="form-control" id="desas">
							<option value="0">-- Pilih Desa --</option>
						</select>
					</div>
					<div class="col-md-3">
						<label for="name">Nik</label>
						<input type="text" class="form-control" id="nik" name="nik"/>
						<div class="validate"></div>
					</div>
					<div class="col-md-3 pt-4">
						<a href="javascript:void(0)" onclick="searchdata()" class="btn btn-primary"> Filter</a>
					</div>
				</div>
				<div class="table-responsive" id="tableVerifikasiApprove">
					<table id="tableVerifikasiApproveContent" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Nik</th>
								<th>Alamat</th>
								<th>Kecamatan</th>
								<th>Desa</th>
								<th>Nama Bantuan</th>
								<th>Status Verifikasi</th>				
								<th>Foto Ktp</th>						
								<th>Foto KK</th>						
								<th>Pembuat Verifikasi</th>						
								<th>Aksi</th>						
							</tr>
						</thead>
					</table>
				</div>
				
			</div>
		</div>


		<div class="card" id="formInput" style="display:none">
			<div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3" id="info-title-input">Proses Persetujuan Verifikasi</h4>
			</div>
			<div class="card-body">
				<div class="row" id="form_proses_approve_verifikasi" >
					<div class="col-md-12">
						<form class="form-horizontal" id="formKecamatan"  enctype="multipart/form-data" role="form" method="post" action="">
							<div class="row">
								<div class="col-12">									
									<div class="form-group row">											
										<label class="col-md-2 control-label text-left pt-2">
											<div class="">
												Nik 
											</div>
										</label>
										<div class="col-md-6">
											<label id="valNik"></label>
										</div>
									</div>	
									<div class="form-group row">											
										<label class="col-md-2 control-label text-left pt-2">
											<div class="">
												Nama 
											</div>
										</label>
										<div class="col-md-6">
											<label id="valNama"></label>
										</div>
									</div>	
									<div class="form-group row">											
										<label class="col-md-2 control-label text-left pt-2">
											<div class="">
												Alamat 
											</div>
										</label>
										<div class="col-md-6">
											<label id="valAlamat"></label>
										</div>
									</div>	
									<div class="form-group row">											
										<label class="col-md-2 control-label text-left pt-2">
											<div class="">
												Foto KTP 
											</div>
										</label>
										<div class="col-md-4">
											<div id="fotKtp"></div>
										</div>

										<label class="col-md-2 control-label text-left pt-2">
											<div class="">
												Foto Rumah 
											</div>
										</label>
										<div class="col-md-4">
											<div id="fotoRumah"></div>
										</div>
									</div>	
									
									<div class="form-group row">											
										<label class="col-md-2 control-label text-left pt-2">
											<div class="">
												Wilayah 
											</div>
										</label>
										<div class="col-md-5">
											<label>Kecamatan</label>
											<label id="valKecamatan"></label>
										</div>
										<div class="col-md-5">
											<label>Desa</label>
											<label id="valDesa"></label>
										</div>

									</div>	
									<div class="form-group row">											
										<label class="col-md-2 control-label text-left pt-2">
											<div class="">
												Nama Bantuan 
											</div>
										</label>
										<div class="col-md-5">										
											<label id="valBantuan"></label>
										</div>
									</div>	
									<div class="form-group row">											
										<div class="map-content d-flex">
											<div id="map"></div>
										</div>
									</div>	
									<div class="form-group row">											
										<label class="col-md-2 control-label text-left pt-2">
											<div class="">
												Pilih Persetujuan Verifikasi
											</div>
										</label>
										<div class="col-md-5">										
											<select class="form-control" id="persetujuan" name="persetujuan">
												<option value="0">-- Pilih -- </option>
												<option value="1">Diterima </option>
												<option value="-1"> Ditolak</option>
											</select>
										</div>
									</div>	
									
								</div>
							</div>
							
							<hr>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group text-right">
										<button type="button" class="btn btn-secondary btn-bordered waves-effect w-md m-b-5" onclick="onCancel()">Batal</button>
										<button type="button" class="btn btn-success btn-bordered waves-effect w-md waves-light m-b-5" id="prosesVerifikasi" >Proses Verifikasi</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		
	</section>


	<!-- modal here -->
<div class="modal fade" id="ModalFormVerifikasiProses" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleFormUser">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
				<div class="table-responsive">
						<table id="datatableKpm" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Id Kpm</th>
									<th>Nama</th>
									<th>Nik</th>
									<th>Alamat</th>					
								</tr>
							</thead>
						</table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>


<div id="modal-verifikasi" class="modal fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">            

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Verifikasi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
			<div class="modal-body">
				<p class="text-center">Apakah akan melanjutkan proses verifikasi ?</p>
				<form id="form_proses_modal_verifikasi" class="form-horizontal" enctype="multipart/form-data" role="form" method="post" action="">
					<input type="hidden" name="id_verifikasi" id="id_verifikasi"/>
					<input type="hidden" name="id_kpm_verifikasi" id="id_kpm_verifikasi"/>
				</form>

			</div>
			<div class="modal-footer">             
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
				<button type="button" class="btn btn-success btn-bordered waves-effect w-md waves-light m-b-5" id="btnModalSaveVerifikasi" onclick="SaveProsesVerifikasiBansos()">Simpan</button>
			</div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</div>
