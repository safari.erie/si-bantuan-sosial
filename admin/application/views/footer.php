<footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2020 <div class="bullet"></div> Design By <a href="javascript:void(0)"><strong> Dinas Sosial Kabupaten Bogor </strong> All Rights Reserved </a>
        </div>
       
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
 <!--  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
  <!--
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script> -->

	<script src="<?php echo base_url();?>assets/admin/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/popper.js"></script>
	<script src="<?php echo base_url();?>assets/admin/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/nicescroll/jquery.nicescroll.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/moment.min.js"></script>

  <script src="<?php echo base_url();?>assets/admin/js/stisla.js"></script>
  <script src="<?php echo base_url();?>assets/admin/js/scripts.js"></script>
  <script src="<?php echo base_url();?>assets/admin/js/custom.js"></script>
	<script src="<?php echo base_url();?>assets/plugin/datatable/1.10.19/js/jquery.dataTables.js"></script>  
	<script src="<?php echo base_url();?>assets/plugin/datatable/1.10.19/js/dataTables.bootstrap4.min.js"></script>  

	<script src="<?php echo base_url();?>assets/admin/amcharts3/amcharts.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/serial.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/radar.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/pie.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/plugins/animate/animate.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/plugins/export/export.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/themes/light.js"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/themes/black.js"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/themes/dark.js"></script>

	<script src="<?php echo base_url()?>assets/plugin/leaflet/leaflet.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/plugin/leaflet/L.Control.Basemaps.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/plugin/leaflet/Leaflet.fullscreen.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugin/leaflet/L.Control.Zoomslider.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/plugin/izitoast/js/iziToast.min.js"></script> 
	<script src="<?php echo base_url();?>assets/plugin/summernote/summernote.min.js"></script> 

  <!-- jsapp -->
<?php
if (isset($jsapp)) : foreach ($jsapp as $js) : ?>
    <script type="text/javascript" src="<?php echo base_url() ?>jsapp/<?php echo $js ?>.js"></script>
<?php
  endforeach;
endif;
?>

<script type="text/javascript">
    var SITE_URL = '<?php echo site_url() ?>admin/';

    //uri string ada di my_controller
    var CONTROLLER = '<?php echo ($this->uri->segment(1) !== FALSE) ? $this->uri->segment(1) : ""; ?>';

    function ProgressBar(Status) {
				if (Status == "wait") {
						$("body").css("cursor", "progress");
						$("#loading").removeAttr("style");
				} else if (Status == "success") {
						$("body").css("cursor", "default");
						$("#loading").css("display", "none");
				}
		}



		$(document).ready(function () {
            if (jQuery().summernote) {
                setTimeout((e) => {
                    $('.summernote').summernote({
                        placeholder: 'Isian Content',
                        tabsize: 20,
                        height: 300
                    });
                }, 2000)
                
                $('.summernote').summernote('destroy');

                $(".summernote-simple").summernote({
                    dialogsInBody: true,
                    minHeight: 310,
                    toolbar: [
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough']],
                        ['para', ['paragraph']]
                    ]
                });
            }
            
        })

	</script>
	
	
</body>
</html>
