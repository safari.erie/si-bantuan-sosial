	<style>
		.modal-full {
			max-width: 98%;
		}
	</style>
  <div class="main-content">
	
	<section class="section">
		<div class="section-header">
            <h1>Daftar Master Desa</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Daftar Master Desa</a></div>
                <div class="breadcrumb-item"><a href="#">Admin</a></div>
            </div>
        </div>
		<div class="card" id="ListData" style="">
			<div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3">Daftar Master Desa</h4>
				<div class="col-md-6 text-right"> 
					<button type="button" class="btn btn-primary btn-flat" id="addBtnMasterDesa" onclick="AddMasterDesa();"><i class="fa fa-plus"></i> Master Desa</button> &nbsp; &nbsp; &nbsp; 
				</div>
			</div>
			<div class="card-body">
				<div class="table-responsive" id="tableMasterDesa">
						<table id="tableMasterDesaContent" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Kode Kecamatan</th>			
									<th>Nama Kecamatan</th>			
									<th>Kode Desa</th>			
									<th>Nama Desa</th>			
									<th>Aksi</th>					
								</tr>
							</thead>
						</table>
				</div>
				<div class="row" id="form_proses" style="display:none;">
				
					<div class="col-sm-12">
						<form class="form-horizontal" id="formMasterDesa"  enctype="multipart/form-data" role="form" method="post" action="">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<label class="col-md-1 control-label text-left">
											<div class="">
												Nama KPM
											</div>
										</label>
										<div class="col-md-5">
											<input type="text" class="form-control" disabled="disabled" id="nama_kpm" name="name_kpm">
											<input type="hidden" class="form-control" id="id_kpm" name="id_kpm">
											
										</div>
										<div class="col-md-4">
											<button type="button" class="btn btn-warning" name="addKpmTable" id="addKpmTable">Pilih Kpm</button>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-1 control-label text-left">
											<div class="">
												Keterangan
											</div>
										</label>
										<div class="col-md-5">
											<div class="form-check">
												<input class="form-check-input" type="radio" name="RadioKeterangan" value="layak" id="RadioKeterangan1" checked="">
												<label class="form-check-label" for="RadioKeterangan1">
													Layak
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="radio" name="RadioKeterangan" value="meninggal" id="RadioKeterangan2">
												<label class="form-check-label" for="RadioKeterangan2">
													Meninggal
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="radio" name="RadioKeterangan" value="pindah" id="RadioKeterangan3" >
												<label class="form-check-label" for="RadioKeterangan3">
													Pindah
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="radio" name="RadioKeterangan" value="Tidak Mampu" id="RadioKeterangan4" >
												<label class="form-check-label" for="RadioKeterangan4">
													Tidak Mampu
												</label>
											</div>
											<div class="form-check">
												<input class="form-check-input" type="radio" name="RadioKeterangan" value="menerima Desa lain" id="RadioKeterangan5" >
												<label class="form-check-label" for="RadioKeterangan5">
													Sudah Menerima Desa Lain
												</label>
											</div>
										</div>
										
									</div>
									<div class="form-group row">
										<label class="col-md-1 control-label text-left">
											<div class="">
												Foto Ktp
											</div>
										</label>
										<div class="col-md-5">												
											<input type="file" name="fotoKtp" id="fotoKtp" class="form-control">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-1 control-label text-left">
											<div class="">
												Foto Rumah
											</div>
										</label>
										<div class="col-md-5">												
											<input type="file" name="fotoKk" id="fotoKk" class="form-control">
										</div>
									</div>
								</div>
							</div>

							<hr />
						<div class="row">
							<div class="col-md-12">
								<div class="form-group text-right">
									<button type="button" class="btn btn-secondary btn-bordered waves-effect w-md m-b-5" onclick="onCancel()">Batal</button>
									<button type="button" class="btn btn-success btn-bordered waves-effect w-md waves-light m-b-5" onclick="onSubmitMasterDesa()">Simpan</button>
								</div>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="card" id="formInput" style="display:none">
			<div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3" id="info-title-input">Input desa</h4>
			</div>
			<div class="card-body">
				<div class="row" id="form_proses_desa" >
					<div class="col-md-12">
						<form class="form-horizontal" id="formdesa"  enctype="multipart/form-data" role="form" method="post" action="">
							<div class="row">
								<input type="hidden" name="id_desa" id="id_desa" value="-1"/>
								<div class="col-12">
									<div class="form-group row">											
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Pilih Kecamatan
											</div>
										</label>
										<div class="col-md-6">
											<select name="kecamatan" id="kecamatan" class="form-control">
												<option value="0"></option>
											</select>
										</div>
									</div>	
									<div class="form-group row">											
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Kode desa
											</div>
										</label>
										<div class="col-md-6">
											<input type="text" class="form-control"  id="kode_desa" name="kode_desa">
										</div>
									</div>	
									<div class="form-group row">											
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Nama desa
											</div>
										</label>
										<div class="col-md-6">
											<input type="text" class="form-control"  id="nama_desa" name="nama_desa">
										</div>
									</div>	
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group text-right">
										<button type="button" class="btn btn-secondary btn-bordered waves-effect w-md m-b-5" onclick="onCancel()">Batal</button>
										<button type="button" class="btn btn-success btn-bordered waves-effect w-md waves-light m-b-5" onclick="onSubmitdesa()">Simpan</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		
	</section>


	<!-- modal here -->
<div class="modal fade" id="ModalFormKpm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleFormUser">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
				<div class="table-responsive">
						<table id="datatableKpm" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Id Kpm</th>
									<th>Nama</th>
									<th>Nik</th>
									<th>Alamat</th>					
								</tr>
							</thead>
						</table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

</div>
