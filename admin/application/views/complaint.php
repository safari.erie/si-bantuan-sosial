 <style>
	 .info {
		border-top: 3px solid #27ae60;
		border-bottom: 3px solid #27ae60;
		padding: 30px;
		background: #fff;
		width: 100%;
		box-shadow: 0 0 24px 0 rgba(0, 0, 0, 0.12);
	}

	.warning {
		border-top: 3px solid #ffa426;
		border-bottom: 3px solid #ffa426;
		padding: 30px;
		background: #ffffff26;
		width: 100%;
		box-shadow: 0 0 24px 0 rgba(0, 0, 0, 0.12);
	}



 </style>
  <div class="main-content">
	
	<section class="section">
		<div class="section-header">
            <h1>Daftar Pengaduan Bansos</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Management Bansos</a></div>
                <div class="breadcrumb-item"><a href="#">Admin</a></div>
                <div class="breadcrumb-item active"><a href="#">Daftar Pengaduan</a></div>
            </div>
        </div>
		<div class="card" id="ListData" style="">
			<div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3">Daftar Pengaduan Bansos</h4>
				
			</div>
			<div class="card-body">
				<div class="table-responsive">
						<table id="tablePengaduan" class="table table-condensed table-bordered table-colored table-custom m-0" 
						style="width:100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Pelapor</th>
									<th>Nik Pelapor</th>
									<th>Alamat Pelapor</th>
									<!-- <th>Desa Pelapor</th> -->
									<!-- <th>Kecamatan Pelapor</th> -->
									<th>Nik Terlapor</th>
									<th>Nama Terlapor</th>
									<th>Alamat Terlapor</th>
									<!-- <th>Desa Pelapor</th>						
									<th>Kecamatan Pelapor</th>	 --> 				
									<th>Nama Bantuan</th>	 				
									<th>Keterangan Pelaporan</th>	 				
									<th>Aksi</th>	 				
								</tr>
							</thead>
						</table>
				</div>
			</div>
		</div>
		
		<div class="card" id="formInput" style="display:none">
			<div class="section-body" style="display=none">
				<div class="alert alert-success alert-has-icon" id="InfoFotoProfile" style="">
					<div class="alert-icon"><i class="fa fa-warning"></i></div>
					<div class="alert-body">
						<div class="alert-title">Data Detail Pengaduan</div>
						
					</div>
					<div class="row">
								<div class="col-md-12">
									<div class="form-group text-right">
										
										<button type="button" class="btn btn-outline-warning btn-bordered waves-effect w-md waves-light m-b-5" id="back" onclick="onCancel()" ><i class="fas fa-backward"></i> Kembali</button>
									</div>
								</div>
							</div>
				</div>
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-5">
								<div class="alert alert-warning shadow" role="alert">
									<h4 class="alert-heading">Data orang yang telah dilaporakan </h4>
									<p  id="info_nama_bantuan"> </p>
									<p class="mb-0" ></p>
									<div class="info shadow">
										<div class="font-bold text-gray-800 mb-3">
											Identitas
										</div>
										<div>
											<div class="text-sm font-bold text-sw-green">NIK</div> 
											<div class="text-sm text-gray-600" id="info_nik"></div>
										</div>
										<div class="mt-4">
											<div class="text-sm font-bold text-sw-green">Nama Lengkap</div> 
											<div class="text-sm text-gray-600" id="info_nama_lengkap">S..XXXD</div>
										</div>
										<div class="text text-gray-800 font-bold py-3 mt-3">
											Alamat
										</div>
										<div>
											<div class="text-sm font-bold text-sw-green">Alamat Lengkap</div> 
											<div class="text-sm text-gray-600" id="info_alamat"></div>
										</div>

									<div class="mt-4">
										<div class="text-sm font-bold text-sw-green">Provinsi</div> 
										<div class="text-sm text-gray-600">Jawa Barat</div>
									</div>
									<div class="mt-4">
										<div class="text-sm font-bold text-sw-green">Kabupaten/Kota</div> 
										<div class="text-sm text-gray-600"> Kabupaten Bogor </div>
									</div>

									<div class="mt-4">
										<div class="text-sm font-bold text-sw-green">Kecamatan</div> 
										<div class="text-sm text-gray-600" id="info_kecamatan"> </div>
									</div>
										<div class="mt-4">
											<div class="text-sm font-bold text-sw-green">Desa/Kelurahan</div> 
											<div class="text-sm text-gray-600" id="info_desa"></div></div>
									</div>
									<hr>
									
								</div>
							</div>
							<div class="col-7">
								<div class="alert alert-light shadow-lg border border-primary" role="alert">
									<h4 class="alert-heading">Data Pelapor</h4>
									<p id="info_nama_pelaporan"> </p>
										<div class="warning shadow-lg">
											<div class="row">
												<div class="col-6">
													<div class="font-bold text-gray-800 mb-3">
														Identitas
													</div>
													<div>
														<div class="text-sm font-bold text-sw-green">NIK</div> 
														<div class="text-sm text-gray-600" id="info_nik_pelapor"></div>
													</div>
													<div class="mt-4">
														<div class="text-sm font-bold text-sw-green">Nama Lengkap</div> 
														<div class="text-sm text-gray-600" id="info_nama_lengkap_pelapor"></div>
													</div>
													<div class="mt-4">
														<div class="text-sm font-bold text-sw-green">No Telepon</div> 
														<div class="text-sm text-gray-600" id="info_tlp_pelapor"></div>
													</div>
												</div>
												<div class="col-6">
													<div class="text text-gray-800 font-bold mb-3">
														Alamat
													</div>
													<div class="mt-4">
														<div class="text-sm font-bold text-sw-green">Alamat Lengkap</div> 
														<div class="text-sm text-gray-600" id="info_alamat_pengadu"></div>
													</div>
													<div class="mt-4">
														<div class="text-sm font-bold text-sw-green">Provinsi</div> 
														<div class="text-sm text-gray-600">Jawa Barat</div>
													</div>
													
													<div class="mt-4">
														<div class="text-sm font-bold text-sw-green">Kabupaten/Kota</div> 
														<div class="text-sm text-gray-600"> Kabupaten Bogor </div>
													
													</div>

													<div class="mt-4">
														<div class="text-sm font-bold text-sw-green">Kecamatan</div> 
														<div class="text-sm text-gray-600" id="info_kecamatan_pengadu"> </div>
													</div>

													<div class="mt-4">
															<div class="text-sm font-bold text-sw-green">Desa/Kelurahan</div> 
															<div class="text-sm text-gray-600" id="info_desa_pengadu"></div>
														</div>
													</div>

											</div>										
											<div class="row">
												<div class="col-12">
													<div class="font-bold text-gray-800 mb-3">
														Foto Pengaduan
													</div>
													<div id="foto-pegaduan"></div>
												</div>
											</div>
										</div>
								</div>
							</div>
						</div>
						<hr>
							
					</div>
				</div>
			</div>
		</div>
	
	</section>
</div>
