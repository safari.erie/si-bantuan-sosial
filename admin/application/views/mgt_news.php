  <style>
	.invalid-feedbacks {
		width: 100%;
		margin-top: .25rem;
		font-size: 80%;
		color: #dc3545;
}
  </style>

  <div class="main-content">
	
	<section class="section">
		<div class="section-header">
            <h1>Manajemen Berita</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Management Bansos</a></div>
                <div class="breadcrumb-item"><a href="#">Admin</a></div>
                <div class="breadcrumb-item active"><a href="#">Daftar Berita</a></div>
            </div>
        </div>
		<div class="card" id="ListData">
			<div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3" id="info-title">Daftar Berita</h4>
				<div class="col-md-6 text-right">
				<button type="button" class="btn btn-primary btn-flat" onclick="AddBerita();"><i class="fa fa-plus"></i> Tambah Data</button>
				</div>
				
			</div>
			<div class="card-body" id="listData">
				<div class="row pb-4" >
					
					<div class="col-md-3">
						<label for="name">Judul</label>
						<input type="text" class="form-control" id="judul_berita" name="judul_berita"/>
						
					</div>
					<div class="col-md-3 pt-4">
						<a href="javascript:void(0)" onclick="searchdata()" class="btn btn-info"><i class="fa fa-search" aria-hidden="true"></i> Filter</a>
					</div>
				</div>
				<div class="table-responsive">
					<table id="tableBerita" 
						class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Judul Berita</th>
								<th>Isi Berita</th>
								<th>Foto</th>
								<th>Pembuat</th>						
								<th>Aksi</th>						
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		
		<div class="card" id="formInput" style="display:none">
			<div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3" id="info-title-input">Input Berita</h4>
			</div>
			<div class="card-body">
				<div class="row" id="form_proses_berita" >
					<div class="col-md-12">
						<form class="form-horizontal needs-validation" novalidate  id="formBerita"  enctype="multipart/form-data" role="form" method="post" action="">
							<div class="row">
								<div class="col-12">
								<input type="hidden" class="form-control"  id="id_berita" name="id_berita" value="-1" />
									<div class="form-group row">
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Judul Berita
											</div>
										</label>
										<div class="col-md-6">
											<input type="text" class="form-control"  id="judul_berita_inp" name="judul_berita_inp" 
											data-input="wajib" required>
											<div class="invalid-feedback">Judul Berita tidak boleh kosong</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Isi Berita
											</div>
										</label>
										<div class="col-md-6">
											<input type="text" class="summernote"  id="isi_berita" 
											name="isi_berita" />
											<div class="invalid-feedbacks" style="display:none">Isi Berita tidak boleh kosong</div>
										</div>					
														
									</div>
									<div class="form-group row">
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Foto
											</div>
										</label>
										<div class="col-md-6">
											<input type="file" class="form-control" accept="image/gif, image/jpeg, image/png"  id="foto_berita" name="foto_berita" 
											required data-input="wajib" onchange="checkextension()">
											<div class="invalid-feedback">Foto Berita tidak boleh kosong</div>
										</div>
										
									</div>
									<div class="form-group row" id="previewFoto" style="display: none">
										<div class="col-md-3">
										
										</div>
										<div class="col-md-3" >
											<div id="foto_berita_preview"></div>
										</div>
									</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button type="button" class="btn btn-secondary btn-bordered waves-effect w-md m-b-5" onclick="onCancel()">Batal</button>
                                        <button type="button" class="btn btn-success btn-bordered waves-effect w-md waves-light m-b-5" onclick="onSubmitNews()"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-warning btn-bordered waves-effect w-md waves-light m-b-5" onclick="onSubmitEdit()" style="display:none"><i class="fa fa-edit"></i> Update</button>
                                    </div>
                                </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		
	</section>


	
<div id="modal-delete" class="modal fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">            

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <form id="form_delete_kpm" class="form-horizontal" enctype="multipart/form-data" role="form" method="post" action="">
                
                <div class="modal-body">
                    <p class="text-center" id="text_confirm_delete"></p>
                    <strong><p class="text-center" id="text_delete"></p></strong>
                    <p class="text-center" id="text_confirm_info"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect waves-light" onclick="deleteProsesNews()"  id="btn_confirm"><i class="fa fa-trash"></i> Hapus</button>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</div>





