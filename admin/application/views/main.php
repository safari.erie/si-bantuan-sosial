

		<style>
				.widget-two-danger {
						background-color: rgba(245, 112, 122, 0.2);
						border-color: rgba(245, 112, 122, 0.5);
				}

				.widget-two-success{
						background-color: rgba(158, 216, 149, 0.2);
						border-color: rgba(245, 112, 122, 0.5);
				}

				
				.widget-two-info{
						background-color: rgba(46, 193, 165, 0.2);
						border-color: rgba(245, 112, 122, 0.5);
				}
				
				.widget-two-warning{
						background-color: rgba(221, 202, 47, 0.2);
						border-color: rgba(245, 112, 122, 0.5);
				}



				.bg-soft-brown{
					background-color: #8c7a7a;
				}

				.bg-soft-green{
					background-color: #8c7a7a;
				}

				.card-box{
					border: 2px solid #f3f3f3;
					border-radius: 5px;

				}

				

				.header-style{
					padding-top:0;
				}
				
				.font-bold {
						font-weight: 700;
						color: #34395e;
						padding-bottom: 0;
				}

					.text-xs {
							font-size: .75rem;
					}

					.text-orange-400 {
								color: #f6ad55;
					}

					.text-14 {
								font-size: .875rem;
						}

					.text-gray-300 {
							color: #828282;
					}

					.flex {
							display: flex;
					}

					.text-sw-green {
							color: #27ae60;
					}

					.text-xl {
							font-size: 1.25rem;
					}

					.resizable {
						display: block;
						width: 800px;
						height: 500px;
						padding: 0;
						margin: 0 20px 20px 170px;
						float: left;
						border: 2px dashed #ddd;
						overflow: hidden;
						position: relative;
					}

		</style>
      <!-- Main Content -->
      <div class="main-content">

	  <section class="section">
		<div class="section-header">
            <h1>Dashboard Bansos</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Admin</a></div>
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            </div>
        </div>
		<div class="card" id="ListData" style="">
			<!-- <div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3">Dashboard</h4>
			</div> -->
			<div class="card-body">
				<div class="row pb-4">
					<div class="col-md-3">
						<label for="name">Kecamatan</label>
						<select name="kecamatan" class="form-control" id="kecamatans">
							<option value="0"> Pilih Salah Satu </option>
						</select>
					</div>
					<div class="col-md-3">
						<label for="name">Desa</label>
						<select name="desa" class="form-control" id="desas">
							<option value="0">-- Pilih Desa --</option>
						</select>
					</div>
					<div class="col-md-3">
						<label for="name">Status Verifikasi</label>
						<select name="status_verifikasi" class="form-control" id="status_verifikasi">
							<option value="0" selected>Belum Verifikasi</option>
							<option value="1">Sudah Verifikasi</option>
							
						</select>
					</div>
					
					<div class="col-md-3 pt-4">
						<a href="javascript:void(0)" onclick="FilterDashboard()" class="btn btn-primary"> <i class="fa fa-filter" aria-hidden="true"></i> Filter</a>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-6 col-12">
						<div class="card card-statistic-1 shadow-primary card-box widget-box-two widget-two-danger">
							<div class="card-wrap">
								<div class="card-header" style="padding-top:10px">
									<div class="font-bold" >
										BANTUAN PROVINSI
									</div>
								</div>
								<div class="card-body">
									<div class="font-bold text-xl text-sw-green" id="card_info_all_bantuan_prov">
										<?php echo number_format($CProvAll,0,",","."); ?> KPM
									</div>
									<div class="text-xs text-gray-300">
											Sumber : Kementerian Sosial
									</div>
									<div class="flex">
										<div class="w-1/2 mt-2 pr-2 border-r">
											<div class="text-xs text-gray-600">DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_dtks_prov">  <?php echo number_format($CDtksProv,0,",","."); ?> KPM  </div>
										</div> 
										<div class="w-1/2 mt-2 pl-2">
											<div class="text-xs text-gray-600">Non-DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_non_dtks_prov">		<?php echo number_format($CNonDtksProv,0,",",".")?>		</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-12">
						<div class="card card-statistic-1 shadow-primary card-box widget-box-two widget-two-success">
							<div class="card-wrap">
								<div class="card-header" style="padding-top:10px">
									<div class="font-bold" >
										BANSOS BANTUAN PRESIDEN
									</div>
								</div>
								<div class="card-body">
									<div class="font-bold text-xl text-sw-green" id="card_info_all_bantuan_presiden" >
										<?php echo number_format($CPresidenAll,0,",","."); ?> KPM
									</div>
									<div class="text-xs text-gray-300">
											Sumber : Kementerian Sosial
									</div>
									<div class="flex">
										<div class="w-1/2 mt-2 pr-2 border-r">
											<div class="text-xs text-gray-600">DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_dtks_presiden"> <?php echo number_format($CDtksPresiden,0,",","."); ?> KPM  </div>
										</div> 
										<div class="w-1/2 mt-2 pl-2">
											<div class="text-xs text-gray-600">Non-DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_non_dtks_presiden"><?php echo number_format($CNonDtksPresiden,0,",","."); ?> KPM 	
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-12">
						<div class="card card-statistic-1 shadow-primary card-box widget-box-two widget-two-info">
							<div class="card-wrap">
								<div class="card-header" style="padding-top:10px">
									<div class="font-bold" >
										BANSOS BUPATI
									</div>
								</div>
								<div class="card-body">
									<div class="font-bold text-xl text-sw-green" id="card_info_all_bantuan_bupati" >
										<?php echo number_format($CBupatiAll,0,",","."); ?> KPM
									</div>
									<div class="text-xs text-gray-300">
											Sumber : Kementerian Sosial
									</div>
									<div class="flex">
										<div class="w-1/2 mt-2 pr-2 border-r">
											<div class="text-xs text-gray-600">DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_dtks_bupati"> <?php echo number_format($CDtksBupati,0,",","."); ?></div>
										</div> 
										<div class="w-1/2 mt-2 pl-2">
											<div class="text-xs text-gray-600">Non-DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_non_dtks_presiden"><?php echo number_format($CNonDtksBupati,0,",",".");?> KPM 	
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 col-sm-6 col-12">
						<div class="card card-statistic-1 shadow-primary card-box widget-box-two widget-two-warning">
							<div class="card-wrap">
								<div class="card-header" style="padding-top:10px">
									<div class="font-bold" >
										BLT DANA DESA
									</div>
								</div>
								<div class="card-body">
									<div class="font-bold text-xl text-sw-green" id="card_info_all_bantuan_dd">
										<?php echo number_format($CDdAll,0,",","."); ?> KPM
									</div>
									<div class="text-xs text-gray-300">
											Sumber : Kementerian Sosial
									</div>
									<div class="flex">
										<div class="w-1/2 mt-2 pr-2 border-r">
											<div class="text-xs text-gray-600">DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_dtks_dd"> <?php echo number_format($CDtksDd,0,",","."); ?></div>
										</div> 
										<div class="w-1/2 mt-2 pl-2">
											<div class="text-xs text-gray-600">Non-DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_non_dtks_dd"><?php echo number_format($CNonDtksDd,0,",",".");?> KPM 	
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>

				</div>	
				
				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-6 col-12">
						<div class="card card-statistic-1 shadow-primary card-box widget-box-two widget-two-danger">
							<div class="card-wrap">
								<div class="card-header" style="padding-top:10px">
									<div class="font-bold" >
										SEMBAKO REGULER
									</div>
								</div>
								<div class="card-body">
									<div class="font-bold text-xl text-sw-green">
										<?php echo number_format($CSembakoRegulerAll,0,",","."); ?> KPM
									</div>
									<div class="text-xs text-gray-300">
											Sumber : Kementerian Sosial
									</div>
									<div class="flex">
										<div class="w-1/2 mt-2 pr-2 border-r">
											<div class="text-xs text-gray-600">DTKS</div> 
											<div class="text-14 text-orange-400"> <?php echo number_format($CDtksSembakoReguler,0,",","."); ?></div>
										</div> 
										<div class="w-1/2 mt-2 pl-2">
											<div class="text-xs text-gray-600">Non-DTKS</div> 
											<div class="text-14 text-orange-400"><?php echo number_format($CNonDtksSembakoReguler,0,",",".");?> KPM 	
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 col-sm-6 col-12">
						<div class="card card-statistic-1 shadow-primary card-box widget-box-two widget-two-success">
							<div class="card-wrap">
								<div class="card-header" style="padding-top:10px">
									<div class="font-bold" >
										KARTU SEMBAKO PERLUASAN
									</div>
								</div>
								<div class="card-body">
									<div class="font-bold text-xl text-sw-green" id="card_info_all_bantuan_perluasan">
										<?php echo number_format($CSembakoPerluasanAll,0,",","."); ?> KPM
									</div>
									<div class="text-xs text-gray-300">
											Sumber : Kementerian Sosial
									</div>
									<div class="flex">
										<div class="w-1/2 mt-2 pr-2 border-r">
											<div class="text-xs text-gray-600">DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_dtks_bantuan_perluasan"> <?php echo number_format($CDtksSembakoPerluasan,0,",","."); ?></div>
										</div> 
										<div class="w-1/2 mt-2 pl-2">
											<div class="text-xs text-gray-600">Non-DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_non_dtks_bantuan_perluasan"><?php echo number_format($CNonDtksSembakoPerluasan,0,",",".");?> KPM 	
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>


					<div class="col-lg-3 col-md-6 col-sm-6 col-12">
						<div class="card card-statistic-1 shadow-primary card-box widget-box-two widget-two-info">
							<div class="card-wrap">
								<div class="card-header" style="padding-top:10px">
									<div class="font-bold" >
										BANTUAN SOSIAL TUNAI 
									</div>
								</div>
								<div class="card-body">
									<div class="font-bold text-xl text-sw-green" id="card_info_all_blt">
										<?php echo number_format($CBltAll,0,",","."); ?> KPM
									</div>
									<div class="text-xs text-gray-300">
											Sumber : Kementerian Sosial
									</div>
									<div class="flex">
										<div class="w-1/2 mt-2 pr-2 border-r">
											<div class="text-xs text-gray-600">DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_dtks_bantuan_blt"> <?php echo number_format($CDtksBlt,0,",","."); ?></div>
										</div> 
										<div class="w-1/2 mt-2 pl-2">
											<div class="text-xs text-gray-600">Non-DTKS</div> 
											<div class="text-14 text-orange-400"  id="card_info_non_dtks_bantuan_blt"><?php echo number_format($CNonDtksBlt,0,",",".");?> KPM 	
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 col-sm-6 col-12">
						<div class="card card-statistic-1 shadow-primary card-box widget-box-two widget-two-warning">
							<div class="card-wrap">
								<div class="card-header" style="padding-top:10px">
									<div class="font-bold" >
										PKH 
									</div>
								</div>
								<div class="card-body">
									<div class="font-bold text-xl text-sw-green" id="card_info_all_pkh">
										<?php echo number_format($CPkhAll,0,",","."); ?> KPM
									</div>
									<div class="text-xs text-gray-300">
											Sumber : Kementerian Sosial
									</div>
									<div class="flex">
										<div class="w-1/2 mt-2 pr-2 border-r">
											<div class="text-xs text-gray-600">DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_dtks_pkh"> <?php echo number_format($CDtksPkh,0,",","."); ?></div>
										</div> 
										<div class="w-1/2 mt-2 pl-2">
											<div class="text-xs text-gray-600">Non-DTKS</div> 
											<div class="text-14 text-orange-400" id="card_info_non_dtks_pkh"><?php echo number_format($CNonDtksPkh,0,",",".");?> KPM 	
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</section>


	<section class="section">
		<div class="row">
			<div class="col-12">
			<div class="card-header">
							<h4>Graphic</h4>
						</div>
				</div>
				<div class="card-body">
					<div id="chartdiv" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
				</div>	
				
			</div>
		</div>
	</section>
</div>
      