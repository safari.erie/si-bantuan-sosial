	<style>
		.modal-full {
			max-width: 98%;
		}

		.map-content {
			-ms-flex-preferred-size: 0;
			flex-basis: 0;
			-webkit-box-flex: 1;
			-ms-flex-positive: 1;
			flex-grow: 1;
			max-width: 100%;
			background-color: #777776;
			position: relative;
		}

		#map {
			margin: 0px;
			width: 100%;
			height: 400px;
			padding: 0px;
		}
	</style>
  <div class="main-content">
	
	<section class="section">
		<div class="section-header">
            <h1>Daftar Verifikasi Bansos</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Daftar Verifikasi Bansos</a></div>
                <div class="breadcrumb-item"><a href="#">Admin</a></div>
                <div class="breadcrumb-item active"><a href="#">Daftar Verifikasi Bansos</a></div>
            </div>
        </div>
		<div class="card" id="ListData" style="">
                <div class="card-header form-row">
                    <h4 class="col-md-6 col-sm-3">Daftar Verifikasi Bansos</h4>
					<div class="col-md-6 text-right"> 
                        <button type="button" class="btn btn-primary btn-flat" id="addBtnVerifikasi" onclick="AddVerifikasi('');"><i class="fa fa-plus"></i> Verifikasi</button> &nbsp; &nbsp; &nbsp; 
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive" id="tableVerifikasi">
							<table id="tableVerifikasiContent" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Nik</th>
										<th>Alamat</th>
										<th>Nama Bantuan</th>
										<th>Status Verifikasi</th>				
										<th>Foto Ktp</th>						
										<th>Foto KK</th>						
										<th>Aksi</th>						
									</tr>
								</thead>
							</table>
                    </div>
					<div class="row" id="form_proses" style="display:none;">
					
						<div class="col-sm-12">
							<form class="form-horizontal" id="formVerifikasi"  enctype="multipart/form-data" role="form" method="post" action="">
								<input type="hidden" name="id_verifikasi" id="id_verifikasi"/>
								<div class="row">
									<div class="col-12">
										<div class="form-group row">
											<label class="col-md-1 control-label text-left">
												<div class="">
													Nama KPM
												</div>
											</label>
											<div class="col-md-5">
												<input type="text" class="form-control" disabled="disabled" id="nama_kpm" name="name_kpm">
												<input type="hidden" class="form-control" id="id_kpm" name="id_kpm">
												
											</div>
											<div class="col-md-4">
												<button type="button" class="btn btn-warning" name="addKpmTable" id="addKpmTable">Pilih Kpm</button>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-1 control-label text-left">
												<div class="">
													Keterangan
												</div>
											</label>
											<div class="col-md-5">
												<div id="rd_ket">
													<div class="form-check">
														<input class="form-check-input" type="radio" name="RadioKeterangan" value="layak" id="RadioKeterangan1" checked="">
														<label class="form-check-label" for="RadioKeterangan1">
															Layak
														</label>
													</div>
													<div class="form-check">
														<input class="form-check-input" type="radio" name="RadioKeterangan" value="meninggal" id="RadioKeterangan2">
														<label class="form-check-label" for="RadioKeterangan2">
															Meninggal
														</label>
													</div>
													<div class="form-check">
														<input class="form-check-input" type="radio" name="RadioKeterangan" value="pindah" id="RadioKeterangan3" >
														<label class="form-check-label" for="RadioKeterangan3">
															Pindah
														</label>
													</div>
													<div class="form-check">
														<input class="form-check-input" type="radio" name="RadioKeterangan" value="tidak Mampu" id="RadioKeterangan4" >
														<label class="form-check-label" for="RadioKeterangan4">
															Tidak Mampu
														</label>
													</div>
													<div class="form-check">
														<input class="form-check-input" type="radio" name="RadioKeterangan" value="menerima bantuan lain" id="RadioKeterangan5" >
														<label class="form-check-label" for="RadioKeterangan5">
															Sudah Menerima Bantuan Lain
														</label>
													</div>
												</div>
											</div>
											
										</div>
										<div class="form-group row">
											<label class="col-md-1 control-label text-left">
												<div class="">
													Foto Ktp
												</div>
											</label>
											<div class="col-md-5">												
												<input type="file" name="fotoKtp" id="fotoKtp" class="form-control">
											</div>
											<div class="col-md-3" id="DivFotoKtp" style="display:none">
												<div class="col-md-4">
													<div id="fotKtp"></div>
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-1 control-label text-left">
												<div class="">
													Foto Rumah
												</div>
											</label>
											<div class="col-md-5">												
												<input type="file" name="fotoKk" id="fotoKk" class="form-control">
											</div>
											<div class="col-md-3" id="DivFotoRumah" style="display:none">
												<div class="col-md-4">
													<div id="fotoRumah"></div>
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-1 control-label text-left">
												<div class="">
													Geometry
												</div>
											</label>
											<div class="col-md-5">							
                               					 <input type="text"  required="" disabled="disabled" name="lat_c" id="lat_c" class="form-control"/>
											</div>
											<div class="col-md-5">							
												<input type="text"  required="" disabled="disabled" name="lon_c" id="lon_c" class="form-control"/>
											</div>
										</div>
										
									</div>
								</div>

								

								<div class="row">
									<div class="map-content d-flex">
										<div id="map"></div>
									</div>
								</div>

								<hr />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button type="button" class="btn btn-secondary btn-bordered waves-effect w-md m-b-5" onclick="onCancel()">Batal</button>
                                        <button type="button" class="btn btn-success btn-bordered waves-effect w-md waves-light m-b-5" id="btn_insert" onclick="onSubmitVerifikasi()"><i class="fa fa-plus" aria-hidden="true"></i> Simpan</button>
                                        <button type="button" class="btn btn-warning btn-bordered waves-effect w-md waves-light m-b-5" id="btn_update" style="display:none" onclick="onSubmitUpdateVerifikasi()"><i class="fas fa-edit" aria-hidden="true"></i> Update</button>
                                    </div>
                                </div>
                            </div>
							</form>
						</div>
					</div>


					

                </div>
            </div>
		
		
	</section>


	<!-- modal here -->
<div class="modal fade" id="ModalFormKpm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleFormUser">Data KPM</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
				<div class="row pb-4" >
					<input type="hidden" id="kode_wilayah" name="kode_wilayah" />
					<div class="col-md-3">
						<label for="name">Kecamatan</label>
						<select name="kecamatan" class="form-control" id="kecamatans">_mo
						</select>
					</div>
					<div class="col-md-3">
						<label for="name">Desa</label>
						<select name="desa" class="form-control" id="desas">
							<option value="0">-- Pilih Desa --</option>
						</select>
					</div>
					<div class="col-md-3">
						<label for="name">Jenis Bantuan</label>
						<label for="name">Desa</label>
						<select name="jenisBantuans" class="form-control" id="jenisBantuans">
							<option value="0">-- Pilih Desa --</option>
						</select>
					</div>
					<div class="col-md-3 pt-4">
						<a href="javascript:void(0)" onclick="searchdata()" class="btn btn-primary"> Filter</a>
					</div>
				</div>
				<div class="table-responsive">
						<table id="datatableKpm" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Id Kpm</th>
									<th>Nama</th>
									<th>Nik</th>
									<th>Alamat</th>					
								</tr>
							</thead>
						</table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

</div>
