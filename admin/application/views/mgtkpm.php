
  <div class="main-content">
	
	<section class="section">
		<div class="section-header">
            <h1>Manajemen Kpm</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Management Bansos</a></div>
                <div class="breadcrumb-item"><a href="#">Admin</a></div>
                <div class="breadcrumb-item active"><a href="#">Input Penerima Bansos</a></div>
            </div>
        </div>
		<div class="card" id="ListData">
			<div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3" id="info-title">Tabel Kpm</h4>
				<div class="col-md-6 text-right">
					<button type="button" class="btn btn-success btn-flat" onclick="showUploadKpm()">
						<i class="fa fa-archive"></i> Upload Kpm Bantuan
					</button>  &nbsp; 
					<button type="button" class="btn btn-primary btn-flat" onclick="AddKpm();"><i class="fa fa-plus"></i> Tambah Data</button> &nbsp; &nbsp; &nbsp; 
				</div>
			</div>
			<div class="card-body" id="listDataKpm">
				<div class="row pb-4" >
					<div class="col-md-3">
						<label for="name">Kecamatan</label>
						<select name="kecamatan" class="form-control" id="kecamatans">
						</select>
					</div>
					<div class="col-md-3">
						<label for="name">Desa</label>
						<select name="desa" class="form-control" id="desas">
							<option value="0">-- Pilih Desa --</option>
						</select>
					</div>
					<div class="col-md-3">
						<label for="name">Nik</label>
						<input type="text" class="form-control" id="nik" name="nik"/>
						<div class="validate"></div>
					</div>
					<div class="col-md-3 pt-4">
						<a href="javascript:void(0)" onclick="searchdata()" class="btn btn-primary"> Filter</a>
					</div>
				</div>
				<div class="table-responsive">
						<table id="datatable" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Nik</th>
									<th>Alamat</th>
									<th>Kecamatan</th>
									<th>Desa</th>
									<th>Status</th>						
									<th>DTKS</th>						
									<th>Aksi</th>						
								</tr>
							</thead>
						</table>
				</div>
			</div>
		</div>
		
		<div class="card" id="formInput" style="display:none">
			<div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3" id="info-title-input">Input Kpm</h4>
			</div>
			<div class="card-body">
				<div class="row" id="form_proses_kpm" >
					<div class="col-md-12">
						<form class="form-horizontal" id="formKpm"  enctype="multipart/form-data" role="form" method="post" action="">
							<div class="row">
								<div class="col-12">
									<div class="form-group row">
										<input type="hidden" class="form-control"  id="id_kpm" name="id_kpm" value="-1">
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Nik
											</div>
										</label>
										<div class="col-md-6">
											<input type="text" class="form-control"  id="nik_kpm" name="nik_kpm">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Nama KPM
											</div>
										</label>
										<div class="col-md-6">
											<input type="text" class="form-control"  id="nama_kpm" name="name_kpm">
										</div>									
									</div>

									<div class="form-group row">
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Alamat 
											</div>
										</label>
										<div class="col-md-8">
											<textarea class="form-control" id="alamat" name="alamat" style="height:90px"></textarea>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Kecamatan 
											</div>
										</label>
										<div class="col-md-8">
											<select class="form-control" id="inputKec" name="inputKec">
												<option value="0">Pilih Kecamatan</option>
											</select>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Desa 
											</div>
										</label>
										<div class="col-md-8">
											<select class="form-control" id="inputDesa" name="inputDesa">
												<option value="0">Pilih Kelurahan</option>
											</select>
										</div>
									</div>
									
									<div class="form-group row">
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Kelayakan 
											</div>
										</label>
										<div class="col-md-6" id="radioLayak">
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" id="kelayakanRadio1" name="kelayakanRadio" value="1">
												<label class="form-check-label" for="kelayakanRadio1">Ya</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" id="kelayakanRadio2" name="kelayakanRadio" value="0">
												<label class="form-check-label" for="kelayakanRadio2">Tidak</label>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												Status 
											</div>
										</label>
										<div class="col-md-4">
											<select name="cmb_status" id="cmb_status" class="form-control">
												<option value="1">Aktif</option>
												<option value="0">Tidak Aktif</option>
											</select>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-md-3 control-label text-left pt-2">
											<div class="">
												DTKS 
											</div>
										</label>
										<div class="col-md-4">
											<select name="cmb_status_dtks" id="cmb_status_dtks" class="form-control">
												<option value="1">DTKS</option>
												<option value="0">Non DTKS</option>
											</select>
										</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button type="button" class="btn btn-secondary btn-bordered waves-effect w-md m-b-5" onclick="onCancel()">Batal</button>
                                        <button type="button" class="btn btn-success btn-bordered waves-effect w-md waves-light m-b-5" onclick="onSubmitKpm()">Simpan</button>
                                    </div>
                                </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	
<div id="modal-cant-delete" class="modal fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">            

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
			<div class="modal-body">
					<div id="text_confirm">
					</div>
                </div>
                <div class="modal-footer">             
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div id="modal-delete" class="modal fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">            

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <form id="form_delete_kpm" class="form-horizontal" enctype="multipart/form-data" role="form" method="post" action="">
                
                <div class="modal-body">
                    <p class="text-center" id="text_confirm_delete"></p>
                    <strong><p class="text-center" id="text_delete"></p></strong>
                    <p class="text-center" id="text_confirm_info"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect waves-light" onclick="" data-dismiss="modal" id="btn_confirm">Yes</button>
                    
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>



<div id="modal-upload-kpm" class="modal fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">            

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">
					Upload Data Kpm dan Bansos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <form id="form_upload_kpm" class="form-horizontal" enctype="multipart/form-data" role="form" method="post" action="">
                
                <div class="modal-body">
					<div class="form-group">
						<label for="jeniBantuan" class="col-form-label">Jenis Bantuan</label>
						<select name="jenis_bantuan" id="bantuanId" class="form-control">
                    
                  		</select>
					</div>
					<div class="form-group">
						<label for="jeniBantuan" class="col-form-label">Pilih Kecamatan</label>
						<select name="cmb_kecamatan_modal" id="cmb_kecamatan_modal" class="form-control">
                    
                  		</select>
					</div>

					<div class="form-group">
						<label for="jeniBantuan" class="col-form-label">Pilih Desa</label>
						<select name="cmb_desa_modal" id="cmb_desa_modal" class="form-control">
							<option value="0">Pilih Desa</option>
                  		</select>
					</div>

					<div class="form-group">
						<label for="jeniBantuan" class="col-form-label">Pilih DTKS</label>
						<select name="cmb_dtks_modal" id="cmb_dtks_modal" class="form-control">
							<option value="1">DTKS</option>
							<option value="0">NON DTKS</option>
                  		</select>
					</div>
					
					<div class="form-group">
						<label for="upload_file" class="col-form-label">Upload File</label>
						<input type="file" class="form-control" id="upload_excel" name="upload_excel" accept=".xls,.xlsx" onchange="checkextension()">
					</div>
					<div class="form-group">
						<div class="alert alert-primary" role="alert">
							Pastikan file yang akan di upload sesaui dengan template
							<a href="javascript:void(0)" id="template_file" name="template_file"
							onclick="TemplateImport()" > Download File</a> 
						</div>
					
					</div>
					<div class="form-group">
					<button type="button" class="btn btn-success waves-effect waves-light" onclick="SaveUploadKpmBansos()" name="btn_save_upload" id="btn_save_upload"><i class="fa fa-upload"> </i> Simpan Upload</button>
					</div>
				</div>
				<hr>
                <div class="modal-footer">
                   
                </div>
				
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>




<div id="modal-confirmation-proses-upload" class="modal fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">            

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">
					Konfirmasi Upload</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <form id="form_upload_kpm" class="form-horizontal" enctype="multipart/form-data" role="form" method="post" action="">
                
                <div class="modal-body">				
					<div class="form-group">
						<div class="alert alert-primary" role="alert">
							<p id="msg_sukses_title">Nik Yang berhasil diupload</p>
							<p id="msg_sukses_nik"></p>
						</div>
					
					</div>

					<div class="form-group">
						<div class="alert alert-danger" role="alert">
							<p id="msg_unsukses_title">Nik Yang Gagal  diupload</p>
							<p id="msg_unsukses"></p>
						</div>
					
					</div>
					
				</div>
				<hr>
                <div class="modal-footer">
				<button type="button" class="btn btn-success waves-effect waves-light" onclick="closes()" name="ctn_close_conf" id="ctn_close_conf"><i class="fa fa-close"> </i> Tutup</button>
                </div>
				
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>


<!-- modal confem delete -->



