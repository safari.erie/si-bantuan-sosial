
	<style>
		.modal-full {
			max-width: 98%;
		}
	</style>
  <div class="main-content">
	
	<section class="section">
		<div class="section-header">
            <h1>Daftar Penerima Bansos</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Management Bansos</a></div>
                <div class="breadcrumb-item"><a href="#">Admin</a></div>
                <div class="breadcrumb-item active"><a href="#">Input Penerima Bansos</a></div>
            </div>
        </div>
		<div class="card" id="ListData">
			<div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3">Daftar Penerima Bansos</h4>
				<div class="col-md-6 text-right">
				<button type="button" class="btn btn-primary btn-flat" onclick="AddBansos();"><i class="fa fa-plus"></i> Tambah Data</button> &nbsp; &nbsp; &nbsp; 
				
			</div>
			</div>
			<div class="card-body">
				<div class="row pb-4" >
					<input type="hidden" id="kode_wilayah_login" name="kode_wilayah_login" value="<?php echo ($kodeWilayahLogin != null) ? $kodeWilayahLogin: 0;?>" />
					<div class="col-md-3">
						<label for="name">Kecamatan</label>
						<select name="kecamatan" class="form-control" id="kecamatans">_mo
						</select>
					</div>
					<div class="col-md-3">
						<label for="name">Desa</label>
						<select name="desa" class="form-control" id="desas">
							<option value="0">-- Pilih Desa --</option>
						</select>
					</div>
					<div class="col-md-3">
						<!-- <label for="name">Nik</label> -->
						<input type="hidden" class="form-control" id="nik" name="nik"/>
						<div class="validate"></div>
					</div>
					<div class="col-md-3 pt-4">
						<a href="javascript:void(0)" onclick="searchdata()" class="btn btn-primary"> Filter</a>
						<a href="javascript:void(0)" onclick="downloadData()" class="btn btn-success"> <i class="fa fa-download" aria-hidden="true"></i> Download Data</a>
					</div>
				</div>
				<div class="table-responsive">
					<table id="datatable" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Nik</th>
								<th>Alamat</th>
								<th>Kecamatan</th>
								<th>Desa</th>
								<th>Nama Bantuan</th>
								<th>Status</th>						
							</tr>
						</thead>
					</table>
				</div>
				<div class="row pt-4" >
					<div class="card-header form-row">
						<h4 class="col-md-6 col-sm-3">Daftar Penerima Bansos Ganda</h4>
					</div>
					<div class="table-responsive">
					<table id="data_duplikat" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Nik</th>
								<th>Alamat</th>
								<th>Kecamatan</th>
								<th>Desa</th>
								<th>Nama Bantuan</th>
								<th>Status</th>						
							</tr>
						</thead>
					</table>
				</div>
				</div>
			</div>
		</div>

		<div class="card" id="formInput" style="display:none">
			<div class="card-header form-row">
				<h4 class="col-md-6 col-sm-3" id="info-title-input">Input Kpm Bansos</h4>
			</div>

			<div class="card-body">
				<div class="row" id="form_proses_kpm" >
					<div class="col-md-12">
						<form class="form-horizontal" id="formBansos"  enctype="multipart/form-data" role="form" method="post" action="">
							<div class="row">
								<div class="col-12">
									<input type="hidden" class="form-control" id="id_bansos" name="id_bansos" value='-1'>
									<div class="form-group row">
										<label class="col-md-1 control-label text-left">
											<div class="">
												Nama KPM
											</div>
										</label>
										<div class="col-md-5">
											<input type="text" class="form-control" disabled="disabled" id="nama_kpm" name="name_kpm">
											<input type="hidden" class="form-control" id="id_kpm" name="id_kpm">
											
										</div>
										<div class="col-md-4">
											<button type="button" class="btn btn-warning" name="addKpmTable" id="addKpmTable">Pilih Kpm</button>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-1 control-label text-left">
											<div class="">
												 Jenis Bantuan
											</div>
										</label>
										<div class="col-md-5">
											<select name="idBantuan" id="idBantuan" class="form-control">
												<option value="0">-- Pilih -- </option>
											</select>
											
										</div>
										
									</div>
								</div>
							</div>

							<hr>
							<div class="row">
								<div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button type="button" class="btn btn-secondary btn-bordered waves-effect w-md m-b-5" onclick="onCancel()">Batal</button>
                                        <button type="button" class="btn btn-success btn-bordered waves-effect w-md waves-light m-b-5" onclick="onSubmitBantuan()">Simpan</button>
                                    </div>
                                </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		
	
	</section>


<!-- modal here -->
<div class="modal fade" id="ModalFormKpm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleFormUser">Modal Data Kpm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
				<div class="table-responsive">
						<table id="datatableKpm" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Id Kpm</th>
									<th>Nama</th>
									<th>Nik</th>
									<th>Alamat</th>					
								</tr>
							</thead>
						</table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

</div>
