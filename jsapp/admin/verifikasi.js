
const acces_url = {
	GetDataKpm : SITE_URL+"verifikasi/getDataKpm",
	saveValidasi : SITE_URL+"verifikasi/saveVerifikasi",
	GetDataKecamatan : SITE_URL+"verifikasi/WilayahKec",
	GetDesaByCodeKec : SITE_URL+"MgtKpm/getDesa",
	GetCheckUserKodeWilayah : SITE_URL+"verifikasi/checkUserLoginWilayah",
	GetBantuan : SITE_URL+"verifikasi/getBantuan",
	GetVerifikasiById : SITE_URL+"VerifikasiApprove/GetVerifikasiById",
	SaveProsesEditVerifikasi : SITE_URL+"Verifikasi/SaveProsesEditVerifikasi",
}

var tableKpm;
var tableVerifikasi;
var kode_kec = 0;

$(document).ready(function(){	

	tableVerifikasi = $("#tableVerifikasiContent").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,10,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "Verifikasi/get",
			"type": "POST",
			"datatype": "json",
		},
	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "nama", "name": "nama", orderable: true,className: 'text-center' },
			{ "data": "nik", "name": "nik", orderable: true ,className: 'text-center'},
			{ "data": "alamat", "name": "alamat", orderable: true,className: 'text-center' },
			{ "data": "nama_bantuan", "name": "nama_bantuan", orderable: false },
			{ "data": "status_verifikasi", "name": "status_verifikasi", orderable: false },
			{ "data": "foto_ktp", "name": "foto_ktp", orderable: false },
			{ "data": "foto_rumah", "name": "foto_rumah", orderable: false },
			{ "data": "aksi", "name": "aksi", orderable: false },
		]
	   
	});
});


var map;
var marker;
let AddVerifikasi = (methodType,lat, lon) => {	
	document.getElementById("form_proses").style.display = "";
	document.getElementById("tableVerifikasi").style.display = "none";
	document.getElementById("addBtnVerifikasi").style.display = "none";
	
	if(methodType == ''){
			$('#addKpmTable').attr('disabled',false);
			$('#DivFotoKtp').css('display','none');
			$('#DivFotoRumah').css('display','none');
			$('#btn_insert').css('display','');
			$('#btn_update').css('display','none');
			$('#nama_kpm').val('');
			$('#id_kpm').val('');
			$('#lat_c').val('');
			$('#lon_c').val('');
			document.querySelector('input[name="RadioKeterangan"]:checked').checked = false;
			map = L.map('map', {
				fullscreenControl: {
					pseudoFullscreen: false
				},
				center: [-6.595038, 106.816635],
			// center: [-6.533918021503638, 106.80722936714096],
				zoom: 9
			});
			var basemaps = [
				L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
					maxZoom: 18,
					id: 'mapbox.streets',
					accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
				}),
				L.tileLayer("//stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png", {
					attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
					subdomains: "abcd",
					maxZoom: 20,
					minZoom: 0,
					label: "Toner Lite"
				}),
				L.tileLayer("//stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png", {
					attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
					subdomains: "abcd",
					maxZoom: 20,
					minZoom: 0,
					label: "Toner"
				})
			];
		
			map.addControl(
				L.control.basemaps({
					basemaps: basemaps,
					tileX: 0,
					tileY: 0,
					tileZ: 1
				})
			);
		
		
			map.on('click', function(e) {
				$('#lat_c').val(e.latlng.lat);
				$('#lon_c').val(e.latlng.lng);
				if (marker) { // check
					map.removeLayer(marker); // remove
				}
				marker = new L.Marker(e.latlng).addTo(map); // set
				//L.marker(e.latlng).addTo(map);
		
			});

	}else{
		$('#addKpmTable').attr('disabled',true);
		map = L.map('map', {
			fullscreenControl: {
				pseudoFullscreen: false
			},
			center: [-6.595038, 106.816635],
		   // center: [-6.533918021503638, 106.80722936714096],
			zoom: 9
		});
		var basemaps = [
			L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
				maxZoom: 18,
				id: 'mapbox.streets',
				accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
			}),
			L.tileLayer("//stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png", {
				attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
				subdomains: "abcd",
				maxZoom: 20,
				minZoom: 0,
				label: "Toner Lite"
			}),
			L.tileLayer("//stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png", {
				attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
				subdomains: "abcd",
				maxZoom: 20,
				minZoom: 0,
				label: "Toner"
			})
		];
	
		map.addControl(
			L.control.basemaps({
				basemaps: basemaps,
				tileX: 0,
				tileY: 0,
				tileZ: 1
			})
		);
		
		if(lat != null && lon != null){
			marker = L.marker([lat, lon]).addTo(map);
		}
	
	
		map.on('click', function(e) {
			$('#lat_c').val(e.latlng.lat);
			$('#lon_c').val(e.latlng.lng);
			if (marker) { // check
				map.removeLayer(marker); // remove
			}
			marker = new L.Marker(e.latlng).addTo(map); // set
			//L.marker(e.latlng).addTo(map);
	
		});
	}

}




let onCancel = () =>{
	document.getElementById("form_proses").style.display = "none";
	document.getElementById("tableVerifikasi").style.display = "";
	document.getElementById("addBtnVerifikasi").style.display = "";
	map.remove();
}



$('#addKpmTable').on('click',function(){
	$('#ModalFormKpm').modal("show");
	GetKecamatan();
	GetUserKodeWilayah();
	GetBantuan();
	setTimeout(function(){ getDesa(); }, 3000);

		
	$("#kecamatans").on('change',function(){
		let kdKec = this.value;
		$.ajax({
			url:acces_url.GetDesaByCodeKec,
			type: "POST",
			dataType : "json",
			data: {"kode_kec":kdKec},
			beforeSend: () => {

			},
			success: (response) => {
				let countData = response.Data.length;
				let html = '';
				if(countData > 0){				
					html += '<option value="0"> Pilih Salah Satu </option>';
					$.each(response.Data,(i,v) => {					
						html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
					});

				}else{
					html += '<option value="-1"> Data tidak ada </option>';
				}
				$("select[name='desa']").empty();
				$("select[name='desa']").append(html);
			}
		})

	});
	
	tableKpm = $("#datatableKpm").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
		"paging": true,
		"destroy": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url":acces_url.GetDataKpm,
			"type": "POST",
			"datatype": "json",
			"data": function (d) { 
				d.kode_wilayah   = $('#kode_wilayah').val();//3201161
				d.kode_kec   = $('#kecamatans').val();//3201161
				d.kode_desa  = $('#desas').val();
				d.bantuan 	 = $('#jenisBantuans').val();
            }
		},	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "id_kpm", "name": "id_kpm", orderable: true ,className: 'text-center'},
			{
                "render": function (data, type, full, meta) {
					
                    return '<a href="javascript:void(0)" class="" >' + full.nama + '</a>'
                }
            },
			{ "data": "nik", "name": "nik", orderable: true ,className: 'text-center'},
			{ "data": "alamat", "name": "alamat", orderable: true,className: 'text-center' },
		]
	   
	});


	// event onclick modal op
    $('#datatableKpm tbody').on('click', 'a', function () {
        var data = tableKpm.row($(this).parents('tr')).data();
		$("#ModalFormKpm").modal("hide");
		$('#nama_kpm').val(data.nama);
		$('#id_kpm').val(data.id_kpm);
    });

});

onSubmitVerifikasi = () => {
	let keterangan = $('input[name="RadioKeterangan"]:checked').val();
	let id_kpm = $('#id_kpm').val();
	let lat_c = $('#lat_c').val();
	let lon_c = $('#lon_c').val();
	var formData = new FormData($('#formVerifikasi')[0]);
	console.log(id_kpm);
	if(id_kpm === ''){
		iziToast.error({
			title: 'ERROR',
			message: 'Kpm harus dipilih terlebih dahulu',
			position: 'topRight'
		});
	}else{
		formData.append('id_kpm',id_kpm);
		formData.append('photo_ktp', $('input[type=file][name="fotoKtp"]')[0].files[0]);
		formData.append('photo_kk', $('input[type=file][name="fotoKk"]')[0].files[0]);
		formData.append('latitude',lat_c);
		formData.append('longitude',lon_c);
		formData.append('keterangan',keterangan);
		$.ajax({
			url:acces_url.saveValidasi,
			type: 'POST',
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			beforeSend:function(){
				ProgressBar('wait');
			},
			success:function(response) {
				ProgressBar('success');
			if(response.state != true){
				iziToast.error({
					title: 'ERROR',
					message: response.msg,
					position: 'topRight'
				});
			}else{
				iziToast.success({
					timeout: 5000,
					title: 'OK',
					message: response.msg,
					position: 'topRight'
				});
					document.getElementById("form_proses").style.display = "none";
					document.getElementById("tableVerifikasi").style.display = "";
					document.getElementById("addBtnVerifikasi").style.display = "";
					tableVerifikasi.ajax.reload();
					map.remove();
			}
				
			}
		})
	}
	

}


onSubmitUpdateVerifikasi = () => {
	let keterangan = $('input[name="RadioKeterangan"]:checked').val();
	let id_kpm = $('#id_kpm').val();
	let id_verifikasi = $('#id_verifikasi').val();
	let lat_c = $('#lat_c').val();
	let lon_c = $('#lon_c').val();
	var formData = new FormData($('#formVerifikasi')[0]);
	formData.append('id_kpm',id_kpm);
	formData.append('id_verifikasi',id_verifikasi);
	formData.append('photo_ktp', $('input[type=file][name="fotoKtp"]')[0].files[0]);
	formData.append('photo_kk', $('input[type=file][name="fotoKk"]')[0].files[0]);
	formData.append('latitude',lat_c);
	formData.append('longitude',lon_c);
	formData.append('keterangan',keterangan);
	$.ajax({
		url:acces_url.SaveProsesEditVerifikasi,
		type: 'POST',
		dataType: 'json',
		data: formData,
		contentType: false,
		processData: false,
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response) {
			
			ProgressBar('success');
		   if(response.state != true){
			iziToast.error({
				timeout: 5000,
				title: 'OK',
				message: 'Berhasil menampilkan Data',
				position: 'topRight'
			});
		   }else{
				iziToast.success({
					timeout: 5000,
					title: 'OK',
					message: 'Berhasil Update Data',
					position: 'topRight'
				});
				document.getElementById("form_proses").style.display = "none";
				document.getElementById("tableVerifikasi").style.display = "";
				document.getElementById("addBtnVerifikasi").style.display = "";
				tableVerifikasi.ajax.reload();
				map.remove();
		   }
			
		}
	})

}



function getDetailVerifikasiById(idVerifikasi){
	let postIdVerifikasi = {'id_verifikasi':idVerifikasi};

	$.ajax({
		url:acces_url.GetVerifikasiById,
		type:'POST',
		data: postIdVerifikasi,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){
			
			ProgressBar('success');
			if(response.state){
				AddVerifikasi('edit',response.data.latitude,response.data.longitude)
				$('#nama_kpm').val(response.data.nama);
				$('#id_kpm').val(response.data.id_kpm);
				$('#lat_c').val(response.data.latitude);
				$('#lon_c').val(response.data.longitude);
				$('#rd_ket').find(':radio[name=RadioKeterangan][value="'+response.data.hasil+'"]').prop('checked', true);
				let htmFotoKtp = '<img class="img-thumbnail" src="'+SITE_URL+'uploads/kk_ktp/'+response.data.foto_ktp+'">';
				
				let htmFotoRumah = '<img class="img-thumbnail" src="'+SITE_URL+'uploads/kk_ktp/'+response.data.foto_rumah+'">';
				$('#fotoRumah').empty().append(htmFotoRumah);
				$('#DivFotoKtp').css('display','');
				$('#DivFotoRumah').css('display','');
				$('#btn_insert').css('display','none');
				$('#btn_update').css('display','');
				$('#id_verifikasi').val(idVerifikasi);
				$('#fotKtp').empty().append(htmFotoKtp);
				iziToast.success({
					timeout: 5000,
					title: 'OK',
					message: 'Berhasil menampilkan Data',
					position: 'topRight'
				});
				
				
			}else{
				iziToast.error({
					timeout: 5000,
					title: 'Error',
					message: 'Data tidak ditemukan',
					position: 'topRight'
				});
				
			}
		}
	})

}

function searchdata(){
	tableKpm.ajax.reload();
}


GetKecamatan = () =>
{
	$.ajax({
		url: acces_url.GetDataKecamatan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{
			ProgressBar('wait');
		},
		success:(response)=>{
			ProgressBar('success');
			let countData = response.data_kec.length;
			let html = '';
			kode_kec = response.kode_wilayah.kode_wilayah;
			if(countData > 0){		
				html += '<option value="0"> Pilih Salah Satu </option>';
				let subStrKodeWilayah;
				if(response.kode_wilayah.kode_wilayah != null){
					subStrKodeWilayah = response.kode_wilayah.kode_wilayah.substr(0,7);	
				}
					
				$.each(response.data_kec,(i,v) => {						
					if(v.kode_kec == subStrKodeWilayah){ 	
						html += "<option value='" + v.kode_kec + "' selected >" + v.name + "</option>";	
					}else {
						html += "<option value='" + v.kode_kec + "'>" + v.name + "</option>";
					}				
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='kecamatan']").empty().append(html);
		}
	})	
}

GetUserKodeWilayah = () =>
{
	$.ajax({
		url: acces_url.GetCheckUserKodeWilayah,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{
			
		},
		success:(response)=>{
			let retKodeWilayah;
			if(response.data != null){
				retKodeWilayah = response.data;
			}else{
				retKodeWilayah = 0;
			}
			$('#kode_wilayah').val(retKodeWilayah);
		}
	})	
}

GetBantuan = () =>
{
	$.ajax({
		url: acces_url.GetBantuan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{

		},
		success:(response)=>{
			let countData = response.Data.length;

			let html = '';
			if(countData > 0){				
				html += '<option value="0"> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.id_bantuan + "' >" + v.nama_bantuan + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='jenisBantuans']").empty().append(html);
		}
	})	
}

function getDesa(){
	let kode_wilayah = kode_kec;
	let lengthKodeWilayah
	if(kode_wilayah != null){
		lengthKodeWilayah = kode_wilayah.length;
	}
	html ="";
	if(kode_wilayah == 0){
		html += '<option value="0"> Pilih Desa </option>';
	}else{
		
		if(kode_wilayah != null){
			
			var resKodeKec = kode_wilayah.substr(0, 7);
		}else{
			var resKodeKec = 0;
		}
		
		$.ajax({
			url:acces_url.GetDesaByCodeKec,
			type: "POST",
			dataType : "json",
			data: {"kode_kec":resKodeKec},
			success: (response) => {
				let countData = response.Data.length;
				let html = '';
				if(countData > 0){				
					html += '<option value="0"> Pilih Salah Satu </option>';
					$.each(response.Data,(i,v) => {			
						if(lengthKodeWilayah > 7){
							
							if(v.kode_desa == kode_wilayah ){
								html += "<option value='" + v.kode_desa + "' selected >" + v.name + "</option>";
							}else{
								html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
							}
						}else{
							html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
						}		
						
					});
	
				}else{
					html += '<option value="0"> Data tidak ada </option>';
				}
				$("select[name='desa']").empty();
				$("select[name='desa']").append(html);
			}
		})
	}
}





