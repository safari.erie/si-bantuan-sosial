const acces_url = {
	SaveForgotPassword : SITE_URL + 'ForgotPassword/saveForgotPassword',
	SaveResetPassword : SITE_URL + 'ForgotPassword/SaveResetPassword',
}


function onSubmitForgotPassword(){
	var TypeInput = true;
	
	$('[data-input="wajib"]').each(function (i, v) {
		
		if ($(this).val() == "" || $(this).val() === 'undefined') {
			TypeInput = false;
			$('#formForgotPasswor').addClass('was-validated');
			return i < 0;
		}
	});

	if(TypeInput){
		let user_email = $('#email').val();
		var formData = new FormData($('#formForgotPassword')[0]);
		formData.append('email',user_email);

		$.ajax({
			url:acces_url.SaveForgotPassword,
			type: 'POST',
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			beforeSend:function(){
				ProgressBar('wait');
			},
			success:function(response) {
				console.log(response);
				ProgressBar('success');
				if(response.state != true){
					iziToast.error({
						title: 'ERROR',
						message: response.msg,
						position: 'topRight'
					});
				}else{
					iziToast.success({
						timeout: 5000,
						title: 'OK',
						message: response.msg,
						position: 'topRight'
					});
						
				}
					
			}
				
		});
	}

}


function onSubmitResetPassword(){
	var TypeInput = true;
	
	$('[data-input="wajib"]').each(function (i, v) {
		
		if ($(this).val() == "" || $(this).val() === 'undefined') {
			TypeInput = false;
			$('#formResetPassword').addClass('was-validated');
			return i < 0;
		}
	});

	if(TypeInput){

		
		let password = $('#password').val();
		let ConfirmationPassword = $('#password-confirm').val();
		let KeyCode = $('#key_code').val();
		if(password != ConfirmationPassword){
			iziToast.error({
				title: 'ERROR',
				message: 'Password dan Konfirmasi Password tidak sesuai',
				position: 'topRight'
			});
		}else{
			var formData = new FormData($('#formResetPassword')[0]);
			formData.append('password',ConfirmationPassword);
			formData.append('key_code',KeyCode);
			$.ajax({
				url:acces_url.SaveResetPassword,
				type: 'POST',
				dataType: 'json',
				data: formData,
				contentType: false,
				processData: false,
				beforeSend:function(){
					ProgressBar('wait');
				},
				success:function(response) {
					console.log(response);
					ProgressBar('success');
					if(response.state != true){
						iziToast.error({
							title: 'ERROR',
							message: response.msg,
							position: 'topRight'
						});

					}else{
						iziToast.success({
							timeout: 5000,
							title: 'OK',
							message: response.msg,
							position: 'topRight'
						});

						setTimeout(function(){ 

							window.location = SITE_URL+'Login';
						 }, 3000);
							
					}
						
				}
					
			});
		}
		
	}

}
