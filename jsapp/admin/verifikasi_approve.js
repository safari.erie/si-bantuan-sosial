

const acces_url = {
	GetDataKecamatan : SITE_URL+"MgtKpm/getKecamatan",
	GetDesaByCodeKec : SITE_URL+"MgtKpm/getDesa",
	GetVerifikasiById : SITE_URL+"VerifikasiApprove/GetVerifikasiById",
	SaveProsesVerifikasi : SITE_URL+"VerifikasiApprove/SaveProsesVerifikasi",
}
var tableVerifikasiApprove;
$(document).ready(function(){

	GetKecamatan();
	setMaps('','');
	tableVerifikasiApprove = $("#tableVerifikasiApproveContent").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,10,25, 50, 100, 200, 'All']],
		"paging": true,
		"destroy": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "VerifikasiApprove/get",
			"type": "POST",
			"datatype": "json",
			"data": function (d) {
				d.kode_kec   = $('#kecamatans').val();//3201161
				d.kode_desa  = $('#desas').val();
				d.nik 	 = $('#nik').val();
            }
		},	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "nama", "name": "nama", orderable: true,className: 'text-center' },
			{ "data": "nik", "name": "nik", orderable: true ,className: 'text-center'},
			{ "data": "alamat", "name": "alamat", orderable: true,className: 'text-center' },
			{ "data": "kecamatan", "name": "kecamatan", orderable: false },
			{ "data": "desa", "name": "desa", orderable: false },
			{ "data": "nama_bantuan", "name": "nama_bantuan", orderable: false },			
			{ "data": "status_verifikasi", "name": "status_verifikasi", orderable: false },
			{ "data": "foto_ktp", "name": "foto_ktp", orderable: false },
			{ "data": "foto_rumah", "name": "foto_rumah", orderable: false },
			{ "data": "created", "name": "created", orderable: false },
			{ "data": "aksi", "name": "aksi", orderable: false },
		]
	   
	});
});

function searchdata(){
	tableVerifikasiApprove.ajax.reload();
}

let onCancel = () =>{
	document.getElementById("ListData").style.display = '';
	document.getElementById("formInput").style.display = 'none';
}

GetKecamatan = () =>
{
	$.ajax({
		url: acces_url.GetDataKecamatan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{

		},
		success:(response)=>{
			let countData = response.Data.length;

			let html = '';
			if(countData > 0){		
				html += '<option value="0"> Pilih Salah Satu </option>';		
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_kec + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='kecamatan']").empty().append(html);
		}
	})	
}



$("#kecamatans").on('change',function(){
	let kdKec = this.value;
	$.ajax({
		url:acces_url.GetDesaByCodeKec,
		type: "POST",
		dataType : "json",
		data: {"kode_kec":kdKec},
		beforeSend: () => {

		},
		success: (response) => {
			let countData = response.Data.length;
			let html = '';
			if(countData > 0){				
				html += '<option value="0"> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='desa']").empty();
			$("select[name='desa']").append(html);
		}
	})

});

var map;
var marker;
function setMaps(lat,lon){
	
	map = L.map('map', {
        fullscreenControl: {
            pseudoFullscreen: false
        },
        center: [-6.595038, 106.816635],
       // center: [-6.533918021503638, 106.80722936714096],
        zoom: 9
    });

    var basemaps = [
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
        }),
        L.tileLayer("//stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png", {
            attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
            subdomains: "abcd",
            maxZoom: 20,
            minZoom: 0,
            label: "Toner Lite"
        }),
        L.tileLayer("//stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png", {
            attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
            subdomains: "abcd",
            maxZoom: 20,
            minZoom: 0,
            label: "Toner"
        })
    ];

    map.addControl(
        L.control.basemaps({
            basemaps: basemaps,
            tileX: 0,
            tileY: 0,
            tileZ: 1
        })
	);
	
	if(lat != '' && lon != ''){
		marker = L.marker([lat, lon]).addTo(map);
	}
}
function getDetailVerifikasiById(idVerifikasi){
	let postIdVerifikasi = {'id_verifikasi':idVerifikasi};

	$.ajax({
		url:acces_url.GetVerifikasiById,
		type:'POST',
		data: postIdVerifikasi,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){
			ProgressBar('success');
			if(response.state){
				iziToast.success({
					timeout: 5000,
					title: 'OK',
					message: response.msg,
					position: 'topRight'
				});
				document.getElementById("ListData").style.display = 'none';
				document.getElementById("formInput").style.display = '';
				
				$('#valNik').empty().append(response.data.nik);
				$('#valNama').empty().append(response.data.nama);
				$('#valAlamat').empty().append(response.data.alamat);
				$('#valKecamatan').empty().append(response.data.nama_kec);
				$('#valDesa').empty().append(response.data.nama_desa);
				$('#valBantuan').empty().append(response.data.nama_bantuan);
				let htmFotoKtp = '<img class="img-thumbnail" src="'+SITE_URL+'/uploads/kk_ktp/'+response.data.foto_ktp+'">';
				let htmFotoRumah = '<img class="img-thumbnail" src="'+SITE_URL+'/uploads/kk_ktp/'+response.data.foto_rumah+'">';
				$("#prosesVerifikasi").attr("onClick", "openModalVerifikasi("+response.data.id_verifikasi+','+response.data.id_kpm+");");
				$('#fotKtp').empty().append(htmFotoKtp);
				$('#fotoRumah').empty().append(htmFotoRumah);

				map.remove();
				setMaps(response.data.latitude,response.data.longitude);

				
			}else{
				iziToast.error({
					title: 'ERROR',
					message: response.msg,
					position: 'topRight'
				});
			}
		}
	})

}


function openModalVerifikasi(idVerifikasi,id_kpm){
	let persetujuan = $('#persetujuan').val();
	if(persetujuan == 0){
		
		iziToast.error({
			title: 'ERROR',
			message: 'silahkan pilih status persetujuan verifikasi',
			position: 'topRight'
		});
	}else{
		$('#modal-verifikasi').modal({ backdrop: 'static' });
		$('#id_verifikasi').val(idVerifikasi);
		$('#id_kpm_verifikasi').val(id_kpm);
	}

	

}

function SaveProsesVerifikasiBansos(){
	let persetujuan = $('#persetujuan').val();
	let id_verifikasi = $('#id_verifikasi').val();
	let id_kpm_verifikasi = $('#id_kpm_verifikasi').val();
	let postSaveVerifikasi = {
		'id_verifikasi_inp':id_verifikasi,
		'persetujuan':persetujuan,
		'id_kpm_verifikasi':id_kpm_verifikasi,}

	$.ajax({
		url:acces_url.SaveProsesVerifikasi,
		type:'POST',
		data: postSaveVerifikasi,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){
			ProgressBar('success');
			if(response.state){
				$('#modal-verifikasi').modal('hide');
				iziToast.success({
					timeout: 5000,
					title: 'OK',
					message: response.msg,
					position: 'topRight'
				});
				onCancel();
				searchdata();
				
			}else{
				iziToast.error({
					title: 'ERROR',
					message: response.msg,
					position: 'topRight'
				});
			}
			
		}
	})
}
