
let accessController = {
	GetRoles : SITE_URL+'User/getRoles',
	GetKecamatans : SITE_URL+'Wilayah/getKecamatan',
	GetDesas : SITE_URL+'Wilayah/getDesa',
	SaveUser : SITE_URL+'User/SaveUser',
	Deaktivasi : SITE_URL+'User/Deaktivasi',
	Aktivasi : SITE_URL+'User/Aktivasi',
	GetUserByIds : SITE_URL+'User/getUserById',
	SaveUserEdit : SITE_URL+'User/SaveUserEdit',
}

var oTable;
$(document).ready(function(){
	
	oTable = $("#TabelManajemenUser").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "user/get",
			"type": "POST",
			"datatype": "json",
			"data": function (d) {      				
				d.nama_pengguna  = $('#nama_pengguna').val();
            }
		},
	
		"columns": [		
			{ "data": "no", "name": "no", orderable: false },
			{ "data": "username", "name": "username", orderable: false },
			{ "data": "email", "name": "email", orderable: false },
			{ "data": "fullName", "name": "fullName", orderable: false },
			{ "data": "role_name", "name": "role_name", orderable: false },
			{ "data": "status", "name": "status", orderable: false },
			{ "data": "aksi", "name": "aksi", orderable: false },
		]
	   
	});

});

function searchdata(){
	
	oTable.ajax.reload();
}

let AddUser = () => {
    $('#FormUser')[0].reset();
    $("#TitleFormUser").html("Tambah Pengguna")
    $('#ModalFormUser').modal("show");
	GetRoles(null);
	$('#password').prop('required',true);
	$('#password').attr('data-input',"wajib");
    $("#BtnSaveUser").attr("onClick", "SaveUser('Tambah');");
    $("#DivPassword", "#FormUser").show();

    $("select[name='Roles']").removeAttr("disabled");
    $("input[name='Username']").removeAttr("readonly");
    $("input[name='IdUser']", "#FormUser").val(0);
}

let GetRoles = idRole => {

	$.ajax({
		url:accessController.GetRoles,
		type: "GET",
		dataType: "json",
		beforeSend:function(){
			$("body").css("cursor", "progress");
			$("#loading").removeAttr("style");
		},
		success:function(response) {
			$("body").css("cursor", "default");
			$("#loading").css("display", "none");
			if(response.Data.length > 0){
				let html = '';
				html += '<option value=-1> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {
					if(idRole == v.id_role){
						html += `<option value='${v.id_role}' selected> ${v.role_name} </option>`;
					}else{
						html += `<option value='${v.id_role}' > ${v.role_name} </option>`;
					}

					
				});
				$("select[name='roles']").empty();
					$("select[name='roles']").append(html);
			}else{
				html += '<option value=-1> Data Tidak ada </option>';
			}
			
		}
	})

}

$('#roles').on('change',function(){
	let idRoles = this.value;
	if(idRoles === '3'){ // petugaskecamatan
		document.getElementById("Divkecamatan").style.display = "";
		document.getElementById("Divdesa").style.display = "none";
		GetKecamatan();
	}else if(idRoles === '4'){
		document.getElementById("Divkecamatan").style.display = "";
		document.getElementById("Divdesa").style.display = "";
		GetKecamatan();
		$('#kecamatan').on('change',function(){
			GetDesas(this.value,'');
		})
	}else{
		document.getElementById("Divkecamatan").style.display = "none";
		document.getElementById("Divdesa").style.display = "none";
	}
});

function Deaktivasi(idUser){

	$('#modal-deaktivasi').modal({ backdrop: 'static' });
	$('.modal-title').empty().append('Konfirmasi');
	$('#text_confirmation_deaktivasi').empty().append('Apakah Yakin User pengguna akan di non aktifkan');
	$('#btn_deaktivasi').attr('onclick', 'on_deaktivasi(' + idUser + ')');

}

function Aktivasi(idUser){
	$('#modal-aktivasi').modal({ backdrop: 'static' });
	$('.modal-title').empty().append('Konfirmasi');
	$('#text_confirmation_aktivasi').empty().append('Apakah Yakin User pengguna akan diaktifkan kembali');
	$('#btn_aktivasi').attr('onclick', 'on_aktivasi(' + idUser + ')');
}

function EditPengguna(idUsers){
	let postIds = {'id_user':idUsers};
	$.ajax({
		url:accessController.GetUserByIds,
		type:'POST',
		data :postIds,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){
			ProgressBar('success');
			if(response.state){
				
				iziToast.success({
					timeout: 5000,
					title: 'OK',
					message: response.msg,
					position: 'topRight'
				});
				$('#ModalFormUser').modal({ backdrop: 'static' });
				$('#TitleFormUser').empty().html('Update User');
				GetRoles(response.data.id_role);
				$('#username').val(response.data.username);
				$('#DivPassword').hide();
				$('#password').prop('required',false);
				$('#password').removeAttr('data-input',"wajib");
				$('#email').val(response.data.email);
				$('#firstName').val(response.data.first_name);
				$('#middleName').val(response.data.middle_name);
				$('#lastName').val(response.data.last_name);
				$('#address').val(response.data.address);
				$('#phoneNumber').val(response.data.phone_number);
				$('#mobileNumber').val(response.data.mobile_number);
				
				if(response.data.kode_wilayah == null || response.data.kode_wilayah == 0){
					$('#Divkecamatan').hide();
					$('#Divdesa').hide();
				}else{
					let get_kode_wilayah = response.data.kode_wilayah;
					let kode_wilayah_length = get_kode_wilayah.length;
					
					if(kode_wilayah_length == 7){
						$('#Divkecamatan').show();
						GetKecamatan(get_kode_wilayah);
					}else if(kode_wilayah_length == 10){
						$('#Divkecamatan').show();
						let kode_wilayah_substr = get_kode_wilayah.substring(0,7);
						GetKecamatan(kode_wilayah_substr);
						GetDesas(kode_wilayah_substr,get_kode_wilayah);
						$('#Divdesa').show();

					}
					
				
				}
				$('#BtnSaveUser').attr('onclick', 'on_edit_user(' + response.data.id_user + ')');
			}else{
				iziToast.error({
					title: 'ERROR',
					message: response.msg,
					position: 'topRight'
				});
			}
			
		}
	});
	
}

function on_edit_user(idUser){

	var TypeInput = true;
	
	$('[data-input="wajib"]').each(function (i, v) {
		
		if ($(this).val() == "") {
			TypeInput = false;
			$('#FormUser').addClass('was-validated');
			return i < 0;
		}
	});
	
		if(TypeInput){
			let roles = $('#roles').val();
			let username = $('#username').val();
			let email = $('#email').val();
			let firstName = $('#firstName').val();
			let middleName = $('#middleName').val();
			let lastName = $('#lastName').val();
			let address = $('#address').val();
			let phoneNumber = $('#phoneNumber').val();
			let mobileNumber = $('#mobileNumber').val();
			let kecamatan = $('#kecamatan').val();
			let desa = $('#desa').val();
			if(roles == '-1'){
				iziToast.error({
					title: 'ERROR',
					message: 'Role Harus dipilih',
					position: 'topRight'
				});
			}else if(roles === 3 && kecamatan == '' || kecamatan === '-1'){
				iziToast.error({
					title: 'ERROR',
					message: 'Kecamatan Harus dipilih',
					position: 'topRight'
				});
			}else if(roles === 4 && kecamatan == '' && desa == '' || desa === '-1'){
				iziToast.error({
					title: 'ERROR',
					message: 'Wilayah Desa Harus dipilih',
					position: 'topRight'
				});
			}
		
		else{
			let param = '';
			if(roles === '3'){
				param = {
						"username":username,
						"email":email,
						"firstName":firstName,
						"middleName":middleName,
						"lastName":lastName,
						"address":address,
						"phoneNumber":phoneNumber,
						"mobileNumber":mobileNumber,
						"kodeWilayah":kecamatan,
						"roles"	: roles,
						"idUser": idUser,
					};
			}else if(roles === '4'){
				param = {
					"username":username,
					"email":email,
					"firstName":firstName,
					"middleName":middleName,
					"lastName":lastName,
					"address":address,
					"phoneNumber":phoneNumber,
					"mobileNumber":mobileNumber,
					"kodeWilayah":desa,
					"roles"	: roles,
					"idUser": idUser,
				};
			}else{
				param = {
					"username":username,
					"email":email,
					"firstName":firstName,
					"middleName":middleName,
					"lastName":lastName,
					"address":address,
					"phoneNumber":phoneNumber,
					"mobileNumber":mobileNumber,
					"kodeWilayah":0,
					"roles"	: roles,
					"idUser": idUser,
				};
			}
			
			$.ajax({
				url:accessController.SaveUserEdit,
				type: "POST",
				dataType: "json",
				data: param,
				beforeSend:function(){
					ProgressBar('wait');
				},
				success:function(response) {
					ProgressBar('success');
				if(response.state == -1){
					iziToast.error({
						title: 'ERROR',
						message: response.msg,
						position: 'topRight'
					});
				}else{
						iziToast.success({
							timeout: 5000,
							title: 'OK',
							message: response.msg,
							position: 'topRight'
						});
						$('#ModalFormUser').modal("hide");
						oTable.ajax.reload();
				}
					
				}
			})
		}
	}

}

function on_deaktivasi(idUser){
	let postId = {'id_user':idUser};
	$.ajax({
		url:accessController.Deaktivasi,
		type:'POST',
		data :postId,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){
			ProgressBar('success');
			if(response.state){
				iziToast.success({
					timeout: 5000,
					title: 'OK',
					message: response.msg,
					position: 'topRight'
				});
				$('#modal-deaktivasi').modal('hide');
				
			}else{
				iziToast.error({
					title: 'ERROR',
					message: response.msg,
					position: 'topRight'
				});
				$('#modal-deaktivasi').modal('hide');
				
			}
			searchdata();
		}

	});
}

function on_aktivasi(idUser){
	let postId = {'id_user':idUser};
	$.ajax({
		url:accessController.Aktivasi,
		type:'POST',
		data :postId,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){
			ProgressBar('success');
			if(response.state){
				iziToast.success({
					timeout: 5000,
					title: 'OK',
					message: response.msg,
					position: 'topRight'
				});
				$('#modal-aktivasi').modal('hide');
				
			}else{
				iziToast.error({
					title: 'ERROR',
					message: response.msg,
					position: 'topRight'
				});
				$('#modal-aktivasi').modal('hide');
				
			}
			searchdata();
		}

	});
}




let GetKecamatan = (kodeKec) => {
	$.ajax({
		url:accessController.GetKecamatans,
		type: "GET",
		dataType: "json",
		beforeSend:function(){
			$("body").css("cursor", "progress");
			$("#loading").removeAttr("style");
		},
		success:function(response) {
			$("body").css("cursor", "default");
			$("#loading").css("display", "none");
			if(response.Data.length > 0){
				let html = '';
				html += '<option value=-1> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {
					if(kodeKec == v.kode_kec){
						html += `<option value='${v.kode_kec}' selected> ${v.name} </option>`;
					}else{
						html += `<option value='${v.kode_kec}' > ${v.name} </option>`;
					}

					
				});
				$("select[name='kecamatan']").empty();
					$("select[name='kecamatan']").append(html);
			}else{
				html += '<option value=-1> Data Tidak ada </option>';
			}
			
		}
	})
}

let GetDesas = (kodeKec,kodeDesa) =>{
	$.ajax({
		url:accessController.GetDesas,
		type: "POST",
		dataType: "json",
		data: {"kode_kec":kodeKec},
		beforeSend:function(){
			$("body").css("cursor", "progress");
			$("#loading").removeAttr("style");
		},
		success:function(response) {
			$("body").css("cursor", "default");
			$("#loading").css("display", "none");
			if(response.Data.length > 0){
				let html = '';
				html += '<option value=-1> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {
					if(kodeKec == v.kode_kec){
						html += `<option value='${v.kode_desa}' selected> ${v.name} </option>`;
					}else{
						html += `<option value='${v.kode_desa}' > ${v.name} </option>`;
					}
					
				});
				$("select[name='desa']").empty();
					$("select[name='desa']").append(html);
			}else{
				html += '<option value=-1> Data Tidak ada </option>';
			}
			
		}
	})
}

function SaveUser(str){

	var TypeInput = true;
	
	$('[data-input="wajib"]').each(function (i, v) {
		
		if ($(this).val() == "") {
			TypeInput = false;
			$('#FormUser').addClass('was-validated');
			return i < 0;
		}
	});

	if(TypeInput){
		let roles = $('#roles').val();
		let username = $('#username').val();
		let password = $('#password').val();
		let email = $('#email').val();
		let firstName = $('#firstName').val();
		let middleName = $('#middleName').val();
		let lastName = $('#lastName').val();
		let address = $('#address').val();
		let phoneNumber = $('#phoneNumber').val();
		let mobileNumber = $('#mobileNumber').val();
		let kecamatan = $('#kecamatan').val();
		let desa = $('#desa').val();
		if(roles == '-1'){
			iziToast.error({
				title: 'ERROR',
				message: 'Role Harus dipilih',
				position: 'topRight'
			});
		}else if(roles === 3 && kecamatan == '' || kecamatan === '-1'){
			iziToast.error({
				title: 'ERROR',
				message: 'Kecamatan Harus dipilih',
				position: 'topRight'
			});
		}else if(roles === 4 && kecamatan == '' && desa == '' || desa === '-1'){
			iziToast.error({
				title: 'ERROR',
				message: 'Wilayah Desa Harus dipilih',
				position: 'topRight'
			});
		}
	
	else{
		let param = '';
		if(roles === '3'){
			param = {
					"username":username,
					"password":password,
					"email":email,
					"firstName":firstName,
					"middleName":middleName,
					"lastName":lastName,
					"address":address,
					"phoneNumber":phoneNumber,
					"mobileNumber":mobileNumber,
					"kodeWilayah":kecamatan,
					"roles"	: roles
				};
		}else if(roles === '4'){
			param = {
				"username":username,
				"password":password,
				"email":email,
				"firstName":firstName,
				"middleName":middleName,
				"lastName":lastName,
				"address":address,
				"phoneNumber":phoneNumber,
				"mobileNumber":mobileNumber,
				"kodeWilayah":desa,
				"roles"	: roles
			};
		}else{
			param = {
				"username":username,
				"password":password,
				"email":email,
				"firstName":firstName,
				"middleName":middleName,
				"lastName":lastName,
				"address":address,
				"phoneNumber":phoneNumber,
				"mobileNumber":mobileNumber,
				"kodeWilayah":0,
				"roles"	: roles
			};
		}

		$.ajax({
			url:accessController.SaveUser,
			type: "POST",
			dataType: "json",
			data: param,
			beforeSend:function(){
				ProgressBar('wait');
			},
			success:function(response) {
				ProgressBar('success');
			if(response.state == -1){
				iziToast.error({
					title: 'ERROR',
					message: response.msg,
					position: 'topRight'
				});
					//alert(response.msg);
			}else{
					//alert(response.msg);
					iziToast.success({
						timeout: 5000,
						title: 'OK',
						message: response.msg,
						position: 'topRight'
					});
					$('#ModalFormUser').modal("hide");
					oTable.ajax.reload();
			}
				
			}
		})
	}
	}



	/* let roles = $('#roles').val();
	 */


}


