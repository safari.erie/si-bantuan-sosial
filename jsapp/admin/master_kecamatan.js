

var oTable;

const acces_url = {
	GetDataKecamatan : SITE_URL+"MasterKecamatan/get",
	SaveKecamatan : SITE_URL+"MasterKecamatan/SaveKecamatan",
	GetDataKecByKode : SITE_URL+"MasterKecamatan/GetDataKecByKode",
}
$(document).ready(function(){
	
	oTable = $("#tableMasterKecamatanContent").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,10,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": acces_url.GetDataKecamatan,
			"type": "POST",
			"datatype": "json",
			/* "data": function (d) {      				


				d.kode_kec   = $('#kecamatans').val();//3201161
				d.kode_desa  = $('#desas').val();
				d.nik 	 = $('#nik').val();
            } */
		},
	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "nama", "name": "nama", orderable: true,className: 'text-center' },
			{ "data": "aksi", "name": "aksi", orderable: false },
		]
	   
	});
});


function AddMasterKecamatan  () {
	document.getElementById("ListData").style.display = 'none';
	document.getElementById("formInput").style.display = '';
}

function onSubmitkecamatan(){
	let id_kecamatan = $('#id_kecamatan').val();
	let kode_kec = $('#kode_kecamatan').val();
	let nama_kecamatan = $('#nama_kecamatan').val();

	if(kode_kec.length < 7){
		iziToast.error({
			title: 'ERROR',
			message: 'Kode Kecamatan kurang 7 digit',
			position: 'topRight'
		});
	}else{
		var formData = new FormData($('#formKecamatan')[0]);
		formData.append('id_kecamatan',id_kecamatan);
		formData.append('kode_kec',kode_kec);
		formData.append('name',nama_kecamatan);

		$.ajax({
			url:acces_url.SaveKecamatan,
			type: 'POST',
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			beforeSend:function(){
				ProgressBar('wait');
			},
			success:function(response) {
				ProgressBar('success');
			   if(response.state != true){
					alert(response.msg);
			   }else{
					alert(response.msg);
					document.getElementById("ListData").style.display = '';
					document.getElementById("formInput").style.display = 'none';
					searchdata();
					clear();
			   }
			}
		})
	}
}

function searchdata(){
	oTable.ajax.reload();
}

function clear(){
	$('#kode_kecamatan').val('');
	$('#nama_kecamatan').val('');

}

function onCancel(){
	document.getElementById("ListData").style.display = '';
	document.getElementById("formInput").style.display = 'none';
}

function EditKec(kodeKec){

	let postKode = {'kodeKec':kodeKec}
	$.ajax({
		url:acces_url.GetDataKecByKode,
		type:'POST',
		data :postKode,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){	
			ProgressBar('success');
			if(response){
				$('#id_kecamatan').val(response.data.kode_kec);
				$('#kode_kecamatan').val(response.data.kode_kec);
				$('#nama_kecamatan').val(response.data.name);
				document.getElementById("ListData").style.display = 'none';
				document.getElementById("formInput").style.display = '';
				$('#kode_kecamatan').attr('disabled','disabled');
			}else{
				
				iziToast.error({
					title: 'ERROR',
					message: response.data.msg,
					position: 'topRight'
				});
			}	
		}

	})
}
