

var oTable;

const acces_url = {
	GetDataDesa : SITE_URL+"MasterDesa/get",
	GetDataKecamatan : SITE_URL+"MgtKpm/getKecamatan",
	SaveDesa : SITE_URL+"MasterDesa/SaveDesa",
}
$(document).ready(function(){
	
	oTable = $("#tableMasterDesaContent").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,10,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": acces_url.GetDataDesa,
			"type": "POST",
			"datatype": "json",			
		},
	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "kodeKec", "name": "kodeKec", orderable: true,className: 'text-center' },
			{ "data": "namaKec", "name": "namaKec", orderable: true,className: 'text-center' },
			{ "data": "kodeDesa", "kodeDesa": "nama", orderable: true,className: 'text-center' },
			{ "data": "namaDesa", "namaDesa": "nama", orderable: true,className: 'text-center' },
			{ "data": "aksi", "name": "aksi", orderable: false },
		]
	   
	});
});

function searchdata(){
	oTable.ajax.reload();
}

function clear(){
	$('#kecamatan').val(0);
	$('#kode_desa').val('');
	$('#nama_desa').val('');

}

function onCancel(){
	document.getElementById("ListData").style.display = '';
	document.getElementById("formInput").style.display = 'none';
}


GetKecamatan = () =>
{
	$.ajax({
		url: acces_url.GetDataKecamatan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{
			ProgressBar('wait');
		},
		success:(response)=>{
			ProgressBar('success');
			let countData = response.Data.length;

			let html = '';
			if(countData > 0){		
				html += '<option value="0"> Pilih Salah Satu </option>';		
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_kec + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='kecamatan']").empty().append(html);
		}
	})	
}


function AddMasterDesa  () {
	document.getElementById("ListData").style.display = 'none';
	document.getElementById("formInput").style.display = '';
	GetKecamatan();
}

function onSubmitdesa(){
	let kode_kec = $('#kecamatan').val();
	let kode_desa = $('#kode_desa').val();
	let nama_desa = $('#nama_desa').val();
	let id_desa = $('#id_desa').val();
	if(kode_kec == 0){
		
		iziToast.error({
			title: 'ERROR',
			message: 'Kecamatan Belum dipilih',
			position: 'topRight'
		});
	}else if(kode_desa.length < 10){
		
		iziToast.error({
			title: 'ERROR',
			message: 'Kode Desa kurang dari 10 digit',
			position: 'topRight'
		});
	}else {
		var formData = new FormData($('#formdesa')[0]);
		formData.append('id_desa',id_desa);
		formData.append('kode_kec',kode_kec);
		formData.append('kode_desa',kode_desa);
		formData.append('nama_desa',nama_desa);

		$.ajax({
			url:acces_url.SaveDesa,
			type: 'POST',
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			beforeSend:function(){
				ProgressBar('wait');
			},
			success:function(response) {
				ProgressBar('success');
			   if(response.state != true){
					
					iziToast.error({
						title: 'ERROR',
						message: response.msg,
						position: 'topRight'
					});
			   }else{
					
					iziToast.success({
						timeout: 5000,
						title: 'OK',
						message: response.msg,
						position: 'topRight'
					});
					document.getElementById("ListData").style.display = '';
					document.getElementById("formInput").style.display = 'none';
					searchdata();
					clear();
			   }
			}
		})


	}
}
