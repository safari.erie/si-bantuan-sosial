var oTable;

const acces_url = {
	GetDataBerita : SITE_URL+"MgtNews/get",
	SaveBerita : SITE_URL+"MgtNews/SaveBerita",
	GetBeritaById : SITE_URL+"MgtNews/getBeritaById",
	NewsDelete : SITE_URL+"MgtNews/NewsDelete",
}
$(document).ready(function(){
	
	oTable = $("#tableBerita").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,10,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": acces_url.GetDataBerita,
			"type": "POST",
			"datatype": "json",
			"data": function (d) {  
				d.judul_berita 	 = $('#judul_berita').val();
            }
		},
	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "judul_berita", "name": "judul_berita", orderable: true,className: 'text-center' },
			{ "data": "isi", "name": "isi", orderable: true,className: 'text-center' },
			{ "data": "gambar", "name": "gambar", orderable: true,className: 'text-center' },
			{ "data": "created", "name": "created", orderable: true,className: 'text-center' },
			{ "data": "aksi", "name": "aksi", orderable: false },
		]
	   
	});



	
});

function searchdata(){
	oTable.ajax.reload();
}

function AddBerita(){
	$('#foto_berita').prop('required',true);
	$('#foto_berita').attr('data-input',"wajib");
	document.getElementById("ListData").style.display = 'none';
	document.getElementById("formInput").style.display = '';
	$('#id_berita').val(-1);
	$('#judul_berita_inp').val('');
	$('#previewFoto').css('display','none');
	$("#foto_berita").replaceWith($("#foto_berita").val('').clone(true));
	setTimeout((e) => {
		$('.summernote').summernote({
			placeholder: 'Isian Content',
			tabsize: 20,
			height: 300
		});
	}, 1000)
	
	$('.summernote').summernote('destroy');
}

function onSubmitNews(){

	var TypeInput = true;
	
	$('[data-input="wajib"]').each(function (i, v) {
		
		if ($(this).val() == "" || $(this).val() === 'undefined') {
			TypeInput = false;
			$('#formBerita').addClass('was-validated');
			return i < 0;
		}
	});

	if(TypeInput){
		if($('#isi_berita').summernote('isEmpty')) {
			iziToast.error({
				title: 'Gagal',
				message: 'Isi Berita tidak boleh kosong',
				position: 'topRight'
			});
			$('#formBerita').addClass('was-validated');
			$('.invalid-feedbacks').show();
		  }
		  else {
			let text_of_isi_berita = $('#isi_berita').summernote('code');
			let judul_berita = $('#judul_berita_inp').val();
			let id_berita = $('#id_berita').val();
			$('.invalid-feedbacks').hide();
			var formData = new FormData($('#formBerita')[0]);
			formData.append('id_berita',id_berita);
			formData.append('judul_berita',judul_berita);
			formData.append('isi_berita',text_of_isi_berita);
			formData.append('gambar_berita',$('input[type=file][name="foto_berita"]')[0].files[0]);

			$.ajax({
				url:acces_url.SaveBerita,
				type: 'POST',
				dataType: 'json',
				data: formData,
				contentType: false,
				processData: false,
				beforeSend:function(){
					ProgressBar('wait');
				},
				success:function(response) {
					
					ProgressBar('success');
					if(response.state != true){
						iziToast.error({
							title: 'ERROR',
							message: response.msg,
							position: 'topRight'
						});
					}else{
							iziToast.success({
								timeout: 5000,
								title: 'OK',
								message: response.msg,
								position: 'topRight'
							});
							onCancel();
							searchdata();
							
					}
						
				}
					
			});


		  }
	}


}

function checkextension() {
	var file = document.querySelector("#foto_berita");
	if (  /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false ) { 
		iziToast.error({
            title: 'Gagal Upload',
            message: 'File yang diupload harus format photo',
            position: 'topRight'
        });
		$("#foto_berita").replaceWith($("#foto_berita").val('').clone(true));
 
 	}
  }

  function onCancel(){
	document.getElementById("ListData").style.display = '';
	document.getElementById("formInput").style.display = 'none';
  }


  function EditBerita(idBerita){
	let postIdBerita = {'id_berita':idBerita};
	$('#foto_berita').prop('required',false);
	$('#foto_berita').removeAttr('data-input',"wajib");
	$.ajax({
		url:acces_url.GetBeritaById,
		type:'POST',
		data: postIdBerita,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){
			ProgressBar('success');
			if(response.state){
				document.getElementById("ListData").style.display = 'none';
				document.getElementById("formInput").style.display = '';
				
				$('#id_berita').val(response.data.id_berita);
				$('#judul_berita_inp').val(response.data.judul_berita);
				$("input[name='isi_berita']", "#formBerita").summernote('code', response.data.isi_berita);
				if(response.data.gambar != null){ 
					$('#previewFoto').css('display','');
					let htmlGambar = '<img class="img-thumbnail" src="'+SITE_URL+'uploads/berita/'+response.data.gambar+'">';
					$('#foto_berita_preview').empty().append(htmlGambar);
				}else{
					$('#previewFoto').css('display','none');
				}
			}else{
				iziToast.error({
					timeout: 5000,
					title: 'Error',
					message: 'Data tidak ditemukan',
					position: 'topRight'
				});
				
			}
		}
	});
  }


  function HapusBerita(idBerita){
	let postIdBerita = {'id_berita':idBerita};
	
	$('#modal-delete').modal({ backdrop: 'static' });
	$('#text_delete').empty().html('Apakah Yakin Id Berita '+idBerita+' Akan dihapus');
	$('#btn_confirm').attr('onclick', 'on_delete_confirm_news(' + idBerita + ')');
  }

  function on_delete_confirm_news(idBerita){
	let postIdBerita = {'id_berita':idBerita};

	$.ajax({
		url:acces_url.NewsDelete,
		type:'POST',
		data :postIdBerita,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){
			ProgressBar('success');
			if(response.state){
				iziToast.success({
					timeout: 5000,
					title: 'Ok',
					message: response.msg,
					position: 'topRight'
				});
				$('#modal-delete').modal('hide');
				oTable.ajax.reload();

			}else{
				iziToast.error({
					timeout: 5000,
					title: 'Error',
					message: response.msg,
					position: 'topRight'
				});
				$('#modal-delete').modal('hide');
				
			}
			searchdata();
		}

	});
  }

