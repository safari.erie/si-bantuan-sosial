const acces_url = {
	GetDataKpm : SITE_URL+"MgtBansos/getDataKpm",
	saveValidasi : SITE_URL+"verifikasi/saveVerifikasi",
	GetBantuan : SITE_URL+"MgtBansos/getBantuan",
	SaveBansos : SITE_URL+"MgtBansos/SaveBansos",
	GetDataKecamatan : SITE_URL+"MgtKpm/getKecamatan",
	GetDesaByCodeKec : SITE_URL+"MgtKpm/getDesa",
}
var oTable;
var oTableDuplicates;

$(document).ready(function(){

	oTable = $("#datatable").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,10,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "MgtBansos/get",
			"type": "POST",
			"datatype": "json",
			"data": function (d) {
				d.kode_kec   = $('#kecamatans').val();//3201161
				d.kode_desa  = $('#desas').val();
				d.nik 	 = $('#nik').val();
            }
		},
	
		"columns": [		
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "nama", "name": "nama", orderable: true,className: 'text-center' },
			{ "data": "nik", "name": "nik", orderable: true ,className: 'text-center'},
			{ "data": "alamat", "name": "alamat", orderable: true,className: 'text-center' },
			{ "data": "kecamatan", "name": "kecamatan", orderable: false },
			{ "data": "desa", "name": "desa", orderable: false },
			{ "data": "nama_bantuan", "name": "nama_bantuan", orderable: false },
			{ "data": "status", "name": "status", orderable: false },
		]
	   
	});

	oTableDuplicates = $("#data_duplikat").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,10,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "MgtBansos/get_bantuan_duplikat",
			"type": "POST",
			"datatype": "json",
			"data": function (d) {
				d.kode_kec   = $('#kecamatans').val();//3201161
				d.kode_desa  = $('#desas').val();
				d.nik 	 = $('#nik').val();
            }
		},
	
		"columns": [		
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "nama", "name": "nama", orderable: true,className: 'text-center' },
			{ "data": "nik", "name": "nik", orderable: true ,className: 'text-center'},
			{ "data": "alamat", "name": "alamat", orderable: true,className: 'text-center' },
			{ "data": "kecamatan", "name": "kecamatan", orderable: false },
			{ "data": "desa", "name": "desa", orderable: false },
			{ "data": "nama_bantuan", "name": "nama_bantuan", orderable: false },
			{ "data": "status", "name": "status", orderable: false },
		]
	   
	});

	GetKecamatan();
});


GetKecamatan = () =>
{
	$.ajax({
		url: acces_url.GetDataKecamatan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{

		},
		success:(response)=>{
			let countData = response.Data.length;

			let html = '';
			if(countData > 0){		
				html += '<option value="0"> Pilih Salah Satu </option>';		
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_kec + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='kecamatan']").empty().append(html);
		}
	})	
}


function AddBansos(){
	document.getElementById("ListData").style.display = 'none';
	document.getElementById("formInput").style.display = '';
	getBantuan('');
}


function searchdata(){
	oTable.ajax.reload();
	oTableDuplicates.ajax.reload();
   
}


function getBantuan(idBantuan){
	
	$.ajax({
		url: acces_url.GetBantuan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{

		},
		success:(response)=>{
			let countData = response.data.length;
			let html = '';
			if(countData > 0){		
				html += '<option value="0"> Pilih Salah Satu </option>';		
				$.each(response.data,(i,v) => {					
					if(v.id_bantuan == idBantuan){
						html += "<option value='" + v.id_bantuan + "' selected >" + v.nama_bantuan + "</option>";
					}else{
						
						html += "<option value='" + v.id_bantuan + "' >" + v.nama_bantuan + "</option>";
					}
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='idBantuan']").empty().append(html);
		}
	})	
}


$('#addKpmTable').on('click',function(){
	$('#ModalFormKpm').modal("show");

	tableKpm = $("#datatableKpm").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,10,25, 50, 100, 200, 'All']],
		"paging": true,
		"destroy": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url":acces_url.GetDataKpm,
			"type": "POST",
			"datatype": "json",
			/* "data": function (d) {      				


				d.kode_kec   = $('#kecamatans').val();//3201161
				d.kode_desa  = $('#desas').val();
				d.bantuan 	 = $('#jenisBantuans').val();
            } */
		},	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "id_kpm", "name": "id_kpm", orderable: true ,className: 'text-center'},
			{
                "render": function (data, type, full, meta) {
					
                    return '<a href="javascript:void(0)" class="" >' + full.nama + '</a>'
                }
            },
			{ "data": "nik", "name": "nik", orderable: true ,className: 'text-center'},
			{ "data": "alamat", "name": "alamat", orderable: true,className: 'text-center' },
		]
	   
	});


	// event onclick modal op
    $('#datatableKpm tbody').on('click', 'a', function () {
        var data = tableKpm.row($(this).parents('tr')).data();
		$("#ModalFormKpm").modal("hide");
		$('#nama_kpm').val(data.nama);
		$('#id_kpm').val(data.id_kpm);
	});
	
	

});


function onSubmitBantuan(){

	let id_kpm = $('#id_kpm').val();
	let idBantuan = $('#idBantuan').val();
	let id_bansos = $('#id_bansos').val();

	if(id_kpm == '' || id_kpm == null || typeof id_kpm == 'undefined'){
		alert('Data Kpm belum dipilih');
	}else if(idBantuan == 0){	
		alert('Data Bantuan belum dipilih');
	}else{
		var formData = new FormData($('#formBansos')[0]);
		formData.append('id_bansos',id_bansos);
		formData.append('id_kpm',id_kpm);
		formData.append('id_bantuan',idBantuan);

		$.ajax({
			url:acces_url.SaveBansos,
			type: 'POST',
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			beforeSend:function(){
				$("body").css("cursor", "progress");
				$("#loading").removeAttr("style");
			},
			success:function(response) {
				$("body").css("cursor", "default");
			   if(response.state != true){
					alert(response.msg);
			   }else{
					alert(response.msg);
					document.getElementById("ListData").style.display = '';
					document.getElementById("formInput").style.display = 'none';
					searchdata();
			   }
			}
		})


	}

}


$("#kecamatans").on('change',function(){
	let kdKec = this.value;
	$.ajax({
		url:acces_url.GetDesaByCodeKec,
		type: "POST",
		dataType : "json",
		data: {"kode_kec":kdKec},
		beforeSend: () => {

		},
		success: (response) => {
			let countData = response.Data.length;
			let html = '';
			if(countData > 0){				
				html += '<option value="0"> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='desa']").empty();
			$("select[name='desa']").append(html);
		}
	})

});

function downloadData(){
	
	var param;
	var kecamatans = $('#kecamatans').val();
	var desa = $('#desas').val();
	var nik = $('#nik').val();
	var kode_wilayah_login = $('#kode_wilayah_login').val();
	if(kecamatans == 0 ){
		
		iziToast.error({
			title: 'ERROR',
			message: 'Harap Pilih Kecamatan',
			position: 'topRight'
		});
	}else{
		if(kode_wilayah_login != 0){
			
			if(kode_wilayah_login.length == 7){
				if(kode_wilayah_login != kecamatans){
					
					iziToast.error({
						title: 'ERROR',
						message: 'Tidak bisa download data karena bukan wilyah',
						position: 'topRight'
					});
				}else{
					
					if(kecamatans != 0){
						if(desa != 0){
							param = '?kode_kec='+kecamatans+'&kode_desa='+desa;
						}else{
							if(nik != '' || nik != ''){
								param = '?kode_kec='+kecamatans+'&nik='+nik;
							}else{
								param = '?kode_kec='+kecamatans;
							}
						}
					}else {
						if(nik != '' || nik != '' ){
							param = '?nik='+nik;
						}
					} 
					window.location = SITE_URL + 'MgtBansos/DownloadData'+param;

				}
			}else if(kode_wilayah_login.length > 7){
				if(kode_wilayah_login != kdDesa){
					iziToast.error({
						title: 'ERROR',
						message: 'Tidak bisa download data karena bukan wilyah',
						position: 'topRight'
					});
				}else{
					if(kecamatans != 0){
						if(desa != 0){
							param = '?kode_kec='+kecamatans+'&kode_desa='+desa;
						}else{
							if(nik != '' || nik != ''){
								param = '?kode_kec='+kecamatans+'&nik='+nik;
							}else{
								param = '?kode_kec='+kecamatans;
							}
						}
					}else {
						if(nik != '' || nik != '' ){
							param = '?nik='+nik;
						}
					}
					window.location = SITE_URL + 'MgtBansos/DownloadData'+param; 
				}
			}
		}else{
			if(kecamatans != 0){
				if(desa != 0){
					param = '?kode_kec='+kecamatans+'&kode_desa='+desa;
				}else{
					if(nik != '' || nik != ''){
						param = '?kode_kec='+kecamatans+'&nik='+nik;
					}else{
						param = '?kode_kec='+kecamatans;
					}
				}
			}else {
				if(nik != '' || nik != '' ){
					param = '?nik='+nik;
				}
			} 
			//console.log(param)
			window.location = SITE_URL + 'MgtBansos/DownloadData'+param;
		}
	}
	

	
}

