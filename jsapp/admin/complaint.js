
var tableComplaint;

const acces_url = {
	GetPengaduanId : SITE_URL+"Complaint/GetPengaduanId",
}

$(document).ready(function(){
	
	tableComplaint = $("#tablePengaduan").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,10,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "Complaint/get",
			"type": "POST",
			"datatype": "json",		
		},
	
		"columns": [
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},			
			{ "data": "nama_pelapor", "name": "nama_pelapor", orderable: true ,className: 'text-center'},
			{ "data": "nik_pelapor", "name": "nik_pelapor", orderable: true,className: 'text-center' },
			{ "data": "alamat_pelapor", "name": "alamat_pelapor", orderable: true,className: 'text-center' },			
			/* { "data": "desa_pelapor", "name": "desa_pelapor", orderable: false }, */
			/* { "data": "kecamatan_pelapor", "name": "desa_pelapor", orderable: false }, */
			{ "data": "nik_terlapor", "name": "nik_terlapor", orderable: false },
			{ "data": "nama_terlapor", "name": "nama_terlapor", orderable: false },
			{ "data": "alamat_terlapor", "name": "alamat_terlapor", orderable: false },
			/* { "data": "nama_desa", "name": "nama_desa", orderable: false },			
			{ "data": "nama_kec", "name": "nama_kec", orderable: false }, */
			{ "data": "nama_bantuan", "name": "nama_bantuan", orderable: false },
			{ "data": "keterangan_pelaporan", "name": "keterangan_pelaporan", orderable: false },
			{ "data": "aksi", "name": "aksi", orderable: false },
		]
	   
	});
});

const getDetailPengaduan = (idJenisPengaduan)=>{

	let postIdPengaduan = {'idJenisPengaduan':idJenisPengaduan};

	$.ajax({
		url:acces_url.GetPengaduanId,
		type:'POST',
		data: postIdPengaduan,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){
			ProgressBar('success');
			document.getElementById("ListData").style.display = 'none';
			document.getElementById("formInput").style.display = '';
			if(response.state){
				iziToast.success({
					timeout: 5000,
					title: 'OK',
					message: 'Berhasil Menampilkan Data',
					position: 'topRight'
				});
				$('#info_nik').empty().append(response.data.nik);						
				$('#info_nama_lengkap').empty().append(response.data.nama);						
				$('#info_alamat').empty().append(response.data.alamat);						
				$('#info_kecamatan').empty().append(response.data.nama_kec);						
				$('#info_desa').empty().append(response.data.nama_desa);						
				$('#info_nama_bantuan').empty().append('<strong>Nama Bantuan : '+response.data.nama_bantuan+'</strong> ');						
				$('#info_nama_pelaporan').empty().append('<strong>Keterangan Pelaporan : '+response.data.nama_jenis_bantuan+'</strong> ');						
				$('#info_nik_pelapor').empty().append(response.data.nik_pengadu);						
				$('#info_nama_lengkap_pelapor').empty().append(response.data.nama_pengadu);						
				$('#info_alamat_pengadu').empty().append(response.data.alamat_pengadu);						
				$('#info_kecamatan_pengadu').empty().append(response.data.nama_kec_pengadu);						
				$('#info_desa_pengadu').empty().append(response.data.nama_desa_pengadu);						
				$('#info_tlp_pelapor').empty().append(response.data.no_telp);
				let htmlDokumentasi = '<img class="img-thumbnail" src="'+SITE_URL+'/uploads/pegaduan/'+response.data.dokumentasi+'">';						
				$('#foto-pegaduan').empty().append(htmlDokumentasi);
			}else{
				iziToast.error({
					title: 'ERROR',
					message: response.msg,
					position: 'topRight'
				});
			}
		}
	})
}

let onCancel = () =>{
	document.getElementById("ListData").style.display = '';
	document.getElementById("formInput").style.display = 'none';
}
