const acces_url = {
	getDtksNonDtksKec : SITE_URL+'Main/GraphDtKsNonDtksKec',
	FilterDashboard : SITE_URL+'Main/FilterDashboard',
	GetDataKecamatan : SITE_URL+"MgtKpm/getKecamatan",
	GetDesaByCodeKec : SITE_URL+"MgtKpm/getDesa",
}
const initAjax = (service) => {
    var dataArray = $.ajax({
        type: "GET",
        async: false,
        dataType: "json",
        beforeSend: function () {
            //ProgressBar("wait");
        },
        url: service,
    });

    var result = dataArray.responseJSON; //JSON.stringify

    return result;
};

$(document).ready(function(){
	getChartDtksNonDtksKec("");
	GetKecamatan()
});



GetKecamatan = () =>
{
	$.ajax({
		url: acces_url.GetDataKecamatan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{

		},
		success:(response)=>{
			let countData = response.Data.length;

			let html = '';
			if(countData > 0){		
				html += '<option value="0"> Pilih Salah Satu </option>';		
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_kec + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='kecamatan']").empty().append(html);
		}
	})	
}



$("#kecamatans").on('change',function(){
	let kdKec = this.value;
	$.ajax({
		url:acces_url.GetDesaByCodeKec,
		type: "POST",
		dataType : "json",
		data: {"kode_kec":kdKec},
		beforeSend: () => {

		},
		success: (response) => {
			let countData = response.Data.length;
			let html = '';
			if(countData > 0){				
				html += '<option value="0"> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='desa']").empty();
			$("select[name='desa']").append(html);
		}
	})

});


function getChartDtksNonDtksKec(data){

	let dataChart1 = initAjax(acces_url.getDtksNonDtksKec);
	let DataProviders = [];

	

	if(data == '' || data == null){
		$.each(dataChart1.data,(i,v) => {
			DataProviders.push({
				category : v.nama_kecamatan,
				column1 : v.dtks,
				column2 : v.no_dtks,
			})
		});
	}

	AmCharts.makeChart("chartdiv",
		{
			"type": "serial",
			"categoryField": "category",
			"autoMarginOffset": 40,
			"marginRight": 70,
			"marginTop": 70,
			"startDuration": 1,
			"fontSize": 13,
			"theme": "patterns",
			"categoryAxis": {
				"gridPosition": "start",
				"labelRotation":25,
			},
			"chartScrollbar": {
				"color": "FFFFFF"
			},
			"responsive": {
			"enabled": true
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonText": "[[title]] of [[category]]:[[value]]",
					"fillAlphas": 0.9,
					"id": "AmGraph-1",
					"title": "DTKS",
					"type": "column",
					"valueField": "column1"
				},
				{
					"balloonText": "[[title]] of [[category]]:[[value]]",
					"fillAlphas": 0.9,
					"id": "AmGraph-2",
					"title": "Non DTKS",
					"type": "column",
					"valueField": "column2"
				}
			],
			"guides": [],
			"valueAxes": [
				{
					"id": "ValueAxis-1",
					"title": "Berdasarkan Kecamatan"
				}
			],
			"allLabels": [],
			"balloon": {},
			"titles": [
				{
                    "id": "Title-1",
                    "size": 15,
                    "text": 'DTKS dan Non DTKS Kecamatan'
                }
			],
			"dataProvider": DataProviders
		}
	);

}

function FilterDashboard(){
	let getKec = $('#kecamatans').val();
	let getDesa = $('#desas').val();
	let getStatusVerifikasi = $('#status_verifikasi').val();

	let postInputAjax = {'kode_kec':getKec, 'kode_desa':getDesa, 'status_verifikasi':getStatusVerifikasi};

	$.ajax({
		url:acces_url.FilterDashboard,
		type:'POST',
		data: postInputAjax,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){
			ProgressBar('success');
			if(response.state){
				
				iziToast.success({
					timeout: 5000,
					title: 'OK',
					message: response.msg,
					position: 'topRight'
				});

				let cProvAll = response.data.CProvAll;
				let cProvDtks = response.data.CDtksProv;
				let cProvNonDtks = response.data.CNonDtksProv;
				$('#card_info_all_bantuan_prov').empty().html(cProvAll+" KPM");
				$('#card_info_dtks_prov').empty().html(cProvDtks+" KPM");
				$('#card_info_non_dtks_prov').empty().html(cProvNonDtks+" KPM");

				$('#card_info_all_bantuan_presiden').empty().html(response.data.CPresidenAll+" KPM");
				$('#card_info_dtks_presiden').empty().html(response.data.CDtksPresiden+" KPM");
				$('#card_info_non_dtks_presiden').empty().html(response.data.CNonDtksPresiden+" KPM");
				
				$('#card_info_all_bantuan_bupati').empty().html(response.data.CBupatiAll+" KPM");
				$('#card_info_dtks_bupati').empty().html(response.data.CDtksBupati+" KPM");
				$('#card_info_non_dtks_presiden').empty().html(response.data.CNonDtksBupati+" KPM");
				
				$('#card_info_all_bantuan_dd').empty().html(response.data.CDdAll+" KPM");
				$('#card_info_dtks_dd').empty().html(response.data.CDtksDd+" KPM");
				$('#card_info_non_dtks_dd').empty().html(response.data.CNonDtksDd+" KPM");
				
				$('#card_info_all_bantuan_perluasan').empty().html(response.data.CSembakoPerluasanAll+" KPM");
				$('#card_info_dtks_bantuan_perluasan').empty().html(response.data.CDtksSembakoPerluasan+" KPM");
				$('#card_info_non_dtks_bantuan_perluasan').empty().html(response.data.CNonDtksSembakoPerluasan+" KPM");

				$('#card_info_all_blt').empty().html(response.data.CBltAll+" KPM");
				$('#card_info_dtks_bantuan_blt').empty().html(response.data.CDtksBlt+" KPM");
				$('#card_info_non_dtks_bantuan_blt').empty().html(response.data.CNonDtksBlt+" KPM");

				$('#card_info_all_pkh').empty().html(response.data.CPkhAll+" KPM");
				$('#card_info_dtks_pkh').empty().html(response.data.CDtksPkh+" KPM");
				$('#card_info_non_dtks_pkh').empty().html(response.data.CNonDtksPkh+" KPM");
				
			}else{
				iziToast.error({
					title: 'ERROR',
					message: response.msg,
					position: 'topRight'
				});
			}
		}
	})
}
