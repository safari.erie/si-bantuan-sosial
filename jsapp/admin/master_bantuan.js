

var oTable;

const acces_url = {
	GetDataBantuan : SITE_URL+"MasterBantuan/get",
	SaveBantuan : SITE_URL+"MasterBantuan/SaveBantuan",
	GetDataBantuanById : SITE_URL+"MasterBantuan/GetDataBantuanById",
	GetBantuanDataDelete : SITE_URL+"MasterBantuan/GetBantuanDataDelete",
	BantuanDelete : SITE_URL+"MasterBantuan/BantuanDelete",
}
$(document).ready(function(){
	
	oTable = $("#tableMasterBantuanContent").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,10,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": acces_url.GetDataBantuan,
			"type": "POST",
			"datatype": "json",
			/* "data": function (d) {      				


				d.kode_kec   = $('#kecamatans').val();//3201161
				d.kode_desa  = $('#desas').val();
				d.nik 	 = $('#nik').val();
            } */
		},
	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "nama", "name": "nama", orderable: true,className: 'text-center' },
			{ "data": "keterangan", "name": "keterangan", orderable: true ,className: 'text-center'},
			{ "data": "status", "name": "status", orderable: true ,className: 'text-center'},
			{ "data": "aksi", "name": "aksi", orderable: false },
		]
	   
	});
});

function searchdata(){
	oTable.ajax.reload();
}


function AddMasterBantuan(){
	document.getElementById("ListData").style.display = 'none';
	document.getElementById("formInput").style.display = '';
}

function onCancel(){
	document.getElementById("ListData").style.display = '';
	document.getElementById("formInput").style.display = 'none';
}

function onSubmitBantuan(){
	let id_bantuan = $('#id_bantuan').val();
	let nama_bantuan = $('#nama_bantuan').val();
	let keterangan = $('#keterangan').val();
	let cmb_status = $('#cmb_status').val();

	if(nama_bantuan == null || nama_bantuan == '' || typeof nama_bantuan == 'undefined'){
		
		iziToast.error({
			title: 'ERROR',
			message: 'Nama Bantuan tidak boleh kosong',
			position: 'topRight'
		});
	}else{
		var formData = new FormData($('#formKpm')[0]);
		formData.append('id_bantuan',id_bantuan);
		formData.append('nama_bantuan',nama_bantuan);
		formData.append('keterangan',keterangan);
		formData.append('status',cmb_status);

		$.ajax({
			url:acces_url.SaveBantuan,
			type: 'POST',
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			beforeSend:function(){
				ProgressBar('wait');
			},
			success:function(response) {				
				ProgressBar('success');
			   if(response.state != true){
					
					iziToast.error({
						title: 'ERROR',
						message: response.msg,
						position: 'topRight'
					});
			   }else{
					iziToast.success({
						timeout: 5000,
						title: 'OK',
						message: response.msg,
						position: 'topRight'
					});
					document.getElementById("ListData").style.display = '';
					document.getElementById("formInput").style.display = 'none';
					searchdata();
					clear();
			   }
			}
		})

	}
}

EdiBantuan = (edit_id_bantuan)=>{
	let post_bantuan = {'id_bantuan':edit_id_bantuan};

	$.ajax({
		url:acces_url.GetDataBantuanById,
		type:'POST',
		data :post_bantuan,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){
			ProgressBar('success');
			let respStatus;
			if(response.data.status == 't'){
				respStatus = '1'
			}else{
				respStatus = '0'
			}		
			$('#id_bantuan').val(response.data.id_bantuan);
			$('#nama_bantuan').val(response.data.nama_bantuan);
			$('#keterangan').val(response.data.keterangan);
			$("#cmb_status").val(respStatus).change();
			document.getElementById("ListData").style.display = 'none';
			document.getElementById("formInput").style.display = '';
		}

	})
}

function HapusBantuan(id_bantuan){
	let postId = {'id_bantuan':id_bantuan};
	$.ajax({
		url:acces_url.GetBantuanDataDelete,
		type:'POST',
		data :postId,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');

		},
		success:function(response){
			
			ProgressBar('success');
			if(response.state){
				$('#modal-delete').modal({ backdrop: 'static' });
				$('.modal-title').empty().append('Konfirmasi');
				$('#text_confirm_delete').empty().append('Apakah Yakin Data Kpm Akan dihapus');
				$('#btn_confirm').attr('onclick', 'on_delete_confirm_bantuan(' + id_bantuan + ')');

			}else{
				
				$('#modal-cant-delete').modal({ backdrop: 'static' });
				$('.modal-title').empty().append('Konfirmasi');
				$('#text_confirm').html(response.msg);
				$('#text_confirm_info').html();
			}
		}

	});
}

function on_delete_confirm_bantuan(idBantuan) {
	let postId = {'id_bantuan':idBantuan};
	$.ajax({
		url:acces_url.BantuanDelete,
		type:'POST',
		data :postId,
		dataType:'json',
		beforeSend:function(){
			ProgressBar('wait');
		},
		success:function(response){

			ProgressBar('success');
			if(response.state){
				$('#modal-delete').modal('hide');
				iziToast.success({
					timeout: 5000,
					title: 'OK',
					message: response.msg,
					position: 'topRight'
				});
			}else{
				iziToast.error({
					title: 'ERROR',
					message: response.msg,
					position: 'topRight'
				});
				$('#modal-delete').modal('hide');
				
			}
			searchdata();
		}

	});
}

function clear(){
	 $('#id_bantuan').val('');
	 $('#nama_bantuan').val('');
	 $('#keterangan').val('');
	 $('#cmb_status').val(1);
}
