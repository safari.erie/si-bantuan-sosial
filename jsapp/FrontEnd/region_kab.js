
var oTable;

const acces_url = {
	GetDataKecamatan : SITE_URL+"wilayah/getKecamatan",
	GetDesaByCodeKec : SITE_URL+"wilayah/getDesa",
	GetBantuan : SITE_URL+"wilayah/getBantuan",
}
$(document).ready(function() {
	
	
	oTable = $("#datatable").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "Region/getDtksNonDtks",
			"type": "POST",
			"datatype": "json",
			"data": function (d) {               
				d.kode_kec   = $('#kecamatans').val(); //3201161
				d.kode_desa  = $('#desas').val();
            }
		},
	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "nama_kec", "name": "nama_kec", orderable: true,className: 'text-center' },
			{ "data": "nama_desa", "name": "nama_desa", orderable: true ,className: 'text-center'},
			{ "data": "dtks", "name": "dtks", orderable: true ,className: 'text-center'},
			{ "data": "non_dtks", "name": "non_dtks", orderable: true,className: 'text-center' },
		]
	   
	});

	GetKecamatan();
});

GetKecamatan = () =>
{
	$.ajax({
		url: acces_url.GetDataKecamatan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{

		},
		success:(response)=>{
			let countData = response.Data.length;
			let html = '';
			if(countData > 0){		
				html += '<option value="0"> Pilih Salah Satu </option>';		
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_kec + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='kecamatan']").empty().append(html);
		}
	})	
}


$("#kecamatans").on('change',function(){
	let kdKec = this.value;
	$.ajax({
		url:acces_url.GetDesaByCodeKec,
		type: "POST",
		dataType : "json",
		data: {"kode_kec":kdKec},
		beforeSend: () => {

		},
		success: (response) => {
			let countData = response.Data.length;
			let html = '';
			if(countData > 0){				
				html += '<option value="0"> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='desa']").empty();
			$("select[name='desa']").append(html);
		}
	})

});


function searchData(){
	oTable.ajax.reload();
}

 

