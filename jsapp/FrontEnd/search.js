
var oTable;
$(document).ready(function() {
	
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	let kode_kec = urlParams.get('kode_kec');
	let kode_desa = urlParams.get('kode_desa');
	let bantuan = urlParams.get('bantuan');
	let nik = urlParams.get('nik');
	oTable = $("#datatable").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "search/get",
			"type": "POST",
			"datatype": "json",
			"data": function (d) {               
				d.kode_kec   = kode_kec //3201161
				d.kode_desa  = kode_desa
				d.bantuan = bantuan
				d.nik = nik
            }
		},
	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "nama", "name": "nama", orderable: true,className: 'text-center' },
			{ "data": "nik", "name": "nik", orderable: true ,className: 'text-center'},
			{ "data": "alamat", "name": "alamat", orderable: true,className: 'text-center' },
			{ "data": "kecamatan", "name": "kecamatan", orderable: false },
			{ "data": "desa", "name": "desa", orderable: false },
			{ "data": "nama_bantuan", "name": "nama_bantuan", orderable: false },
			{ "data": "status", "name": "status", orderable: false },
			{ "data": "status_verifikasi", "name": "status_verifikasi", orderable: false },
			{ "data": "aksi", "name": "aksi", orderable: false },
		]
	   
	});
});

 

