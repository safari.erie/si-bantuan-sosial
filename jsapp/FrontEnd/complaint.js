

const acces_url = {
	
	GetDesaByCodeKec : SITE_URL+"wilayah/getDesa",
	SavePengaduan : SITE_URL+"Complaint/savePengaduan",
}

$("#kecamatans").on('change',function(){
	let kdKec = this.value;
	$.ajax({
		url:acces_url.GetDesaByCodeKec,
		type: "POST",
		dataType : "json",
		data: {"kode_kec":kdKec},
		beforeSend: () => {

		},
		success: (response) => {
			let countData = response.Data.length;
			let html = '';
			if(countData > 0){				
				html += '<option value="0"> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='desa']").empty();
			$("select[name='desa']").append(html);
		}
	})

});



function SavePengaduan(){
	var TypeInput = true;

	
	$('[data-input="wajib"]').each(function (i, v) {
		//console.log($("#desas").val());
		if ($(this).val() == "") {
			TypeInput = false;
			$('#formPengaduan').addClass('was-validated');
			return i < 0;
		}
	});

	if(TypeInput){
		if($("#desas").val() == 0 || $("#desas").val() == -1 ){
			console.log('masuk')
			iziToast.error({
				title: 'Input Pengaduan',
				message: 'Desa Belum dipilih',
				position: 'topRight'
			});
		}else if($("#kecamatans").val() == 0){
			iziToast.error({
				title: 'Input Pengaduan',
				message: 'Kecamatan Belum dipilih',
				position: 'topRight'
			});
		}else{
			
			var formData = new FormData($('#formPengaduan')[0]);
			let id_kpm = $('#id_kpm_terlapor').val();
			let nik_pelapor = $('#nik').val();
			let fullName = $('#fullName').val();
			let noTlp = $('#NoTlp').val();
			let alamat = $('#alamat').val();
			let kecamatans = $('#kecamatans').val();
			let desas = $('#desas').val();
			let keterangan = $('input[name="RadioKeterangan"]:checked').val();
			
		
				var formData = new FormData($('#formPengaduan')[0]);
				formData.append('id_desa',desas);
				formData.append('nik_pengadu',nik_pelapor);
				formData.append('nama_pengadu',fullName);
				formData.append('alamat',alamat);
				formData.append('id_jenis_pengaduan',keterangan);		
				formData.append('id_kpm',id_kpm);
				formData.append('no_tlp',noTlp);
				formData.append('dokumentasi_aduan',$('input[type=file][name="dokumentasi_aduan"]')[0].files[0]);
				$.ajax({
					url:acces_url.SavePengaduan,
					type: 'POST',
					dataType: 'json',
					data: formData,
					contentType: false,
					processData: false,
					beforeSend:function(){
						/* $("body").css("cursor", "progress");
						$("#loading").removeAttr("style"); */
						ProgressBar('wait');
					},
					success:function(response) 
					{
						ProgressBar('success');
						if(response.state != true){
							iziToast.error({
								title: 'ERROR',
								message: response.msg,
								position: 'topRight'
							});
						}else{

							iziToast.success({
								timeout: 5000,
								title: 'OK',
								message: response.msg,
								position: 'topRight'
							});
							setTimeout(function(){
								window.location = SITE_URL + 'search'; }, 3000);
							
						}
						
					}
				});
		}
	}
}

function checkextension() {
	var file = document.querySelector("#dokumentasi_aduan");
	if (  /\.(jpe?g|png|gif)$/i.test(file.files[0].name) === false ) { 
		iziToast.error({
            title: 'Gagal Upload',
            message: 'File yang diupload harus format photo',
            position: 'topRight'
        });
		$("#dokumentasi_aduan").replaceWith($("#dokumentasi_aduan").val('').clone(true));
 
 	}
  }

function BatalPengaduan(){
	window.location = SITE_URL + 'search';
}
