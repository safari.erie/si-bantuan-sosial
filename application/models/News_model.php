<?php

class News_model extends CI_Model
{

	

	function get_berita_page(
		$StartData, $rows, $data_cnt, $searching
	){
		$judul_berita_lower = strtolower($searching);
		$sql = "
			SELECT
				 a.id_berita,a.judul_berita,a.isi_berita,a.gambar_berita, concat(b.first_name,' ',b.middle_name,' ',b.last_name) as created, TO_CHAR(
					a.created_dt,
					'MON-DD-YYYY HH:MM:SS'
				) as created_dt
			FROM bansos.tx_berita a
			INNER join tb_user_profile b on a.created_by = b.id_user
		";

		if($searching != ''){
			$sql .= " WHERE LOWER(judul_berita) LIKE '%{$judul_berita_lower}%'";
		}


		if($data_cnt > 0){
			$sql .= " limit " . $StartData . " OFFSET " . $rows;
		}

		
        return $this->db->query($sql)->result();
	}

	function count_berita($judul_berita){
		$judul_berita_lower = strtolower($judul_berita);
		$sql = "
			SELECT
				 count(*) as cnt
			FROM bansos.tx_berita a
			INNER join tb_user_profile b on a.created_by = b.id_user
		";

		if($judul_berita != ''){
			$sql .= " WHERE LOWER(judul_berita) LIKE '%{$judul_berita_lower}%'";
		}

		return $this->db->query($sql)->row()->cnt;

	}

	function get_berita_by_id($id_berita){
		$sql = "
			SELECT
					a.id_berita,a.judul_berita,a.isi_berita,a.gambar_berita, concat(b.first_name,' ',b.middle_name,' ',b.last_name) as created, TO_CHAR(
					a.created_dt,
					'Day,Mon YYYY HH:MM:SS'
				) as created_dt
			FROM bansos.tx_berita a
			INNER join tb_user_profile b on a.created_by = b.id_user
			where id_berita = $id_berita
		";

		$query = $this->db->query($sql);
		return $query->row();
	}

}
