<?php


class Search_model extends CI_Model
{
	
	function get_data(
		$start = ''
		, $length = ''
	    , $order = ''
	    , $dir = 'asc'
	    , $kode_kec = ''
		, $kode_desa  = ''
		, $id_bantuan  = ''
		, $nik = '' 

	){
		$sql = "
			SELECT 
				a.id_kpm, a.nama, a.nik, a.alamat, a.layak, a.status as status_aktif ,a.dtks,
				f.id_bantuan,f.nama_bantuan,f.keterangan,f.status,
				a. kode_desa, b.name as nama_desa,
				c.kode_kec, c.name as nama_kecamatan,
				d.kode_kab,d.name as nama_kabupaten,
				e.kode_prov,e.name as nama_prov,txb.status_verifikasi 	
			from bansos.tm_kpm a
			INNER JOIN bansos.tx_bansos txb on a.id_kpm = txb.id_kpm 
			INNER JOIN ref.tb_bantuan f on txb.id_bantuan = f.id_bantuan 
			INNER JOIN master.tb_desa b on a.kode_desa = b.kode_desa
			INNER JOIN master.tb_kec c on b.kode_kec = c.kode_kec
			INNER JOIN master.tb_kab d on c.kode_kab = d.kode_kab
			INNER JOIN master.tb_prov e on d.kode_prov = e.kode_prov
			WHERE 1 = 1
		";

		if($kode_kec != '' && $kode_desa == '' && $id_bantuan == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}'";
		}

		if($kode_kec != '' && $kode_desa != '' && $id_bantuan == null ){
			$sql .= " AND c.kode_kec = '{$kode_kec}' AND b.kode_desa = '{$kode_desa}'";
		}

		if($kode_kec != '' && $kode_desa != '' && $id_bantuan != '' ){
			$sql .= " AND f.id_bantuan = {$id_bantuan}  AND c.kode_kec = {$kode_kec} AND b.kode_desa = {$kode_desa}";
		}
		if($kode_kec == '' && $kode_desa == '' && $id_bantuan != ''){
			$sql .= " AND f.id_bantuan = {$id_bantuan}";
		}
		if($kode_kec == '' && $kode_desa == '' && $id_bantuan == '' && $nik != ''){
			$sql .= " AND a.nik = '{$nik}'";
		}

		$sql .= " limit " . $start . "OFFSET " . $length;
        return $this->db->query($sql)->result();
		//$sql .= " order by $order $dir";
		/* if ($length > 0) {
            $sql .= " limit " . $length . "OFFSET " . $start;
        } */
	
	}

	function count_data_search($kode_kec = ''
	, $kode_desa  = ''
	, $id_bantuan  = ''
	, $nik = ''){

		$sql = "
			SELECT 
				count(*) as cnt
			from bansos.tm_kpm a
			INNER JOIN bansos.tx_bansos txb on a.id_kpm = txb.id_kpm 
			INNER JOIN ref.tb_bantuan f on txb.id_bantuan = f.id_bantuan 
			INNER JOIN master.tb_desa b on a.kode_desa = b.kode_desa
			INNER JOIN master.tb_kec c on b.kode_kec = c.kode_kec
			INNER JOIN master.tb_kab d on c.kode_kab = d.kode_kab
			INNER JOIN master.tb_prov e on d.kode_prov = e.kode_prov
			WHERE 1 = 1
		";

		if($kode_kec != '' && $kode_desa == '' && $id_bantuan == '' && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}'";
		}

		if($kode_kec != '' && $kode_desa != '' && $id_bantuan == '' && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}' AND b.kode_desa = '{$kode_desa}'";
		}

		if($kode_kec != '' && $kode_desa != '' && $id_bantuan != '' && $nik == ''){
			$sql .= " AND f.id_bantuan = {$id_bantuan}  AND c.kode_kec = {$kode_kec} AND b.kode_desa = {$kode_desa}";
		}
		if($kode_kec == '' && $kode_desa == '' && $id_bantuan == '' && $nik != ''){
			$sql .= " AND a.nik = '{$nik}'";
		}

		return $this->db->query($sql)->row()->cnt;

	}
	

}
