<?php


class Wilayah_model extends CI_Model
{
    
	public function getKec(){

		$sql = "
			Select 
				kode_kec,name FROM 
				master.tb_kec
		";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getDesa($kode_kec){
		$sql = "
			SELECT a.kode_desa, a.kode_kec,a.name, b.name as name_kecamatan
			FROM master.tb_desa a
			JOIN master.tb_kec b on  a.kode_kec = b.kode_kec
			where a.kode_kec = $kode_kec
		";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getBantuan(){
		$sql = "
			SELECT id_bantuan,nama_bantuan from ref.tb_bantuan;
		";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function common_last_id($field,$tableName){
		$sql = "
			SELECT 
				max($field)as lastId from $tableName
			";
		
		$query = $this->db->query($sql)->row();

		return $query;
		
	}

}
