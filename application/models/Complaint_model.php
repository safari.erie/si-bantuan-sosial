<?php

class Complaint_model extends CI_Model
{
	

	public function get_id_kpm($id_kpm){
		$sql = "
						
			SELECT 
			a.id_kpm, a.nama, a.nik, a.alamat, a.layak, a.status,a.dtks,
			f.id_bantuan,f.nama_bantuan,f.keterangan,f.status,
			a. kode_desa, b.name as nama_desa,
			c.kode_kec, c.name as nama_kecamatan,
			d.kode_kab,d.name as nama_kabupaten,
			e.kode_prov,e.name as nama_prov	
			from bansos.tm_kpm a
			INNER JOIN bansos.tx_bansos txb on a.id_kpm = txb.id_kpm 
			INNER JOIN ref.tb_bantuan f on txb.id_bantuan = f.id_bantuan 
			INNER JOIN master.tb_desa b on a.kode_desa = b.kode_desa
			INNER JOIN master.tb_kec c on b.kode_kec = c.kode_kec
			INNER JOIN master.tb_kab d on c.kode_kab = d.kode_kab
			INNER JOIN master.tb_prov e on d.kode_prov = e.kode_prov
			WHERE a.id_kpm = $id_kpm
		
		";

		$query = $this->db->query($sql);
		return $query->row();
	}


	function getLastIdPengaduan(){
		$sql = "
			select count(*) + 1 as  lastId from bansos.tb_pengaduan
		";
		$query = $this->db->query($sql)->row();
		return $query;
	}

	function savePengaduan($data){

		$this->db->insert('bansos.tb_pengaduan', $data);
		return $this->db->affected_rows() > 0;
	}

	function getJenisPengaduan(){
		$sql = "
			SELECT id_jenis_pengaduan,name from ref.tb_jenis_pengaduan
		";

		$query = $this->db->query($sql);
		return $query->result();
	}
}

?>
