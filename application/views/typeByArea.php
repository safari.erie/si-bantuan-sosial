<style>
			.bg-sw-green {
					background-color: #27ae60;
			}

			.text-sm {
					font-size: .875rem;
			}

			.text-white {
					color: #fff;
			}

			.text-left {
					text-align: left;
			}

			.shadow {
					box-shadow: 0 2px 8px rgba(8,37,65,.08);
			}

			.relative {
					position: relative;
			}
			.flex {
					display: flex;
			}

			.rounded-lg, .rounded-md {
					border-radius: .5rem;
			}

						
			element {

			}
			.w-auto {

				width: auto;

			}
			.text-sm {

				font-size: .875rem;

			}
			.text-white {

				color: #fff;

			}
			.text-left {

				text-align: left;

			}
			.shadow {

				box-shadow: 0 2px 8px rgba(8,37,65,.08);

			}
			.relative {

				position: relative;

			}
			.p-3 {

				padding: .75rem;

			}
			.mb-4 {

				margin-bottom: 1rem;

			}
			.font-normal {

				font-weight: 400;

			}
			.flex {

				display: flex;

			}
			.rounded-lg, .rounded-md {

				border-radius: .5rem;

			}
			.bg-sw-green {

				background-color: #27ae60;

			}
			button, input {

				padding: 0;
				line-height: inherit;
				color: inherit;

			}

			.font-bold {
					font-weight: 700;
				}

			.text-xl {
				font-size: 1.25rem;
			}
			.text-sw-green {
				color: #27ae60;
			}

			.text-xs {
				font-size: .75rem;
			}

			.text-gray-300 {
				color: #828282;
			}

			.text-14 {
				font-size: .875rem;
			}
			.text-orange-400 {
				color: #f6ad55;
			}
			.italic {
				font-style: italic;
			}

			.text-gray-300 {
				color: #828282;
			}

		.green-text {
				color: #27ae60;
			}

			.hover\:bg-sw-green:hover {
					background-color: #27ae60;
				}
	</style>
  
  <main id="main">
  	<section id="contact" class="contact">
		<div class="container">
			<h1> Statistik Bantuan di Kabupaten Bogor </h1>
			<div class="row">
				<div class="col-md-12">
					<button class="flex relative w-auto bg-sw-green font-normal mb-4 text-left text-sm p-3 text-white shadow rounded-md">
						<img src="<?php echo base_url(); ?>assets/frontEnd/img/warning_green.svg" alt="icon-warning" class="absolute left-0 top-0"> 
						<div style="z-index: 1;">
							&nbsp;&nbsp;Berikut ini merupakan data penetapan Bantuan Seluruh Kecamatan di Kabupaten Bogor dan PKH, data bantuan lainnya sedang dalam proses pengolahan. 
						</div>
						<br/>
						
					</button>
				</div>
			</div>
			<div class="row"> <!-- hover:bg-sw-green hover:text-white stat -->
				<div class="col-12">
					<div data-v-4c4d23c1=""><div data-v-4c4d23c1="" class="text-sw-maintitle font-bold text-xl lg:text-3xl">
						Statistik Kecamatan
					</div> 
					<div data-v-4c4d23c1="" class="text-14 italic py-2 text-sw-desctitle">
						Pilih salah satu Kecamatan di bawah ini untuk info lebih
						detail.
					</div>
					</div>
				</div>
			</div>
			<?php
				$numOfCols = 4;
				$rowCount = 0;
				$bootstrapColWidth = 12 / $numOfCols;
				?>
				<div class="row" style="padding:10px">
				<?php
				foreach ($Count_kec as $row){
				?>  
						<div class="col-md-<?php echo $bootstrapColWidth; ?>">
							<a href="<?php echo base_url();?>region/areaDet?kdArea=<?php echo $row->kode_kec;?>">
								<div class="info" style="padding:10px">
									<div>
										<?php echo $row->nama_kecamatan;?>
									</div>
									<div class="text-14 green-text py-1 font-bold">
									<?php echo number_format($row->cnt,0,",","."); ?> KPM
									</div>
								</div>
							</a>
						</div>
				<?php
					$rowCount++;
					if($rowCount % $numOfCols == 0) echo '</div><div class="row" style="padding:10px">';
				}
				?>
				</div>

			

			

			<div class="row pt-4">
				<div class="col-4">
						<div class="max-w-xs p-1 italic"><p>*Keterangan:</p></div>
						<p><strong>PKH</strong>: Program Keluarga Harapan</p>
				</div>
				<div class="col-4">
						<div class="max-w-xs p-2 italic"><p><strong>KPM</strong>: Keluarga Penerima Manfaat</p></div>
				</div>
				<div class="col-4">
				<div class="max-w-xs p-2 italic"><p><strong>DTKS</strong>: Data Terpadu Kesejahteraan Sosial</p></div>
				</div>
			</div>
		</div>
	 </section> 
  </main>
