
  <style>
			.bg-sw-green {
					background-color: #27ae60;
			}

			.text-sm {
					font-size: .875rem;
			}

			.text-white {
					color: #fff;
			}

			.text-left {
					text-align: left;
			}

			.shadow {
					box-shadow: 0 2px 8px rgba(8,37,65,.08);
			}

			.relative {
					position: relative;
			}
			.flex {
					display: flex;
			}

			.rounded-lg, .rounded-md {
					border-radius: .5rem;
			}

			
element {

}
.w-auto {

    width: auto;

}
.text-sm {

    font-size: .875rem;

}
.text-white {

    color: #fff;

}
.text-left {

    text-align: left;

}
.shadow {

    box-shadow: 0 2px 8px rgba(8,37,65,.08);

}
.relative {

    position: relative;

}
.p-3 {

    padding: .75rem;

}
.mb-4 {

    margin-bottom: 1rem;

}
.font-normal {

    font-weight: 400;

}
.flex {

    display: flex;

}
.rounded-lg, .rounded-md {

    border-radius: .5rem;

}
.bg-sw-green {

    background-color: #27ae60;

}
button, input {

    padding: 0;
    line-height: inherit;
    color: inherit;

}
	</style>

  <main id="main">
  
     <!-- ======= Contact Us Section ======= -->
    <section id="contact" class="contact">
      <div class="container">
        <div class="row">
				<button class="flex relative w-auto bg-sw-green font-normal mb-4 text-left text-sm p-3 text-white shadow rounded-md">
					<img src="<?php echo base_url(); ?>assets/frontEnd/img/warning_green.svg" alt="icon-warning" class="absolute left-0 top-0"> <div style="z-index: 1;">
            Demi menjaga privasi penerima bantuan, maka untuk <b>Nama</b>,
            <b>Alamat</b> dan <b>NIK</b> akan disamarkan. Jika Anda ingin
            detail, silahkan hubungi Petugas Dinas Sosial Kabupaten Bogor
             <span class="ml-1 text-xs font-bold px-1 bg-white rounded-full" style="color: rgb(39, 174, 96);">?</span></div></button>
					<div class="col-lg-12 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
							<div class="table-responsive" >
								<table id="datatable" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama</th>
											<th>Nik</th>
											<th>Alamat</th>
											<th>Kecamatan</th>
											<th>Desa</th>
											<th>Nama Bantuan</th>
											<th>Status</th>						
											<th>Verifikasi </th>						
											<th style="width:10%">Aksi</th>						
										</tr>
									</thead>
								</table>
							</div>
						
					</div>


        </div>

      </div>
    </section><!-- End Contact Us Section -->
    
		 <!-- ======= About Section ======= -->
		<!--  <section id="about" class="about">
      <div class="container">

        <div class="row justify-content-between">
					<div class="col-lg-6 pt-5 pt-lg-0">
							<h3 data-aos="fade-up">Kontak Bantuan</h3>
							<p data-aos="fade-up" data-aos-delay="100">
							Anda dapat menghubungi Hotline Bantuan Sosial untuk bertanya atau sekedar mencari informasi seputar Bantuan Sosial di Kabupaten Bogor. 
							</p>
							<button class="bg-sw-green p-4 text-white shadow hover:bg-green-800 rounded-md"><a href="https://s.id/ChatbotPikobar" target="_blank" rel="noopener"><div class="flex"><span class="text-14 font-bold pr-4">Hubungi Hotline Kami</span> <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.643 18C10.5286 18 8.42779 17.1851 6.84706 15.6043L2.39565 11.153C-0.368899 8.38847 -0.791096 4.03283 1.39182 0.796302C1.6937 0.34863 2.18086 0.0616958 2.72829 0.00898407C3.30015 -0.0462037 3.86117 0.154739 4.26658 0.560149L6.84698 3.14059C7.58335 3.87692 7.58335 5.07506 6.84698 5.81143L5.95672 6.70169C5.71066 6.94779 5.71066 7.3459 5.95672 7.592L10.4081 12.0434C10.6535 12.2888 11.0529 12.2888 11.2984 12.0434L12.1887 11.1531C12.9249 10.4168 14.1231 10.4167 14.8595 11.1531L17.4399 13.7335C17.8452 14.1388 18.0462 14.6995 17.9911 15.2717C17.9383 15.8191 17.6514 16.3063 17.2038 16.6081C15.8178 17.5429 14.2266 18 12.643 18ZM2.84899 1.26219C2.74806 1.27188 2.56008 1.31578 2.43561 1.50027C0.588977 4.23826 0.946627 7.92338 3.28592 10.2627L7.73729 14.7141C10.0767 17.0534 13.7618 17.411 16.4998 15.5643C16.6843 15.4398 16.7281 15.2519 16.7379 15.1509C16.7567 14.9543 16.6881 14.7622 16.5496 14.6236L13.9692 12.0432C13.7238 11.7978 13.3244 11.7978 13.079 12.0432L12.1887 12.9335C11.4523 13.6698 10.2542 13.6699 9.51786 12.9335L5.06645 8.48218C4.3284 7.74422 4.32823 6.54947 5.06645 5.81134L5.95672 4.92108C6.20219 4.67561 6.20219 4.27624 5.95672 4.03077L3.37632 1.45041C3.23812 1.31221 3.04607 1.24322 2.84899 1.26219Z" fill="white"></path></svg></div></a></button>
						</div>
          <div class="col-lg-5 d-flex align-items-center justify-content-center about-img">
            <img src="<?php echo base_url();?>assets/frontEnd/img/hotline.png" class="img-fluid" alt="" data-aos="zoom-in">
          </div>
          
        </div>

      </div>
    </section> -->
		<!-- End About Section -->

   

    <!-- ======= Clients Section ======= -->
   <!--  <section id="clients" class="clients section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <p>Bekerja sama dengan</p>
        </div>

        <div class="owl-carousel clients-carousel" data-aos="fade-up" data-aos-delay="100">
          <img src="<?php echo base_url(); ?>assets/frontEnd/img/kemensos.png" alt="">
        </div>

      </div>
    </section> --><!-- End Clients Section -->

    

  </main><!-- End #main -->

	
  <!-- Modal: modalPoll -->
<div class="modal fade right" id="ModalFilterAdvance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
    <div class="modal-content">

      <form action="contact.php" method="post" role="form" class="">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Pencarian Lebih Lengkap </p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">×</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">

        <div class="text-center">
          <i class="far fa-file-alt fa-4x mb-3 animated rotateIn"></i>
          <p>
            <strong>Pencarian hanya bisa dilakukan sampai tingkat kelurahan/desa</strong>
          </p>
        </div>

        <hr>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="name">Kecamatan</label>
                  <select name="kecamatan" class="form-control" id="kecamatans">
                    <option value="Babakan Madang">Madang</option>
                  </select>
                  <div class="validate"></div>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Desa</label>
                  <select name="desa" class="form-control" id="desas">
                    <option>-- Pilih Desa --</option>
                  </select>
                  
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Jenis Bantuan</label>
                <select name="jenis_bantuan" class="form-control" id="jenisBantuans">
                    <option>-- Pilih Jenis Bantuan --</option>
                  </select>
                <div class="validate"></div>
              </div>
              
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-success waves-effect waves-light" id="terapkan">Terapkan
          <i class="fa fa-paper-plane ml-1"></i></a>
        <a type="button" class="btn btn-outline-succes waves-effect" data-dismiss="modal">Batal</a>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Modal: modalPoll -->

  