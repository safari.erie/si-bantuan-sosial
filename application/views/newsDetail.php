<style>
	.jdl{
		color: #8c8c8c;
		/* padding: 10px 15px; */
	}
	.jdl h1 {
		color: #2a781e;
		font-weight: bold;
		line-height: 120%;
		letter-spacing: -0.5px;
	}
	.jdl .author {
		padding-top: 5px;
	}
</style>

<main id="main">
	<div class="container" style="min-height:40em">
		<div class="row pt-3">
			<div class="col-md-12">
				<?php 
					if(count($dataNews) != 0){
						$gbr_berita = $dataNews->gambar_berita;
						$star = array('stars.png', 'star_black.png', 'star_gold.png', 'star_silver.png');
						if($gbr_berita != "" || $gbr_berita != null){
							if(file_exists('admin/uploads/berita/'.$dataNews->gambar_berita)){
								$gbr_berita = 'admin/uploads/berita/'.$gbr_berita;
							}else{
								$gbr_berita = 'assets/img/news-icon-default.png';
							}
						}else{
							$gbr_berita = 'assets/img/news-icon-default.png';
						}
				?>
					
					<img src="<?php echo base_url().$gbr_berita; ?>" class="pull-left mr-3 mb-2">
					
					<div class="jdl">
						<div class="date"><?php echo $dataNews->created_dt;?></div>
							<h2></h2>
						<h1><?php echo $dataNews->judul_berita;?></h1>
						<div class="author"><?php echo $dataNews->created;?> - Sibos</div>
                	</div>
					<p><?php 
							$isi = $dataNews->isi_berita;
							echo $isi;
						?>
					</p>
				<?php } else { ?>
					<h1>Data Berita Tidak ditemukan</h1>
				<?php } ?>
			</div>
		</div>
	</div>
	
</main>
