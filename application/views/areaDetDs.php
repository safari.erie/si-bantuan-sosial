<style>
			.bg-sw-green {
					background-color: #27ae60;
			}

			.text-sm {
					font-size: .875rem;
			}

			.text-white {
					color: #fff;
			}

			.text-left {
					text-align: left;
			}

			.shadow {
					box-shadow: 0 2px 8px rgba(8,37,65,.08);
			}

			.relative {
					position: relative;
			}
			.flex {
					display: flex;
			}

			.rounded-lg, .rounded-md {
					border-radius: .5rem;
			}

						
			element {

			}
			.w-auto {

				width: auto;

			}
			.text-sm {

				font-size: .875rem;

			}
			.text-white {

				color: #fff;

			}
			.text-left {

				text-align: left;

			}
			.shadow {

				box-shadow: 0 2px 8px rgba(8,37,65,.08);

			}
			.relative {

				position: relative;

			}
			.p-3 {

				padding: .75rem;

			}
			.mb-4 {

				margin-bottom: 1rem;

			}
			.font-normal {

				font-weight: 400;

			}
			.flex {

				display: flex;

			}
			.rounded-lg, .rounded-md {

				border-radius: .5rem;

			}
			.bg-sw-green {

				background-color: #27ae60;

			}
			button, input {

				padding: 0;
				line-height: inherit;
				color: inherit;

			}

			.font-bold {
					font-weight: 700;
				}

			.text-xl {
				font-size: 1.25rem;
			}
			.text-sw-green {
				color: #27ae60;
			}

			.text-xs {
				font-size: .75rem;
			}

			.text-gray-300 {
				color: #828282;
			}

			.text-14 {
				font-size: .875rem;
			}
			.text-orange-400 {
				color: #f6ad55;
			}
			.italic {
				font-style: italic;
			}

			.text-gray-300 {
				color: #828282;
			}
	</style>
  
  <main id="main">
  	<section id="contact" class="contact">
		<div class="container">
			<h1> Statistik Bantuan di Desa <?php echo $getNamaDesa->nama_desa;?> </h1>
			<div class="row">
				<div class="col-md-12">
					<button class="flex relative w-auto bg-sw-green font-normal mb-4 text-left text-sm p-3 text-white shadow rounded-md">
						<img src="<?php echo base_url(); ?>assets/frontEnd/img/warning_green.svg" alt="icon-warning" class="absolute left-0 top-0"> 
						<div style="z-index: 1;">
							Berikut ini merupakan data penetapan Bantuan Kabupaten Bogor dan PKH , data bantuan lainnya sedang dalam proses pengolahan. 
						</div>
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<div class="info" style="padding:10px">
						<div class="font-bold">
						BANSOS SEMBAKO PRESIDEN 
						</div>
						<div class="font-bold text-xl text-sw-green">
							<?php echo number_format($count_presiden_all,0,",","."); ?> KPM
						</div>
						<div class="text-xs text-gray-300">
							Sumber : Kementerian Sosial
						</div>
						<div class="flex"><div class="w-1/2 mt-2 pr-2 border-r"><div class="text-xs text-gray-600">DTKS</div> <div class="text-14 text-orange-400">
							 <?php echo number_format($count_presiden_dtks,0,",","."); ?> KPM 
						</div></div> <div class="w-1/2 mt-2 pl-2"><div class="text-xs text-gray-600">Non-DTKS</div> <div class="text-14 text-orange-400">
						<?php echo number_format($count_presiden_non_dtks,0,",","."); ?> KPM 
							</div></div>
						</div>
					</div>
				</div>
				<div class="col-3">
				<div class="info" style="padding:10px">
						<div class="font-bold">
						BANTUAN PROVINSI
						</div>
						<div class="font-bold text-xl text-sw-green">
							<?php echo number_format($count_prov_all,0,",","."); ?> KPM
						</div>
						<div class="text-xs text-gray-300">
							Sumber : Kementerian Sosial
						</div>
						<div class="flex"><div class="w-1/2 mt-2 pr-2 border-r"><div class="text-xs text-gray-600">DTKS</div> <div class="text-14 text-orange-400">
							 <?php echo number_format($count_prov_dtks,0,",","."); ?> KPM 
						</div></div> <div class="w-1/2 mt-2 pl-2"><div class="text-xs text-gray-600">Non-DTKS</div> <div class="text-14 text-orange-400">
						<?php echo number_format($count_prov_non_dtks,0,",","."); ?> KPM 
							</div></div>
						</div>
					</div>
				</div>
				<div class="col-3">
					<div class="info" style="padding:10px">
						<div class="font-bold">
							BANSOS BUPATI
						</div>
						<div class="font-bold text-xl text-sw-green">
							<?php echo number_format($count_bupati_all,0,",","."); ?> KPM
						</div>
						<div class="text-xs text-gray-300">
							Sumber : Kementerian Sosial
						</div>
						<div class="flex"><div class="w-1/2 mt-2 pr-2 border-r"><div class="text-xs text-gray-600">DTKS</div> <div class="text-14 text-orange-400">
							 <?php echo number_format($count_bupati_dtks,0,",","."); ?> KPM 
						</div></div> <div class="w-1/2 mt-2 pl-2"><div class="text-xs text-gray-600">Non-DTKS</div> <div class="text-14 text-orange-400">
						<?php echo number_format($count_bupati_non_dtks,0,",","."); ?> KPM 
							</div></div>
						</div>
					</div>
				</div>
				<div class="col-3">
					<div class="info" style="padding:10px">
						<div class="font-bold">
							BLT DANA DESA
						</div>
						<div class="font-bold text-xl text-sw-green">
							<?php echo number_format($count_dana_desa_area_all,0,",","."); ?> KPM
						</div>
						<div class="text-xs text-gray-300">
							Sumber : Kementerian Sosial
						</div>
						<div class="flex"><div class="w-1/2 mt-2 pr-2 border-r"><div class="text-xs text-gray-600">DTKS</div> <div class="text-14 text-orange-400">
							 <?php echo number_format($count_dana_desa_araa_dtks,0,",","."); ?> KPM 
						</div></div> <div class="w-1/2 mt-2 pl-2"><div class="text-xs text-gray-600">Non-DTKS</div> <div class="text-14 text-orange-400">
						<?php echo number_format($count_dana_desa_araa__non_dtks,0,",","."); ?> KPM 
							</div></div>
						</div>
					</div>
				</div>
			</div>

			<div class="row pt-4">
				<div class="col-3">
					<div class="info" style="padding:10px">
						<div class="font-bold">
							BANSOS SEMBAKO REGULER
						</div>
						<div class="font-bold text-xl text-sw-green">
							<?php echo number_format($count_sr_all,0,",","."); ?> KPM
						</div>
						<div class="text-xs text-gray-300">
							Sumber : Kementerian Sosial
						</div>
						<div class="flex"><div class="w-1/2 mt-2 pr-2 border-r"><div class="text-xs text-gray-600">DTKS</div> <div class="text-14 text-orange-400">
							 <?php echo number_format($count_sr_dtks,0,",","."); ?> KPM 
						</div></div> <div class="w-1/2 mt-2 pl-2"><div class="text-xs text-gray-600">Non-DTKS</div> <div class="text-14 text-orange-400">
						<?php echo number_format($count_sr_non_dtks,0,",","."); ?> KPM 
							</div></div>
						</div>
					</div>
				</div>
				<div class="col-3">
					<div class="info" style="padding:10px">
						<div class="font-bold">
							KARTU SEMBAKO PERLUASAN 
						</div>
						<div class="font-bold text-xl text-sw-green">
							<?php echo number_format($count_perluasan_area_all,0,",","."); ?> KPM
						</div>
						<div class="text-xs text-gray-300">
							Sumber : Kementerian Sosial
						</div>
						<div class="flex"><div class="w-1/2 mt-2 pr-2 border-r"><div class="text-xs text-gray-600">DTKS</div> <div class="text-14 text-orange-400">
							 <?php echo number_format($count_perluasan_area_dtks,0,",","."); ?> KPM 
						</div></div> <div class="w-1/2 mt-2 pl-2"><div class="text-xs text-gray-600">Non-DTKS</div> <div class="text-14 text-orange-400">
						<?php echo number_format($count_perluasan_area_non_dtks,0,",","."); ?> KPM 
							</div></div>
						</div>
					</div>
				</div>
				<div class="col-3">
				<div class="info" style="padding:10px">
						<div class="font-bold">
							BANTUAN SOSIAL TUNAI 
						</div>
						<div class="font-bold text-xl text-sw-green">
							<?php echo number_format($count_tunai_area_all,0,",","."); ?> KPM
						</div>
						<div class="text-xs text-gray-300">
							Sumber : Kementerian Sosial
						</div>
						<div class="flex"><div class="w-1/2 mt-2 pr-2 border-r"><div class="text-xs text-gray-600">DTKS</div> <div class="text-14 text-orange-400">
							 <?php echo number_format($count_tunai_area_dtks,0,",","."); ?> KPM 
						</div></div> <div class="w-1/2 mt-2 pl-2"><div class="text-xs text-gray-600">Non-DTKS</div> <div class="text-14 text-orange-400">
						<?php echo number_format($count_tunai_area_non_dtks,0,",","."); ?> KPM 
							</div></div>
						</div>
					</div>
				</div>
				<div class="col-3">
				<div class="info" style="padding:10px">
						<div class="font-bold">
							PKH
						</div>
						<div class="font-bold text-xl text-sw-green">
							<?php echo number_format($count_pkh_area_all,0,",","."); ?> KPM
						</div>
						<div class="text-xs text-gray-300">
							Sumber : Kementerian Sosial
						</div>
						<div class="flex"><div class="w-1/2 mt-2 pr-2 border-r"><div class="text-xs text-gray-600">DTKS</div> <div class="text-14 text-orange-400">
							 <?php echo number_format($count_pkh_area_dtks,0,",","."); ?> KPM 
						</div></div> <div class="w-1/2 mt-2 pl-2"><div class="text-xs text-gray-600">Non-DTKS</div> <div class="text-14 text-orange-400">
						<?php echo number_format($count_pkh_area_non_dtks,0,",","."); ?> KPM 
							</div></div>
						</div>
					</div>
				</div>
			</div>

			<div class="row pt-4 pb-4">
				<div class="col-4">
						<div class="max-w-xs p-1 italic"><p>*Keterangan:</p></div>
						<p><strong>PKH</strong>: Program Keluarga Harapan</p>
				</div>
				<div class="col-4">
						<div class="max-w-xs p-2 italic"><p><strong>KPM</strong>: Keluarga Penerima Manfaat</p></div>
				</div>
				<div class="col-4">
				<div class="max-w-xs p-2 italic"><p><strong>DTKS</strong>: Data Terpadu Kesejahteraan Sosial</p></div>
				</div>
			</div>

			
			

			



		</div>
	 </section> 
  </main>
