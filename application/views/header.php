
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SIBOS Pancakarsa</title>
  <meta content="Sistem Informasi Bantuan Sosial (SIBOS) Kabupaten Bogor - Media Informasi Seputar Bantuan Sosial di Kabupaten Bogor | Dinas Sosial Kabupaten Bogor" name="description">
  <meta content="sistem informasi bantuan sosial, sibos, bantuan sosial, bantuan, sosial, bansos, dinas sosial kabupaten bogor, dinsos bogor, bansos bogor, bantuan sosial bogor" name="keywords">
  <meta name="google-site-verification" content="ELq2TbiHWUaE0WX1ncILaH3UAxXEPTnRRjMohZ9eh9Q" />
  <!-- Favicons -->
<link type="text/css" href="<?php echo base_url();?>assets/img/design_01.png" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/plugin/datatable/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/plugin/izitoast/css/iziToast.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/plugin/select2/dist/css/select2.min.css" rel="stylesheet">
	<style>
		.preloader {
				width: 100%;
				height: 100%;
				top: 0;
				position: fixed;
				z-index: 99999;
				background: #fff;
		}

		.loader {
				overflow: visible;
				padding-top: 2em;
				height: 0;
				width: 2em;
				
		}

		.loader, .loader__figure {
				position: absolute;
				top: 30%;
				left: 50%;
		}

	

		.loader__label {
			float: center;
			-webkit-transform: translateX(-50%);
			-moz-transform: translateX(-50%);
			-ms-transform: translateX(-50%);
			-o-transform: translateX(-50%);
			transform: translateX(-50%);
			margin: .15em 3em 0 50%;
			font-size: .875em;
			letter-spacing: .1em;
			line-height: 1.5em;
			color: #1976d2;
		}

		/* .title-Loader{
			margin-top : 30px;
			margin-left:20px;
		} */

		#loading 
		{
			width: 100%;
			height: 100%;
			top: 0px;
			left: 0px;
			position: fixed;
			display: block;
			opacity: 0.9;
			background-color: #fff;
			z-index: 2000;
			text-align: center;
		}

		.water{
				width:100px;
				height: 100px;				
				 /* background-color: #46ae5b; */
				 background: url("assets/img/design_01.png");
				border-radius: 50%;
				position: relative;
				/* box-shadow: inset 0 0 30px 0 rgba(0,0,0,.5), 0 4px 10px 0 rgba(0,0,0,.5); */
				overflow: hidden;
		}
		.water:before, .water:after{
				content:'';
				position: absolute;
				width:500px;
				height: 350px;
				top:-100px;
				background-color: #fff;
		}

		.water:before{
				border-radius: 25%;
				background:rgba(66, 185, 49, 0.31);
				animation:wave 5s linear infinite;
		}

		.water:after{
				border-radius: 25%;
				background:rgba(66, 185, 49, 0.31);
				animation:wave 5s linear infinite;
				
		}

		@keyframes wave{
				0%{
						transform: rotate(0);
				}
				100%{
						transform: rotate(360deg);
				}
		}


			

	</style>



  <!-- Template Main CSS File -->
  <link href="<?php echo base_url(); ?>assets/frontEnd/css/style.css" rel="stylesheet">

  <script type="text/javascript">
    var SITE_URL = '<?php echo site_url() ?>';
    //uri string ada di my_controller
    var CONTROLLER = '<?php echo ($this->uri->segment(1) !== FALSE) ? $this->uri->segment(1) : ""; ?>';

		// console.log('tes >> '+CONTROLLER);
		function ProgressBar(Status) {
				if (Status == "wait") {
						$("body").css("cursor", "progress");
						$("#loading").removeAttr("style");
				} else if (Status == "success") {
						$("body").css("cursor", "default");
						$("#loading").css("display", "none");
				}
		}
	</script>
</head>

<body>
		
		<div class="preloader">
        <div class="loader">
            <div class="water">
							<p> <img src="<?php echo base_url();?>assets/img/design_01.png" width="100px" alt="Loading..." /> </p>
						</div>
            
        </div>
    </div>
		<div id="loading" style="display:none;">
			<div style="margin-top: 210px;"><center><img src="<?php echo base_url();?>assets/img/loading.gif" width="100px" alt="Loading..." /></center></div>
		</div>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container-fluid d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light">
					<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/logos.png"></a></h1>
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="#" onclick="onClickHeaderMenu('hero')" ><i class="fa fa-home" aria-hidden="true"></i> Beranda</a></li>
	  			<li><a href="<?php echo base_url();?>News"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Berita / Informasi</a></li>
          <li><a href="#" onclick="onClickHeaderMenu('about')" data-id="about"><i class="fa fa-bullhorn" aria-hidden="true"></i> Pengaduan</a></li>
          <li> <a href="#" onclick="onClickHeaderMenu('contact')"><i class="fa fa-search" aria-hidden="true"></i> Pencarian</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
