  <style>
		.hover\:bg-green-800:hover {
				background-color: #008444;
		}

		.text-white {
				color: #fff;
		}

		.shadow {
				box-shadow: 0 2px 8px rgba(8,37,65,.08);
		}

		.bg-sw-green {
					background-color: #27ae60;
			}

		.text-14 {
				font-size: .875rem;
				color: white;
		}

		.pr-4 {
				padding-right: 1rem;

		}

		.text-sw-desctitle {
				color: #414d5c;
		}

		.subtitle-1 {
				line-height: 1.625;
		}

		.font-bold {
				font-weight: 700;
		}

		.hyperlink-1, .hyperlink-2 {
				font-weight: 700;
				line-height: 1.625;
				text-decoration: underline;
				cursor: pointer;
		}

		.text-blue-600 {
				color: #1e88e5;
		}

		.py-4 {
				padding-top: 1rem;
				padding-bottom: 1rem;
		}

		
	</style>

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1">
          <h1>Sistem Informasi Data Bantuan Sosial</h1>
          <h2 style="margin-bottom:30px">Pemerintah Kabupaten Bogor</h2>
          <!-- <a href="#about" class="btn-get-started scrollto">Get Started</a> -->
          <button type="button" class="btn-get-started scrollto" data-toggle="modal" id="FilterDetail"> <i class="fa fa-search" aria-hidden="true"></i>  Cari Data Penerima </button> <br>
          <button type="button" class="btn-get-statistic1 scrollto" onclick="toBantuan()" >   <i class="fa fa-bar-chart" aria-hidden="true"></i> Statistik Kabupaten </button>
					<button type="button" class="btn-get-statistic2 scrollto" onclick="toArea()" >  <i class="fa fa-bar-chart" aria-hidden="true"></i>  Statistik Kecamatan  </button>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img">
          <img src="<?php echo base_url();?>assets/frontEnd/img/design_01_penerima.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">
  
     <!-- ======= Contact Us Section ======= -->
    <section id="contact" class="contact">
      <div class="container">
        <div class="row">

          <div class="col-lg-5 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <h4>Pencarian Berdasarkan NIK</h4>
             <br />
              <div class="form-group">
                <label for="name">Masukan NIK Anda</label>
                <input type="number" class="form-control" name="nik" id="nik">
                <div class="validate"></div>
              </div>
              <div class="text-center">
								<a href="javascript:void(0)" class="btn-get-started scrollto" id="btnCariNik"> Cari Data</a>
							</div>
            </form>
          </div>

          <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="name">Kecamatan</label>
                  <select name="kecamatan" id="kecamatan" class="form-control select2" style="width:100%">
                    <option value="0"> -- Pilih Kecamatan -- </option>
                  </select>
                  <div class="validate"></div>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Desa</label>
                  <select name="desa" class="form-control select2" style="width:100%">
                    <option value="0"> -- Pilih Desa -- </option>
                  </select>
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Jenis Bantuan</label>
                <select name="jenis_bantuan" id="bantuanId" class="form-control select2" style="width:100%">                    
                </select>
                <div class="validate"></div>
              </div>
              <div class="text-center">
							<a type="button" class="btn btn-success waves-effect waves-light" id="terapkanFilter">Terapkan
          		<i class="fa fa-paper-plane ml-1"></i></a>
							</div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Us Section -->
    

    <!-- ======= About Section ======= -->
		<section id="about" class="about">
      <div class="container">

        <div class="row justify-content-between">
					<div class="col-lg-6 pt-5 pt-lg-0">
							<h3 data-aos="fade-up">Kontak Bantuan</h3>
							<p data-aos="fade-up" data-aos-delay="100">
									Anda dapat menghubungi Hotline Bantuan Sosial untuk bertanya atau sekedar mencari informasi seputar Bantuan Sosial di Kabupaten Bogor. 
							</p>
							
							<button class="bg-sw-green p-4 text-white shadow hover:bg-green-800 rounded-md"><a href="javascript:void(0)" target="_blank" rel="noopener"><div class="flex"><span class="text-14 font-bold pr-4">Hubungi Hotline Kami</span> <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.643 18C10.5286 18 8.42779 17.1851 6.84706 15.6043L2.39565 11.153C-0.368899 8.38847 -0.791096 4.03283 1.39182 0.796302C1.6937 0.34863 2.18086 0.0616958 2.72829 0.00898407C3.30015 -0.0462037 3.86117 0.154739 4.26658 0.560149L6.84698 3.14059C7.58335 3.87692 7.58335 5.07506 6.84698 5.81143L5.95672 6.70169C5.71066 6.94779 5.71066 7.3459 5.95672 7.592L10.4081 12.0434C10.6535 12.2888 11.0529 12.2888 11.2984 12.0434L12.1887 11.1531C12.9249 10.4168 14.1231 10.4167 14.8595 11.1531L17.4399 13.7335C17.8452 14.1388 18.0462 14.6995 17.9911 15.2717C17.9383 15.8191 17.6514 16.3063 17.2038 16.6081C15.8178 17.5429 14.2266 18 12.643 18ZM2.84899 1.26219C2.74806 1.27188 2.56008 1.31578 2.43561 1.50027C0.588977 4.23826 0.946627 7.92338 3.28592 10.2627L7.73729 14.7141C10.0767 17.0534 13.7618 17.411 16.4998 15.5643C16.6843 15.4398 16.7281 15.2519 16.7379 15.1509C16.7567 14.9543 16.6881 14.7622 16.5496 14.6236L13.9692 12.0432C13.7238 11.7978 13.3244 11.7978 13.079 12.0432L12.1887 12.9335C11.4523 13.6698 10.2542 13.6699 9.51786 12.9335L5.06645 8.48218C4.3284 7.74422 4.32823 6.54947 5.06645 5.81134L5.95672 4.92108C6.20219 4.67561 6.20219 4.27624 5.95672 4.03077L3.37632 1.45041C3.23812 1.31221 3.04607 1.24322 2.84899 1.26219Z" fill="white"></path></svg></div></a></button>
						</div>
          <div class="col-lg-5 d-flex align-items-center justify-content-center about-img">
            <img src="<?php echo base_url();?>assets/frontEnd/img/2-01.png" class="img-fluid" alt="" data-aos="zoom-in">
          </div>
          
        </div>

      </div>
    </section><!-- End About Section -->



    <!-- ======= F.A.Q Section ======= -->
    <section id="statistic" class="faq section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <p>Statistik Bantuan Sosial</p>
          <div class="body-1 w-3/5 text-sw-desctitle mx-auto" style="padding-top:10px">
						Anda dapat melihat informasi realisasi bantuan sosial beserta jenis dan
						status alokasi bantuan sosial yang dilengkapi jumlah total penerima
						manfaat pada masing-masing wilayah.
					</div>
					<div class="row" style="padding-top:40px">
						<div class="col-md-6">
							<div class="subtitle-1 font-bold font-roboto text-sw-desctitle pb-2">
								Kabupaten Bogor
							</div>
							<div class="body-2 text-sw-desctitle">
								Lihat statistik jumlah penerima manfaat bantuan sosial pemerintah di seluruh wilayah Kabupaten Bogor.
							</div>
							<div class="hyperlink-2 text-blue-600 py-4 cursor-pointer" onclick="toBantuan()">
								Lihat Selengkapnya
							</div>
						</div>
						<div class="col-md-6">
							<div class="subtitle-1 font-bold font-roboto text-sw-desctitle pb-2">
								Kecamatan 
							</div>
							<div class="body-2 text-sw-desctitle">
								Lihat statistik jumlah penerima manfaat bantuan sosial pemerintah di seluruh Kecamatan se- Kabupaten Bogor.
							</div>
							<div class="hyperlink-2 text-blue-600 py-4 cursor-pointer" onclick="toArea()">
								Lihat Selengkapnya
							</div>
						</div>
					</div>
        </div>

        

      </div>
    </section>
		<!-- End F.A.Q Section -->

    <!-- ======= Clients Section ======= -->
    <section id="infoContact" class="clients section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <p class="text-center">Unduh SIBOS Pancakarsa </p>
        </div>
				<div class="text-center">						
								<a href="https://play.google.com/store/apps/details?id=gov.dinsos.sibos">
									<img src="<?php echo base_url(); ?>assets/img/logo_play_store.png" class="rounded col-md-4" /> 
								</a>
							</div>
							<!-- <div class="body-1 w-3/5 text-sw-desctitle mx-auto text-center" style="padding-top:10px">
								Anda dapat mengunduh pada Playstore dengan kata kunci <strong>SIBOS Pancakarsa :</strong>.
							</div> -->

       <!--  <div class="owl-carousel clients-carousel" data-aos="fade-up" data-aos-delay="100">
					<img src="<?php echo base_url(); ?>assets/frontEnd/img/kemensos.png" alt="">
        </div> -->

      </div>
    </section><!-- End Clients Section -->


  </main><!-- End #main -->

	
  <!-- Modal: modalPoll -->
<div class="modal fade right" id="ModalFilterAdvance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
    <div class="modal-content">

      <form action="contact.php" method="post" role="form" class="">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Pencarian Lebih Lengkap </p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">×</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">

        <div class="text-center">
          <i class="far fa-file-alt fa-4x mb-3 animated rotateIn"></i>
          <p>
            <strong>Pencarian hanya bisa dilakukan sampai tingkat kelurahan/desa</strong>
          </p>
        </div>

        <hr>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="name">Kecamatan</label>
                  <select name="kecamatan" class="form-control select2" id="kecamatans" style="width:100%">
                    <option value="0">-- Pilih Kec --</option>
                  </select>
                  <div class="validate"></div>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Desa</label>
                  <select name="desa" class="form-control select2" id="desas" style="width:100%">
                    <option value="0">-- Pilih Desa --</option>
                  </select>
                  
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Jenis Bantuan</label>
                <select name="jenis_bantuan" class="form-control select2" id="jenisBantuans" style="width:100%">
                  </select>
                <div class="validate"></div>
              </div>
              
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-success waves-effect waves-light" id="terapkan">Terapkan
          <i class="fa fa-paper-plane ml-1"></i></a>
        <a type="button" class="btn btn-outline-succes waves-effect" data-dismiss="modal">Batal</a>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Modal: modalPoll -->

  