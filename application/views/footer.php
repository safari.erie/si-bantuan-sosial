<!-- ======= Footer ======= -->
<footer id="footer">

<div class="container py-4">
  <div class="copyright">
	&copy; Copyright <strong><span>Dinas Sosial Kabupaten Bogor</span></strong>. All Rights Reserved
  </div>
  <div class="credits">
	
  </div>
</div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>



<!-- Vendor JS Files -->
<script src="<?php echo base_url();?>assets/frontEnd/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/php-email-form/validate.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/venobox/venobox.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/aos/aos.js"></script>  
<script src="<?php echo base_url();?>assets/plugin/datatable/1.10.19/js/jquery.dataTables.js"></script>  
<script src="<?php echo base_url();?>assets/plugin/datatable/1.10.19/js/dataTables.bootstrap4.min.js"></script>  
<script src="<?php echo base_url();?>assets/plugin/izitoast/js/iziToast.min.js"></script>  
<script src="<?php echo base_url();?>assets/plugin/select2/dist/js/select2.full.js"></script>  
<script src="https://use.fontawesome.com/846b20144e.js"></script>


<!-- Template Main JS File -->
<script src="<?php echo base_url();?>assets/frontEnd/js/main.js"></script>

<!-- jsapp -->
  <?php
if (isset($jsapp)) : foreach ($jsapp as $js) : ?>
    <script type="text/javascript" src="<?php echo base_url() ?>jsapp/<?php echo $js ?>.js"></script>
<?php
  endforeach;
endif;
?>


<script type="text/javascript">

	function onClickHeaderMenu(idSelectors){
		window.location.href= SITE_URL+"#"+idSelectors;
		$("#"+idSelectors).fadeIn(3000)
	}

	$(document).ready(function () {
            
		$('.select2').select2({
				containerCssClass: 'select2-container',
				width: 'resolve',
		});
		setTimeout(function(){$(".preloader").fadeOut();	 }, 3000);
					  
	});


</script>

</body>

</html>
