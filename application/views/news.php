<style>
	.widget-boxed-header {
			padding: 14px 0;
	}
	.btn-gold:hover, .btn-gold.disabled:hover {
		background: #d9b826;
		border: 1px solid #d9b826;
	}
	.widget-boxed {
		background-color: #fff;
		border-radius: 6px;
		padding: 0 12px 5px 20px;
		transform: translate3d(0, 0, 0);
		margin-bottom: 35px;
		position: relative;
		border: 1px solid #eaeff5;
			border-top-color: rgb(234, 239, 245);
			border-top-style: solid;
			border-top-width: 1px;
	}

	.pagination {
		
		padding-left: 0;
		border-radius: 4px;
		margin: 20px auto;
	}

	.pagination > li > a, .pagination > li > span {
		position: relative;
		float: left;
		padding: 0;
		margin: 5px;
		color: #5a6f7c;
		text-decoration: none;
		background-color: #fff;
		border-radius: 2px;
		width: 37px;
		height: 37px;
		text-align: center;
		line-height: 37px;
		border: 1px solid #eaeff5;
		-webkit-box-shadow: 0 2px 10px 0 #d8dde6;
		box-shadow: 0 2px 10px 0 #d8dde6;
	}

	
	.labdate {
		color: #959595;
		display: block;
		margin-top: 5px;
	}
	.f11 {
		font-size: 15px;
	}
	.labdate {
		margin-bottom: 1px;
	}
	.labdate {
		margin: 2px 0;
	}
	.labdate .label {
		color: #059657;
	}

	.title_feed {
		font-family: 'montserrat', sans-serif;
		font-weight: bold;
		padding: 10px;
		background-color: #059657;
		border: none;
	}
	.title_feed, .sticky {
		transition: all 200ms linear 0s;
	}
	.title_feed {
		
		border-bottom: solid 1px #fe8917;
		padding: 9px 10px 0;
		position: relative;
		z-index: 3;
	}
	.title_feed .t_lfl {
		font-size: 16px;
		padding: 3px 0 0;
		font-weight: normal;
		text-transform: uppercase;
	}
	.t_lfl {
		float: left;
		padding: 7px 0px 6px;
		margin-left: 7px;
		border-bottom: solid 4px #ffc62b;
		color: #fff;
		font-size: 16px;
		font-weight: bold;
	}
	.t_lfm_box {
		width: 230px;
		height: 30px;
		margin-left: 130px;
	}

</style>
  <main id="main">
	<div class="container" style="min-height:40em">
		<div class="row">
			<div class="col-12">
				<div class="widget-boxed-header border-0">
					<div class="row" style="padding: 0px; margin-bottom: 10px;">
						<div class="col">
							<h4>Pencarian : </h4>
						</div>
						<div class="col" style="text-align: right;">
							<div class="btn-group">
								<div class="dropdown show">
								   <a class="btn btn-gold dropdown-toggle" href="javascript: void(0)" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filter</a>

								   <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
										<a class="dropdown-item" href="<?php echo base_url(); ?>news?page=1&sc_filter=all">All Post</a>
								   </div>
								</div>
							</div>
						</div>
					</div>
					<div class="row" style="padding: 0px;">
						<div class="col-md-12">
							<form method="post" name="sc_news" id="sc_news" action="<?php echo base_url(); ?>News">
								<div class="input-group" style="max-height: 35px;">
									<input type="text" class="form-control" placeholder="Searching .." id="sc_searching" name="sc_searching" style="border-radius: 10px 0px 0px 10px; max-height: 35px;">
									<button type="submit" class="btn btn-gold" style="border-radius: 0px 10px 10px 0px; border: 1px solid #eaeff5; max-height: 35px;">Search</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="title_feed newsfeed_ga sticky mb-3" data-sticky_column="" style="">
						<div class="t_lfl">Berita</div>
						
						<div class="clearfix"></div>
					</div>
				
				<?php
					$i = 0;
					if($CountData > 0){
						foreach($get_list as $rows){
							if(($i%2) == 0){
								//$bgcolor = '#ffe6e6';
								//$bgcolor = '#f3f3f1';
								$bgcolor = '#27ae60';
								$color_text = "#dbc357";
								$border_color = "#8fdb57";
								
							}else{
								$bgcolor = '#dbc357';
								$color_text = "#302e38";
								$border_color = "#98811b";
							}
				?>
					<div class="media" >
						<?php
							$gbr_berita = $rows->gambar_berita;
							$star = array('stars.png', 'star_black.png', 'star_gold.png', 'star_silver.png');
							if($gbr_berita != "" || $gbr_berita != null){
								if(file_exists('admin/uploads/berita/'.$rows->gambar_berita)){
									$gbr_berita = 'admin/uploads/berita/'.$gbr_berita;
								}else{
									$gbr_berita = 'assets/img/news-icon-default.png';
								}
							}else{
								$gbr_berita = 'assets/img/news-icon-default.png';
							}
						?>
						<img src="<?php echo base_url().$gbr_berita; ?>" class="mr-3 mb-2 img-fluid col-3"/>
						<div class="media-body">
							<h3><?php echo 'Judul Berita '.$rows->judul_berita; ?></h3>
							<span class="labdate f11">
							<span class="label"><?php echo $rows->created ?></span> | <?php echo $rows->created_dt ?></span>
								<p>
									<?php
										$num_char = 520;
										$isi = $rows->isi_berita;
										echo substr(strip_tags($isi), 0, $num_char) . '...';
									?>									
								</p>
								<p style='text-align:right;'>
									
									<a href='<?php echo base_url(); ?>news/newsdetail?idns=<?php echo $rows->id_berita ?>' style='color:white;' class='btn btn-warning'>Baca Selengkapnya</a>
                                </p>
						</div>
					</div>
				<?php
					$i++;
					}
				} 
				?>
				</div>
			</div>
			<div class="col-md-12 d-flex justify-content-center">
					<div class="bs-example">
						<ul class="pagination" style="margin: 5px auto;">
							<li>
								<span style="width: 200px;">Showing <?php echo $StartData; ?> to <?php echo $EndData; ?> of <?php echo $CountData; ?> row(s)</span>
							</li>
						</ul>
						<ul class="pagination" style="margin: 5px auto;">
							<li>
								<a href="<?php echo base_url(); ?>News?page=<?php echo $First;?>&sc_filter=<?php echo $type; ?>">
									<i class="fa fa-angle-double-left" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>News?page=<?php echo $Prev;?>&sc_filter=<?php echo $type; ?>">
									<i class="fa fa-angle-left" aria-hidden="true"></i>
								</a>
							</li>
							<?php
							for ($p = 1; $p <= $CountPage; $p++)
							{
								if (($p >= $MinPage) && ($p <= $MaxPage))
								{
									if ($p == $PositionPage){
							?>
										<li class="active">
											<a href="<?php echo base_url(); ?>News?page=<?php echo $p;?>&sc_filter=<?php echo $type; ?>"><?php echo $p; ?></a>
										</li>
							<?php
									}
									else
									{
							?>
										<li>
											<a href="<?php echo base_url(); ?>News?page=<?php echo $p;?>&sc_filter=<?php echo $type; ?>"><?php echo $p; ?></a>
										</li>
							<?php
									}
								}
							}
							?>
							<li>
								<a href="<?php echo base_url(); ?>News?page=<?php echo $Next;?>&sc_filter=<?php echo $type; ?>">
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>News?page=<?php echo $Last;?>&sc_filter=<?php echo $type; ?>">
									<i class="fa fa-angle-double-right" aria-hidden="true"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>

		</div>
	</div>
  </main>
