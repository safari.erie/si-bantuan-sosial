
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    function __construct() {
        parent:: __construct();
		$this->load->model('News_model','nm');
		$this->load->helper('text');
	}

	function index(){
		

		$page 	= $this->input->get('page') ? $this->input->get('page') : 1;
		$rows 	= 10;		
		$type = $this->input->get('sc_filter');
		$searching = $this->input->post('sc_searching');		
		if($type == "all"){
			$data_cnt = $this->nm->count_berita($searching);
		}else{
			$data_cnt = $this->nm->count_berita($searching);
		}

		$mxpg = 3;
		$EndData = $page * $rows;
		$CountData = $data_cnt;
		$PositionPage = $page;
		$DataPerPage = $rows;
		$StartData = ($page - 1) * $rows;
		$CountPage = ceil($data_cnt / $rows);
		$First = 1;
		$Last = $CountPage;
		$Next = $page < $CountPage ? $page + 1 : $CountPage;
		$Prev = $page == 1 ? 1 : $page - 1;
		$EndData = $EndData > $CountData ? $CountData : $EndData;
		$StartData = $CountData > 0 ? $StartData : $CountData;
		$MaxPage = $page + $mxpg;
		$MinPage = $page - $mxpg;

		$get_data['StartData'] = $StartData;
		$get_data['EndData'] = $EndData;
		$get_data['CountData'] = $CountData;
		$get_data['First'] = $First;
		$get_data['Prev'] = $Prev;
		$get_data['CountPage'] = $CountPage;
		$get_data['MinPage'] = $MinPage;
		$get_data['MaxPage'] = $MaxPage;
		$get_data['PositionPage'] = $PositionPage;
		$get_data['Next'] = $Next;
		$get_data['Last'] = $Last;



		if($type == "all"){
			$get_data['get_list'] = $this->nm->get_berita_page($rows, $StartData , $data_cnt, $searching);
		}else{
			$get_data['get_list'] = $this->nm->get_berita_page($rows, $StartData, $data_cnt, $searching);
		}

		$get_data['type'] = $type;
		//$get_data['jsapp'] = array('FrontEnd/news');
		
		$this->load->view('header');
		$this->load->view('news',$get_data);
		$this->load->view('footer');
	}

	function newsdetail(){
		$idns = $this->input->get('idns');
		$data['dataNews'] = $this->nm->get_berita_by_id($idns);
		$this->load->view('header');
		$this->load->view('newsDetail',$data);
		$this->load->view('footer');
	}
}
