
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaint extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('Complaint_model','cm');
        $this->load->model('Wilayah_model','wm');
	}

	function index(){
		$data['jsapp'] = array('FrontEnd/complaint');
		$id_kpm = $this->input->get('ids');
		$data['dt_kpm'] = $this->cm->get_id_kpm($id_kpm);
		$data['dt_kec'] = $this->wm->getKec();
		$data['jenis_pengaduan'] = $this->cm->getJenisPengaduan();
		$this->load->view('header',$data);
		$this->load->view('complaint');
		$this->load->view('footer');
	}

	function savePengaduan(){

			$id_desa 	 			= $this->input->post('id_desa');
			$nik_pengadu 			= $this->input->post('nik_pengadu');
			$nama_pengadu			= $this->input->post('nama_pengadu');
			$alamat					= $this->input->post('alamat');
			$id_jenis_pengaduan		= $this->input->post('id_jenis_pengaduan');		
			$id_kpm 				= $this->input->post('id_kpm');
			$no_tlp 			    = $this->input->post('no_tlp');

		
			$name_of_file;

			$config['upload_path'] = './admin/uploads/pegaduan/original/';  
			$config['allowed_types'] = 'jpg|jpeg|png|gif';  
			$config['file_name']        = "Pengaduan_".time();
            $this->load->library('upload', $config);  
            if(!$this->upload->do_upload('dokumentasi_aduan'))  
            {  
                 echo $this->upload->display_errors();  
            }  
            else  
            {  
                 $data = $this->upload->data();  
                 $config['image_library'] = 'gd2';  
                 $config['source_image'] = './admin/uploads/pegaduan/original/'.$data["file_name"];  
                 //$config['create_thumb'] = false;  
                 $config['maintain_ratio'] = TRUE;  
                 $config['quality'] = '20%';  
                 $config['width'] = 200;  
                // $config['height'] = 200;  
                 $config['new_image'] = './admin/uploads/pegaduan/'.$data["file_name"];  
				 $this->load->library('image_lib', $config);  
				 $this->image_lib->initialize($config);
				 $this->image_lib->resize();  
				 $name_of_file = $data["file_name"];
				 if (!$this->image_lib->resize()) {
					echo $this->image_lib->display_errors();
				}
				$this->image_lib->clear();
				unlink('./admin/uploads/pegaduan/original/'.$data["file_name"]);
			}

			
			$getLastId = $this->wm->common_last_id('id_pengaduan','bansos.tb_pengaduan');
			$lastIdPengaduan;
			if($getLastId->lastid == null){
				$lastIdPengaduan = 1;
			}else{
				$lastIdPengaduan = $getLastId->lastid + 1;
			}

			$data_pengaduan = array(
				'id_pengaduan'			=> $lastIdPengaduan,
				'id_desa'				=> $id_desa,
				'nik_pengadu'			=> $nik_pengadu,
				'nama_pengadu'			=> $nama_pengadu,
				'alamat'				=> $alamat,
				'id_jenis_pengaduan'	=> $id_jenis_pengaduan,
				'id_kpm'				=> $id_kpm,
				'created_dt'		=> date('Y-m-d H:i:s'),
				'dokumentasi'		=> $name_of_file,
				'no_telp'			=> $no_tlp
			);
			$insertPengaduan = $this->cm->savePengaduan($data_pengaduan);
			if($insertPengaduan){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Berhasil diinput'
				);

				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> false,
					'msg'	=> 'Data gagal diinput'
				);
				echo json_encode($output);
			} 
	}

}
