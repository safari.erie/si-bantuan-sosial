<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah extends CI_Controller {

	function __construct() {
		parent:: __construct();
		header('Access-Control-Allow-Origin: *'); 
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization"); 
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE"); $method = $_SERVER['REQUEST_METHOD'];

        $this->load->model('Wilayah_model','wm');
	}
	

	function getKecamatan()
	{

		$data = $this->wm->getKec();
		$result['Data'] = $data;
		echo json_encode($result);
	}

	function getDesa(){
		$kode_kec = $this->input->post('kode_kec');
		
		$data = $this->wm->getDesa($kode_kec);
		$result['Data'] = $data;
		echo json_encode($result);

	}

	function getBantuan(){
	
		
		$data = $this->wm->getBantuan();
		$result['Data'] = $data;
		echo json_encode($result);
	}

	
}
