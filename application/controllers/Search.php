

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('Search_model','sm');
	}


	public function index()
	{
		$kode_kec = $this->input->get('kode_kec');
		
		$data['jsapp'] = array('FrontEnd/search');
		$this->load->view('header',$data);
		$this->load->view('search');
		$this->load->view('footer');
	}	

	function get() {
		
				

        $order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		$kode_kec = $this->input->post('kode_kec');
		$kode_desa = $this->input->post('kode_desa');
		$bantuan = $this->input->post('bantuan');
		$nik = $this->input->post('nik');

		$result = array();
        $recordsTotal = $this->sm->count_data_search($kode_kec, $kode_desa, $bantuan,  $nik);
		
        $row = array();
		$results = $this->sm->get_data($length,$start, $def['order'], 'asc', $kode_kec, $kode_desa, $bantuan, $nik);
		
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				/* $button = '<div class="btn-group">';
				$button .= '<button type="button" class="btn btn-danger">Action</button>';
				$button .= '<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">';
				$button .= '<span class="caret"></span>';
				$button .= '<span class="sr-only">Toggle Dropdown</span>';
				$button .= '</button>';
				$button .= '<ul class="dropdown-menu" role="menu">';
				$button .= '<li><a href="'.base_url('complaint?ids='.$d->id_kpm).'"> <i class="fa fa-bullhorn ml-2"> </i> Adukan Orang ini</a></li>';
				$button .= '</ul>';
				$button .= '</div>'; */
				$button = '';
				$button .= '<a href="'.base_url('complaint?ids='.$d->id_kpm).'" class="btn btn-icon btn-sm icon-left btn-info"> <i class="fa fa-bars"></i>  Detil </a>';
				$ret_status_verifikasi = '';
				if($d->status_verifikasi == 0){
					$ret_status_verifikasi = 'Belum Verifikasi';
				}else if($d->status_verifikasi == 1){
					$ret_status_verifikasi = 'Sudah Verifikasi';
				}else if($d->status_verifikasi = -1){
					$ret_status_verifikasi = 'Ditolak';
				}
                $row[] = array
				(
					"no"				=> $ii,
					"nama" 				=> $d->nama,
					"nik" 				=> substr_replace($d->nik, 'XXXXXX', 6, -4) ,
					"alamat" 			=> $d->alamat,
					"kecamatan" 		=> $d->nama_kecamatan,
					"desa" 				=> $d->nama_desa,
					"nama_bantuan" 		=> $d->nama_bantuan,
					"status" 			=> ($d->status_aktif == true) ? "Aktif" : "TIDAK",
					"status_verifikasi" => $ret_status_verifikasi,
					'aksi'				=> $button
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
    }


}

